<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'lang_vi' => '越南文',
    'lang_en' => '英语',
    'lang_cn' => '中国',
    'home_page' => '家',
    'about_page' => '关于',
    'product_page' => '项目',
    'keyword' => '关键词...',
    'service' => '服务',
    'news' => '消息',
    'customer' => '顾客',
    'trademark' => '商标',
    'contact' => '接触',
    'view_more' => '查看更多',
    'product_of' => '的项目',
    'service_of' => '服务于',
    'customer_of' => '客户',
    'news_of' => '新闻',
    'project' => '项目',
    'partner' => '伙伴',
    'who_is' => '谁是 <span>我们?</span>',
    'category' => '类别',
    'product_new' => '新项目',
    'contact_us' => '联系我们',
    'view_image_big' => '查看大图',
    'producer' => '制片人',
    'origin' => '起源',
    'old_price' => '旧价格',
    'price' => '价格',
    'product_info' => '项目信息',
    'product_other' => '项目相同',
    'cart' => 'Cart',
    'input_full_name' => '输入全名',
    'address' => '地址',
    'input_phone' => '输入您的电话',
    'input_email_address' => '输入您的地址电子邮件',
    'select_payment_method' => 'Select Payment Method',
    'input_content' => '输入内容信息',
    'by_cash' => 'Cash payment',
    'transfer_payment' => 'Transfer payment',
    'online_payment' => 'Online payment',
    'buy' => 'Buy',
    'download_file_pdf' => '下载文件PDF',
    'product_name' => '项目名',
    'qty' => 'Qty',
    'price_unit' => '单价',
    'line_total' => 'Line Total',
    'delete' => 'Delete',
    'total' => 'Total',
    'send' => '发送',
    'detail' => '细节',
    'highlight' => '强调',
    'main_office' => '主办公室',
    'office' => '办公室',
    'phone' => '电话',
    'title' => '标题',
    'mr_hai' => 'Mr Hai',
    'ms_hien' => 'Ms Hien',
    'home_slug' => 'home-cn',
    'home_contact_slug' => 'home-contact-cn',
];