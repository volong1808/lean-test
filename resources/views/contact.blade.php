@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ trans('message.contact') }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- end break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a
                                        href="{{ route('home') }}">{{ trans('message.home_page') }}</a> » <span
                                        class="breadcrumb_last"
                                        aria-current="page">{{ trans('message.contact') }}</span></span></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="block block--padding block--grey-lightest">

        <div class="wrap">
            <div class="call-to-action" style="margin-bottom: 40px;">
                <iframe src="{{ $company['google_maps'] }}" width="100%" height="300" frameborder="0" style="border: 0px; height: 323.692px;" allowfullscreen=""></iframe>
            </div>

            <div class="wysiwyg">
                <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1">
                    <div id="gf_1" class="gform_anchor" tabindex="-1"></div>
                    <form method="post" enctype="multipart/form-data" id="form-contact"
                          action="{{ route('contact_post_' . $lang) }}">
                        @csrf
                        <div id="success-form" class="message__form message__form--success"></div>
                        <div class="gform_body">
                            <ul class="gform_fields left_label form_sublabel_below description_below">
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--name">
                                    <label class="gfield_label" for="name">
                                        {{ trans('message.full_name') }}<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="text" name="name" id="name" />
                                    </div>
                                    <div id="error-name" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--textarea">
                                    <label class="gfield_label" for="message">
                                        {{ trans('message.content') }}<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <textarea name="message" id="message" class="textarea medium" rows="10" cols="39"></textarea>
                                    </div>
                                    <div id="error-message" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--email">
                                    <label class="gfield_label" for="email">
                                        Email<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="email" name="email" id="email" />
                                    </div>
                                    <div id="error-email" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--address">
                                    <label class="gfield_label" for="address">
                                        {{ trans('message.address') }}<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="text" name="address" id="address" />
                                    </div>
                                    <div id="error-address" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--phone">
                                    <label class="gfield_label" for="phone_number">
                                        {{ trans('message.phone') }}<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="text" name="phone_number" id="phone_number" />
                                    </div>
                                    <div id="error-phone_number" class="message__error"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="gform_footer left_label">
                            <button type="submit" class="button">
                                <span class="button__inner"><span class="button__label">{{ trans('message.submit') }}</span></span>
                            </button>
                        </div>
                    </form>
                </div>


            </div>

        </div>

    </div>
    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>
@endsection
@section('script_page')
    <script>
        // Ajax Contact
        $(document).on('submit', '#form-contact', function (e) {
            e.preventDefault();
            var form = $(this);
            var successForm = form.find('#success-form');
            successForm.html('');
            successForm.hide();

            var inputs = [
                { name: 'name', element: form.find('#error-name') },
                { name: 'message', element: form.find('#error-message') },
                { name: 'email', element: form.find('#error-email') },
                { name: 'address', element: form.find('#error-address') },
                { name: 'phone_number', element: form.find('#error-phone_number') }
            ];
            clearErrorInputs(inputs);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: {
                    name: form.find('input[name="name"]').val(),
                    message: form.find('textarea[name="message"]').val(),
                    email: form.find('input[name="email"]').val(),
                    address: form.find('input[name="address"]').val(),
                    phone_number: form.find('input[name="phone_number"]').val()
                },
                success: function (response) {
                    if(response.success) {
                        successForm.html(response.success);
                        successForm.show();
                        form.trigger('reset');
                    }
                },
                error: function (err) {
                    var response = JSON.parse(err.responseText);
                    showErrorInputs(response, inputs);
                }
            });
        });
    </script>
@endsection