@extends('frontend.layouts.master')
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="bg-fff">
        <div class="container">
            <div id="map-link-bar">
                <ul>
                    <li><a href="{{ route('home') }}" title="{{ trans('message.home_page') }}">{{ trans('message.home_page') }}</a></li>
                    <li><a href="{{ route('fr_product_list_en') }}" title="{{ trans('message.product_page') }}">{{ trans('message.product_page') }}</a></li>
                    <li class="active">{{ $name }}</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="bg-fff main-product">

        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="title">
                        <div class="titH-4 border-bottom-sm">{{ $name }}</div>
                    </div>
                    <div class="cart-detail form-contact">
                        @if (!empty($cart))
                            <form action="{{ route('checkout_order') }}" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-md-8" id="content-list-order">
                                        @include('product.includes.body_cart_list')
                                    </div>
                                    <div class="col-md-4">
                                        @if(session('senContactSuccess'))
                                            <div class="alert alert-success">
                                                <strong>Thành công!</strong> {{ session('senContactSuccess') }}.
                                            </div>
                                        @endif
                                        <label for="">
                                            <input name="name" value="{{ $errors->any() ? old('name') : '' }}"
                                                   placeholder="{{ trans('message.input_full_name') }}" required type="text">
                                            @if ($errors->has('name'))
                                                <label class="red-text">{{ $errors->first('name') }}</label>
                                            @endif
                                        </label>
                                        <label for="">
                                            <input type="text" name="address" placeholder="{{ trans('message.address') }} *" value="{{ $errors->any() ? old('address') : '' }}">
                                            @if ($errors->has('address'))
                                                <label class="red-text">{{ $errors->first('address') }}</label>
                                            @endif
                                        </label>
                                        <label for="">
                                            <input name="phone" value="{{ $errors->any() ? old('phone') : '' }}"
                                                   placeholder="{{ trans('message.input_phone') }}" required type="text">
                                            @if ($errors->has('phone'))
                                                <label class="red-text">{{ $errors->first('phone') }}</label>
                                            @endif
                                        </label>
                                        <label for="">
                                            <input name="email" value="{{ $errors->any() ? old('email') : '' }}"
                                                   placeholder="{{ trans('message.input_email_address') }}" required type="text">
                                            @if ($errors->has('email'))
                                                <label class="red-text">{{ $errors->first('email') }}</label>
                                            @endif
                                        </label>
                                        <label for="">
                                            <textarea name="message" placeholder="{{ trans('message.input_content') }}">{!! $errors->any() ? old('message') : '' !!}</textarea>
                                            @if ($errors->has('message'))
                                                <label class="red-text">{{ $errors->first('message') }}</label>
                                            @endif
                                        </label>
                                        <label for="">
                                            <select name="banking_type" id="js-show-content-payment-type">
                                                <option value="">{{ trans('message.select_payment_method') }}</option>
                                                @foreach ($paymentType as $key => $type)
                                                    <option value="{{ $key }}">{{ trans('message.' . $type['trans']) }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('banking_type'))
                                                <label class="red-text">{{ $errors->first('banking_type') }}</label>
                                            @endif
                                        </label>
                                        @if (!empty($paymentTypeContent))
                                        <div class="content-payment-type">
                                            <ul>
                                                @foreach ($paymentTypeContent as $key => $content)
                                                    <li id="{{ $key }}">{!! $content !!}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        <div class="box-f">
                                            <button type="submit" class="submit">{{ trans('message.send') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
@endsection

@section('script_page')
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('input', '.js-change-amount', function () {
                let url = $(this).attr('data-url');
                let token = $(this).attr('data-token');
                let amount = $(this).val();
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        amount: amount,
                        _token: token
                    }
                }).done(function(res) {
                   if (res.status == 200) {
                       $('#content-list-order').html(res.tableCart);
                   }
                });
            });
            $('#js-show-content-payment-type').change(function () {
                let paymentType = $(this).val();
                $('.content-payment-type li').hide();
                $('#' + paymentType).show();
            })
        });
    </script>
@endsection