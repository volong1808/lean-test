@extends('frontend.layouts.master')
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home' . $prefixLang) }}">{{ trans('message.home_page') }}</a> »
                                @if (!empty($categoryDetail))
                                    <span><a href="{{ route('fr_product_list' . $prefixLang) }}">{{ trans('message.product_page') }}</a></span> »
                                    <span class="breadcrumb_last" aria-current="page">{{ $categoryDetail->name }}</span></span></span></p>
                                @else
                                <span class="breadcrumb_last" aria-current="page">{{ trans('message.product_page') }}</span></span></span></p>
                                @endif
                </div>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="block block--padding block--grey-lightest">
        <div class="block__inner">
            <div class="wrap">
                @if ($categories->count() > 0)
                <div class="product-category__items">
                    @foreach ($categories as $category)
                    <div class="product-category__item">
                        <a href="{{ route('product_category' . $prefixLang, $category->slug) }}" class="container-small">
                            <div class="container-small__icon-container">
                                <img class="container-small__icon icon" alt="{{ $category->name }}" src="{{ !empty($category->image) ? asset($category->image->origin) : '' }}">
                            </div>
                            <div class="container-small__inner">
                                <div class="container-small__content">
                                    <div class="container-small__title">{{ $category->name }}</div>
                                    <div class="container-small__subtitle">{{ $category->posts->count() }} {{ trans('message.product_page') }}</div>
                                </div>

                                <div class="container-small__arrow-container">
                                    <span class="container-small__arrow icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                                </div>

                            </div>

                        </a>

                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>
    @include('frontend.includes.branch')
    @include('frontend.includes.subscribe_new_letter')
    <!-- footer -->
@endsection