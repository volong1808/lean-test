@extends('frontend.layouts.master')
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home' . $prefixLang) }}">{{ trans('message.home_page') }}</a>
                                @if (!empty($category->parent))
                                » <span>
                                    <a href="{{ route('product_category' . $prefixLang, $category->parent->slug) }}">{{ $category->parent->name }}</a>
                                @endif
                                    » <span class="breadcrumb_last"
                                            aria-current="page">{{ $category->name }}</span></span></span></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="block block--padding">
        <div class="wrap">
            <div class="block__inner">
                <div data-v-9b406082="" class="block__body">
                    <div data-v-9b406082="" class="product-overview">
                        <!-- filter -->
                        <aside data-v-3016f21d="" data-v-9b406082=""
                               class="product-overview__column product-overview__column--sidebar">
                            <div data-v-3016f21d="" class="filter">
                                <div data-v-3016f21d="" class="filter__inner">
                                    <div data-v-3016f21d="" class="filter__header"><span data-v-3016f21d="" class="filter__title heading h4">Filter</span>
                                        <button data-v-3016f21d="" class="close"><span data-v-3016f21d=""
                                                                                       class="close__icon"></span>
                                        </button>
                                    </div>
                                    <div data-v-3016f21d="" class="filter__body">
                                        <div data-v-3016f21d="" class="filter__items">
                                            <div data-v-3bcc0bcd="" data-v-3016f21d="" class="filter__item"
                                                 style="display: block;">
                                                <div data-v-3bcc0bcd="" class="filter-menu"><h3 data-v-3bcc0bcd=""
                                                                                                class="filter-menu__title">
                                                        Brand
                                                    </h3>
                                                    <ul data-v-3bcc0bcd="">
                                                        <li data-v-3bcc0bcd="">
                                                            <ul data-v-3bcc0bcd="" class="checkboxes">
                                                                <li data-v-3bcc0bcd=""><input data-v-3bcc0bcd=""
                                                                                              type="checkbox"
                                                                                              id="brand-Riken-Keiki"
                                                                                              value="Riken Keiki">
                                                                    <label data-v-3bcc0bcd="" for="brand-Riken-Keiki">Riken
                                                                        Keiki (21)</label></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div data-v-3bcc0bcd="" data-v-3016f21d="" class="filter__item"
                                                 style="display: block;">
                                                <div data-v-3bcc0bcd="" class="filter-menu"><h3 data-v-3bcc0bcd=""
                                                                                                class="filter-menu__title">
                                                        Industry
                                                    </h3>
                                                    <ul data-v-3bcc0bcd="">
                                                        <li data-v-3bcc0bcd="">
                                                            <ul data-v-3bcc0bcd="" class="checkboxes">
                                                                <li data-v-3bcc0bcd=""><input data-v-3bcc0bcd=""
                                                                                              type="checkbox"
                                                                                              id="industry-Process-Industry"
                                                                                              value="Process Industry">
                                                                    <label data-v-3bcc0bcd=""
                                                                           for="industry-Process-Industry">Process
                                                                        Industry (20)</label></li>
                                                                <li data-v-3bcc0bcd=""><input data-v-3bcc0bcd=""
                                                                                              type="checkbox"
                                                                                              id="industry-Maritime"
                                                                                              value="Maritime"> <label
                                                                            data-v-3bcc0bcd="" for="industry-Maritime">Maritime
                                                                        (1)</label></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div data-v-3bcc0bcd="" data-v-3016f21d="" class="filter__item"
                                                 style="display: none;">
                                                <div data-v-3bcc0bcd="" class="filter-menu"><h3 data-v-3bcc0bcd=""
                                                                                                class="filter-menu__title">
                                                        Gas
                                                    </h3>
                                                    <ul data-v-3bcc0bcd="">
                                                        <li data-v-3bcc0bcd="">
                                                            <ul data-v-3bcc0bcd="" class="checkboxes"></ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div data-v-3bcc0bcd="" data-v-3016f21d="" class="filter__item"
                                                 style="display: none;">
                                                <div data-v-3bcc0bcd="" class="filter-menu"><h3 data-v-3bcc0bcd=""
                                                                                                class="filter-menu__title">
                                                        Case
                                                    </h3>
                                                    <ul data-v-3bcc0bcd="">
                                                        <li data-v-3bcc0bcd="">
                                                            <ul data-v-3bcc0bcd="" class="checkboxes"></ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-v-3016f21d="" class="filter__overlay js-filter-overlay"></div>
                        </aside>
                        <!-- /filter -->
                        @if ($products->count() > 0)
                        <div data-v-9b406082="" class="product-overview__column"><!---->
                            <div data-v-9b406082="" class="product-filters">
                                <div data-v-9b406082="" class="product-filters__result-count">Showing <span>{{ $products->count() }}</span> of
                                    <span>{{ $products->total() }}</span> {{ trans('message.product_page') }}
                                </div>
                            </div>
                            <div data-v-9b406082="" class="product-items"><!---->
                                @foreach ($products as $product)
                                <div data-v-9b406082="" class="product-items__item">
                                    <div data-v-9b406082="" class="product-item product-item--active"><a
                                                data-v-9b406082=""
                                                href="{{ route('fr_product_detail' . $prefixLang, ['slug' => $product->slug, 'id' => $product->id]) }}"
                                                class="product-item__image"><!---->
                                            <figure data-v-9b406082="" class="product-item__figure"><img
                                                        data-v-9b406082=""
                                                        src="{{ !empty($product->image->thumb) ? asset($product->image->thumb) : '' }}"
                                                        alt="{{ $product->name }}">
                                            </figure>
                                        </a>
                                        <div data-v-9b406082="" class="product-item__content">
                                            <div data-v-9b406082="" class="product-item__category-title">
                                                {{ $product->categories->first()->name ?? '' }}
                                            </div>
                                            <a href="{{ route('fr_product_detail' . $prefixLang, ['slug' => $product->slug, 'id' => $product->id]) }}"
                                               class="product-item__title">{{ $product->name }}</a>
                                            @if (!empty($product->product->properties_description))
                                            <ul data-v-9b406082="" class="product-item__specifications content-ul">
                                                @php
                                                    $propertiesDescription = json_decode($product->product->properties_description, true);
                                                @endphp
                                                @foreach ($propertiesDescription as $item)
                                                <li data-v-9b406082="" class="product-item__specification">
                                                    {{ $item['value'] ?? '' }}
                                                </li>
                                                @endforeach
                                            </ul>
                                            @endif
                                            <a data-v-9b406082=""
                                               href="{{ route('fr_product_detail' . $prefixLang, ['slug' => $product->slug, 'id' => $product->id]) }}"
                                               class="product-item__link"><span data-v-9b406082=""
                                                                                class="link-icon icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                                                View product
                                            </a></div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                            @include('frontend.includes.paging', ['pagination' => $products])
                        </div>
                        @endif
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection