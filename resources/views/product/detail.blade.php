@extends('frontend.layouts.master', ['seoData' => $seoData, 'ogType' => 'article'])
@section('title_head')
    {{ empty($seoData) ? $productDetail->name : $seoData->name }}
@endsection

@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- ==============================================
    **Product Listing**
    =================================================== -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home') }}">{{ trans('home_page') }}</a>
                         » <span><a
                                    href="{{ route('fr_product_list') }}">{{ trans('message.product_page') }}</a>
                                @if (!empty($productDetail->categoryChild->first()))
                                » <span><a href="{{ route('product_category', $productDetail->categoryChild->first()->parent->slug) }}">{{ $productDetail->categoryChild->first()->parent->name }}</a>
                                » <span><a href="{{ route('product_category', $productDetail->categoryChild->first()->slug) }}">{{ $productDetail->categoryChild->first()->name }}</a>
                                @endif
                                    » <span
                                            class="breadcrumb_last"
                                            aria-current="page">{{ $productDetail->name }}</span></span></span></span></span></span>
                    </p></div>
            </div>
        </div>
    </div>
    <div class="block block--padding block--grey-lightest">
        <div class="wrap">
            <div class="product-details">
                <div class="product-details__column product-details__column--images">
                    <div class="gallery">
                        <div class="gallery__item">
                            <div class="gallery__thumbnails js-gallery">
                                <a href="{{ asset($productDetail->image->origin) }}" class="gallery__thumbnail gallery__thumbnail--large">
                                    <figure class="gallery__image" role="group">
                                        <img src="{{ asset($productDetail->image->origin) }}" class="attachment-square-image size-square-image" alt="{{ $productDetail->name }}"></figure>
                                </a>
                                @if (!empty($galleries))
                                    <?php $slideIndex = 1; ?>
                                    @foreach($galleries as $item)
                                        <?php
                                        $img = $item->image;
                                        ?>
                                            <a href="{{ asset($img->origin) }}" class="gallery__thumbnail ">
                                                <figure class="gallery__image" role="group">
                                                    <img width="70" height="70" src="{{ asset($img->thumb) }}" class="attachment-brand-logo-small size-brand-logo-small" alt="{{ $productDetail->name }}" loading="lazy" data-object-fit="cover"></figure>
                                            </a>
                                        <?php
                                        $slideIndex++;
                                        ?>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="gallery__item gallery__item--large">
                            <a href="{{ asset($productDetail->image->origin) }}" class="gallery__holder js-image">
                                <figure class="gallery__figure" role="group">
                                    <figure class="gallery__image" role="group">
                                        <img width="640" height="640" src="{{ asset($productDetail->image->origin) }}" class="attachment-square-image size-square-image" alt="{{ $productDetail->name }}"></figure>
                                </figure>
                            </a>
                        </div>

                    </div>

                </div>


                <div class="product-details__column">
                    @if (!empty($productDetail->product->partner))
                    <a class="product-details__category-title link-alt" href="{{ route('product_list_by_producer', ['slug_producer' => $productDetail->product->partner->slug, 'id'=> $productDetail->product->partner->id]) }}">{{ $productDetail->product->partner->name }}</a>
                    @endif

                    <h1 class="product-details__title h2">{{ $productDetail->name }}</h1>

                    <p class="product-details__short-description">{{ nl2br($productDetail->description) }}</p>

                    <div class="product-details__links">

                        <button class="product-details__button button js-offer-button">
						<span class="button__inner">{{ trans('message.request_an_offer') }}</span>
                        </button>

                        <a href="#tab-description" class=" product-details__read-more read-more js-read-more">{{ trans('message.read_full_description') }}</a>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="block block--padding block--grey-lightest">

        <div class="wrap">

            <div class="block__inner">

                <div class="tabs js-woocommerce-tabs">

                    <ul class="tabs__tabs js-tabs" role="tablist">
                        <li class="tabs__tab" role="presentation">
                            <a href="#tab-specifications" class="tabs__link js-tab" data-tabby-default="" id="tabby-toggle_tab-specifications" role="tab" aria-controls="tab-specifications" aria-selected="true">{{ trans('message.specifications') }}</a>
                        </li>

                        <li class="tabs__tab" role="presentation">

                            <a href="#tab-description" class="tabs__link js-tab" id="tabby-toggle_tab-description" role="tab" aria-controls="tab-description" aria-selected="false" tabindex="-1">{{ trans('message.description') }}</a>

                        </li>

                        <li class="tabs__tab" role="presentation">

                            <a href="#tab-downloads" class="tabs__link js-tab" id="tabby-toggle_tab-downloads" role="tab" aria-controls="tab-downloads" aria-selected="false" tabindex="-1">{{ trans('message.download') }}</a>
                        </li>
                    </ul>
                    <div class="tabs__panes js-panes">
                        <div id="tab-specifications" class="tabs__pane js-pane" role="tabpanel" aria-labelledby="tabby-toggle_tab-specifications">
                            <div class="notice notice--warning">
                                <div class="notice__inner">
                                    <div class="notice__icon-container">
									<span class="notice__icon icon"><i class="fa fa-info" aria-hidden="true"></i></span>
                                    </div>
                                    <span class="notice__label">{{ trans('message.price_and_ship_time') }}</span>
                                </div>
                            </div>
                            <div class="zebra-table">
                                @if (!empty($productDetail->product->properties))
                                <table class="zebra-table__table">
                                    <tbody>
                                    @php
                                        $properties = json_decode($productDetail->product->properties, true);
                                    @endphp
                                    @foreach ($properties as $row)
                                    <tr>
                                        <th>{{ $row['name'] ?? '' }}</th>
                                        <td>{{ $row['value'] ?? '' }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody></table>
                                @endif
                            </div>
                        </div>
                        <div id="tab-description" class="tabs__pane js-pane" role="tabpanel" aria-labelledby="tabby-toggle_tab-description" hidden="hidden">
                            {!! $productDetail->content !!}
                        </div>
                        <div id="tab-downloads" class="tabs__pane js-pane" role="tabpanel" aria-labelledby="tabby-toggle_tab-downloads" hidden="hidden">
                            <div class="zebra-table">
                                <table class="zebra-table__table">

                                    <tbody><tr>
                                        <th>Brochure</th>
                                        <td>
                                            <a class="product-file-download" href="#" download="" target="_blank">{{ $productDetail->name }}</a>
                                        </td>
                                    </tr>

                                    </tbody></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('product.includes.request_offer', ['productDetail' => $productDetail])
    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>

@endsection

@section('css_page')

@endsection
@section('script_page')
    <script>
        // Ajax Order
        $(document).on('submit', '#form-order', function (e) {
            e.preventDefault();
            var form = $(this);
            var successForm = form.find('#success-form');
            successForm.html('');
            successForm.hide();

            console.log(form.find('input[name="product_id"]').val());
            console.log(form.find('input[name="product_name"]').val());

            var inputs = [
                { name: 'name', element: form.find('#error-name') },
                { name: 'message', element: form.find('#error-message') },
                { name: 'email', element: form.find('#error-email') },
                { name: 'address', element: form.find('#error-address') },
                { name: 'phone_number', element: form.find('#error-phone_number') },
                { name: 'amount', element: form.find('#error-amount') },
            ];
            clearErrorInputs(inputs);

            var amount = form.find('input[name="amount_other"]').val()
                ? form.find('input[name="amount_other"]').val()
                : form.find('input[name="amount"]:checked').val() ? form.find('input[name="amount"]:checked').val() : 1;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: {
                    name: form.find('input[name="name"]').val(),
                    message: form.find('textarea[name="message"]').val(),
                    email: form.find('input[name="email"]').val(),
                    address: form.find('input[name="address"]').val(),
                    phone_number: form.find('input[name="phone_number"]').val(),
                    amount: amount,
                    product_id: form.find('input[name="product_id"]').val(),
                    product_name: form.find('input[name="product_name"]').val()
                },
                success: function (response) {
                    if(response.success) {
                        successForm.html(response.success);
                        successForm.show();
                        form.trigger('reset');
                    }
                },
                error: function (err) {
                    var response = JSON.parse(err.responseText);
                    showErrorInputs(response, inputs);
                }
            });
        });
    </script>
@endsection