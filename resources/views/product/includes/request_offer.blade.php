<div class="block block--padding block--grey-lightest">
    <div class="wrap">
        <div class="box offer-request js-request-offer">
            <div class="block__header">

                <h2 class="offer-request__title">{{ trans('message.request_an_offer') }}</h2>

                <p class="offer-request__text">{{ trans('message.request_offer_reply') }}</p>

            </div>


            <div class="block__body">

                <div class="gf_browser_chrome gform_wrapper offer_form_wrapper">
                    <form method="post" enctype="multipart/form-data" id="form-order"
                          class="offer_form"  action="{{ route('contact_order_' . $lang) }}">
                        <div id="success-form" class="message__form message__form--success"></div>
                        <div class="gform_body">
                            <ul class="gform_fields top_label form_sublabel_below description_below">
                                <li class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible gfield--html">
                                    <h3>1. {{ trans('message.product_request') }}</h3></li>
                                <li class="gfield gform_hidden field_sublabel_below field_description_below gfield_visibility_visible gfield--hidden">
                                    <input name="input_15" id="input_3_15" type="hidden" class="gform_hidden"
                                           aria-invalid="false" value="Prisma DI-5/ DI-5C"></li>
                                <li class="gfield gform_hidden field_sublabel_below field_description_below gfield_visibility_visible gfield--hidden">
                                    <input name="input_16" id="input_3_16" type="hidden" class="gform_hidden"
                                           aria-invalid="false" value="Diagnostic, Meters"></li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--radio">
                                    <label class="gfield_label">{{ trans('message.qty') }}<span class="gfield_required">*</span></label>
                                    <div class="ginput_container ginput_container_radio">
                                        <ul class="gfield_radio" id="input_3_18">
                                            <li>
                                                <input name="amount" type="radio" value="1" id="amount_1" tabindex="1">
                                                <label for="amount_1">1</label>
                                            </li>
                                            <li>
                                                <input name="amount" type="radio" value="2" id="amount_2" tabindex="2">
                                                <label for="amount_2">2</label>
                                            </li>
                                            <li>
                                                <input name="amount" type="radio" value="3" id="amount_3" tabindex="3">
                                                <label for="amount_3">3</label>
                                            </li>
                                            <li>
                                                <input name="amount" type="radio" value="4" id="amount_4" tabindex="4">
                                                <label for="amount_4">4</label>
                                            </li>
                                            <li>
                                                <input name="amount" type="radio" value="5" id="amount_5" tabindex="5">
                                                <label for="amount_5">5</label>
                                            </li>

                                            <li>
                                                <div class="gfield_radio__other-separator">or</div>
                                                <input name="amount" type="radio" value="other" tabindex="6"
                                                       onfocus="$(this).next('input').focus();">
                                                <input id="amount_other" name="amount_other" type="number" tabindex="6">
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="error-mount" class="message__error"></div>
                                </li>
                                <li class="gfield field_sublabel_below field_description_below gfield_visibility_visible gfield--textarea">
                                    <label class="gfield_label" for="message">{{ trans('message.remarks') }}</label>
                                    <div class="ginput_container ginput_container_textarea">
                                        <textarea name="message" id="message" class="textarea medium" tabindex="7"
                                            placeholder="{{ trans('message.question_or_remarks') }}"
                                            aria-invalid="false" rows="10" cols="50"></textarea>
                                    </div>
                                </li>
                            </ul>
                            <ul class="gform_fields">
                                <li class="gfield gfield_html gfield_html_formatted gfield_no_follows_desc field_sublabel_below field_description_below gfield_visibility_visible gfield--html">
                                    <h3>2. {{ trans('message.your_information') }}</h3></li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--name">
                                    <label class="gfield_label" for="name">
                                        {{ trans('message.full_name') }}<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="text" name="name" id="name" />
                                    </div>
                                    <div id="error-name" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--email">
                                    <label class="gfield_label" for="email">
                                        Email<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="email" name="email" id="email" />
                                    </div>
                                    <div id="error-email" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--text">
                                    <label class="gfield_label" for="address">
                                        {{ trans('message.address') }}<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="text" name="address" id="address" />
                                    </div>
                                    <div id="error-address" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--text">
                                    <label class="gfield_label" for="phone_number">
                                        {{ trans('message.phone') }}<span class="gfield_required">*</span>
                                    </label>
                                    <div class="ginput_container">
                                        <input type="text" name="phone_number" id="phone_number" />
                                    </div>
                                    <div id="error-phone_number" class="message__error"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="gform_footer top_label">
                            <button type="submit" class="button">
                                <span class="button__inner"><span class="button__label">{{ trans('message.submit') }}</span></span>
                            </button>
                            <input type="hidden" name="product_id" value="{{ $productDetail->id }}" />
                            <input type="hidden" name="product_name" value="{{ $productDetail->name }}" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>