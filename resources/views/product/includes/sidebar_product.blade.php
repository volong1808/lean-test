<div class="sidebar">
    <div class="box">
        <div class="cate-sb">
            <h5><i class="mcn-ic"></i>{{ trans('message.category') }}</h5>
            @if (!empty($productMenu))
                <ul class="lst-cate custom-active">
                    @foreach ($productMenu as $item)
                        <li>
                            @if ($item->subCategory->count() > 0)
                                <span class="js-show-sub-sidebar-product" data-content="#sub-sidebar-product-{{ $item->id }}"></span>
                            @endif
                            <a href="{{ route('product_list_sub' . $prefixLang, $item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a>
                            @if ($item->subCategory->count() > 0)
                                <ul class="sub-menu-product" id="sub-sidebar-product-{{ $item->id }}">
                                    @foreach ($item->subCategory as $sub)
                                        <li>
                                            <a href="{{ route('product_list_sub' . $prefixLang, $sub->slug) }}"
                                               title="{{ $sub->name }}">{{ $sub->name }}</a>
                                        </li>
                                    @endforeach

                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>

    <div class="box">
        <div class="pr-new-sb">
            <div class="mcn-title">
                <h2><span>{{ trans('message.product_new') }}</span></h2>
            </div>
            @if (!empty($newProducts))
            <ul class="lst_">
                @foreach ($newProducts as $product)
                    <?php
                    $image = 'uploads/feedback/man-2.png';
                    if (!empty($product->image)) {
                        $image = $product->image->thumb;
                    }
                    ?>
                    <li>
                        <div class="detail">
                            <div class="img_">
                                <a href="{{ route('product_detail' . $prefixLang, $product->slug) }}"
                                   class="img"
                                   title="{{ $product->name }}">
                                    <img src="{{ asset($image) }}"
                                         alt="{{ $product->name }}"
                                         title="{{ $product->name }}">
                                </a>
                            </div>
                            <div class="txt_">
                                <h4><a href="{{ route('product_detail' . $prefixLang, $product->slug) }}"
                                            class="titA-1">{{ $product->name }}</a></h4>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            @endif
        </div>
    </div>


    <div class="box sb-support">
        <h5><i class="mcn-ic ic-cate"></i>{{ trans('message.contact_us') }}</h5>
        <ul>
            <p><i class="fa fa-user"></i>{{ $company['name'] }}</p>
            <p><i class="fa fa-phone"></i>Phone: {{ $company['phone'] }} </p>
            <p><i class="fa fa-envelope"></i>{{ $company['email'] }}</p>
        </ul>
    </div>





</div>