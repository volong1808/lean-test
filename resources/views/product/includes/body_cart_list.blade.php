<table class="table table-bordered">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">{{ trans('message.product_name') }}</th>
        <th scope="col">{{ trans('message.qty') }}</th>
        <th scope="col">{{ trans('message.unit_price') }}</th>
        <th scope="col">{{ trans('message.line_total') }}</th>
        <th scope="col">{{ trans('message.delete') }}</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1 ?>
    @foreach ($cart as $item)
        <tr>
            <?php
            $price = trans('message.contact');
            $totalItem = trans('message.contact');
            if ($item['product']->price > 0) {
                $price = number_format($item['product']->price);
                $totalItem = $item['amount'] * $item['product']->price;
                $totalItem = number_format($totalItem);
            }

            ?>
            <th scope="row">{{ $i }}</th>
            <td>{{ $item['product']->name }}</td>
            <td><input type="number" style="width: 100px"
                       class="form-control js-change-amount"
                       data-token="{{ csrf_token() }}"
                       data-url="{{ route('change_amount_item' . $prefixLang, $item['product']->id) }}"
                       value="{{ $item['amount'] }}"></td>
            <td>{{ $price }}</td>
            <td>{{ $totalItem }}</td>
            <td><a href="{{ route('delete_cart_item' . $prefixLang, $item['product']->id) }}" class="btn btn-sm btn-danger"><i
                            class="fa fa-trash-o" aria-hidden="true"></i></a>
            </td>
        </tr>
        <?php $i++ ?>
    @endforeach
    <tr>
        <td colspan="4" class="text-right text-danger">{{ trans('message.total') }}</td>
        <?php
        $totalCart = getTotalPriceCart($cart);
        if ($totalCart > 0) {
            $totalCart = number_format($totalCart);
        } else {
            $totalCart = trans('message.contact');
        }
        ?>
        <td class="text-danger" colspan="2">{{ $totalCart }}</td>
    </tr>
    </tbody>
</table>