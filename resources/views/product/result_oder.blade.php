@extends('frontend.layouts.master')
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="bg-fff">
        <div class="container">
            <div id="map-link-bar">
                <ul>
                    <li><a href="{{ route('home') }}" title="{{ trans('message.home_page') }}">{{ trans('message.home_page') }}</a></li>
                    <li><a href="{{ route('fr_product_list') }}" title="{{ $page->name }}">{{ $page->name }}</a></li>
                    <li class="active">{{ $name }}</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="bg-fff main-product">

        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="title">
                        <div class="titH-4 border-bottom-sm">{{ $name }}</div>
                    </div>
                    <div class="cart-detail form-contact">
                        @if (!empty($cart))
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2" id="content-list-order">
                                    <div class="billing-detail">
                                        <div class="row">
                                            <div class="col-md-6"><trong>Đơn Hàng Số:</trong>{{ $orderDetail->code }}</div>
                                            <div class="col-md-6"><trong>Ngày:</trong> {{ $orderDetail->created_at }}</div>
                                            <div class="col-md-6"><trong>Tên Người Nhận:</trong>{{ $orderDetail->name }}</div>
                                            <div class="col-md-6"><trong>Số Điện Thoại:</trong> {{ $orderDetail->phone_number }}</div>
                                            <div class="col-md-6"><trong>Địa Chỉ:</trong> {{ $orderDetail->address }}</div>
                                        </div>
                                    </div>
                                    @include('product.includes.body_cart_block')
                                    <div class="billing-detail">
                                        <div class="row">
                                            <div class="col-md-12 danger"><trong>Ghi Chú:</trong>{{ $orderDetail->message }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
@endsection

@section('script_page')
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('input', '.js-change-amount', function () {
                let url = $(this).attr('data-url');
                let token = $(this).attr('data-token');
                let amount = $(this).val();
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        amount: amount,
                        _token: token
                    }
                }).done(function(res) {
                   if (res.status == 200) {
                       $('#content-list-order').html(res.tableCart);
                   }
                });
            });
            $('#js-show-content-payment-type').change(function () {
                let paymentType = $(this).val();
                $('.content-payment-type li').hide();
                $('#' + paymentType).show();
            })
        });
    </script>
@endsection