@extends('user.master')
@section('title_page')
    Đăng Nhập
@endsection
@section('content')
    <section class="login-outer">
        <div class="content-area">
            <div class="login-form-holder">
                <div class="inner">
                    <div class="login-form">
                        <h3>Đăng Nhập</h3>
                        <form action="{{ route('front_en_login_post') }}" method="post">
                            {{ csrf_field() }}
                            @if(session('errorLogin'))
                                <div class="alert alert-danger">
                                    <strong>Lỗi!</strong>  {{ session('errorLogin') }}
                                </div>
                            @endif
                            @if(session('resetSuccess'))
                                <div class="alert alert-success">
                                    <strong>Thành công!</strong>  {{ session('resetSuccess') }}
                                </div>
                            @endif
                            @if(session('successRegister'))
                                <div class="alert alert-success">
                                    <strong>Thành công!</strong>  {{ session('successRegister') }}
                                </div>
                            @endif
                            <div class="mail">
                                <label>Tên Đăng Nhập<sup class="red-text">*</sup></label>
                                @if ($errors->has('username'))
                                    <label class="red-text">{{ $errors->first('username') }}</label>
                                @endif
                                <input name="username" value="{{ $errors->any() ? old('username') : '' }}" required placeholder="Nhập Tên Đăng Nhập" type="text">
                            </div>
                            <div class="password">
                                <label>Mật Khẩu<sup class="red-text">*</sup></label>
                                @if ($errors->has('password'))
                                    <label class="red-text">{{ $errors->first('password') }}</label>
                                @endif
                                <input name="password" required placeholder="" type="password">
                            </div>
                            <div class="forgot">
                                <label>
                                    <input value="1" @if (old('remember') == 1) checked @endif name="remember" type="checkbox">
                                    <span>Ghi Nhớ Đăng Nhập</span></label>
                                    @if ($errors->has('remember'))
                                        <label class="red-text">{{ $errors->first('remember') }}</label>
                                    @endif
                                <a href="{{ route('front_en_reset') }}"><span class="q-mark">?</span>Quên Mật Khẩu</a> </div>
                            <button type="submit" class="btn login-btn">Đăng Nhập</button>
                        </form>
                    </div>
                </div>
                <div class="box-hav-accnt">
                    <p>Bạn chưa có tài khoản? <a href="{{ route('front_en_register') }}">Đăng Ký</a></p>
                </div>
            </div>
        </div>
    </section>
@endsection