@extends('user.master')
@section('title_page')
    Quên Mật Khẩu
@endsection
@section('content')
    <section class="login-outer">
        <div class="content-area">
            <div class="login-form-holder">
                <div class="inner">
                    <div class="login-form">
                        <h3>Quên Mật Khẩu</h3>
                        <form action="{{ route('front_en_reset_post') }}" method="post">
                            {{ csrf_field() }}
                            @if(session('errorReset'))
                                <div class="alert alert-danger">
                                    <strong>Lỗi!</strong>  {{ session('errorReset') }}.
                                </div>
                            @endif
                            <div class="username">
                                <label>Tên Đăng Nhập<sup class="red-text">*</sup></label>
                                @if ($errors->has('username'))
                                    <label class="red-text">{{ $errors->first('username') }}</label>
                                @endif
                                <input name="username" value="{{ $errors->any() ? old('username') : '' }}" required placeholder="Nhập Tên Đăng Nhập" type="text">
                            </div>
                            <div class="mail">
                                <label>Địa Chỉ Email<sup class="red-text">*</sup></label>
                                @if ($errors->has('email'))
                                    <label class="red-text">{{ $errors->first('email') }}</label>
                                @endif
                                <input name="email" value="{{ $errors->any() ? old('email') : '' }}" required placeholder="Nhập Địa Chỉ Email" type="email">
                            </div>
                            <div class="forgot">
                                <a href="#"><span>Nhập email đăng ký để mật khẩu được gữi về mail.</span></a> </div>
                            <button type="submit" class="btn login-btn">Khôi Phục Mật Khẩu</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection