@extends('user.master')
@section('title_page')
    Đăng Ký
@endsection
@section('content')
<section class="login-outer">
    <div class="content-area">
        <div class="login-form-holder">
            <div class="inner">
                <div class="login-form">
                    <h3>Đăng Ký Ngay</h3>
                    <form method="post" action="{{ route('front_en_register_post') }}">
                        {{ csrf_field() }}
                        <div class="user-name">
                            <label>Tên<sup class="red-text">*</sup></label>
                            @if ($errors->has('name'))
                                <label class="red-text">{{ $errors->first('name') }}</label>
                            @endif
                            <input name="name" value="{{ $errors->any() ? old('name') : '' }}" required placeholder="Nhập Tên Đầy Đủ" type="text">
                        </div>
                        <div class="user-name">
                            <label>Tên Đăng Nhập<sup class="red-text">*</sup></label>
                            @if ($errors->has('username'))
                                <label class="red-text">{{ $errors->first('username') }}</label>
                            @endif
                            <input name="username" value="{{ $errors->any() ? old('username') : '' }}" required placeholder="Nhập Tên Đăng Nhập" type="text">
                        </div>
                        <div class="mail">
                            <label>Địa Chỉ Email<sup class="red-text">*</sup></label>
                            @if ($errors->has('email'))
                                <label class="red-text">{{ $errors->first('email') }}</label>
                            @endif
                            <input name="email" value="{{ $errors->any() ? old('email') : '' }}" required placeholder="Nhập Địa Chỉ Email" type="email">
                        </div>
                        <div class="mail">
                            <label>Điện Thoại<sup class="red-text">*</sup></label>
                            @if ($errors->has('phone'))
                                <label class="red-text">{{ $errors->first('phone') }}</label>
                            @endif
                            <input name="phone" value="{{ $errors->any() ? old('phone') : '' }}" required placeholder="Nhập Số Điện Thoại" type="text">
                        </div>
                        <div class="password">
                            <label>Mật Khẩu<sup class="red-text">*</sup></label>
                            @if ($errors->has('password'))
                                <label class="red-text">{{ $errors->first('password') }}</label>
                            @endif
                            <input name="password" required placeholder="" type="password">
                        </div>
                        <div class="confirm-password">
                            <label>Nhập Lại Mật Khẩu</label>
                            @if ($errors->has('password_confirmation'))
                                <label class="red-text">{{ $errors->first('password_confirmation') }}</label>
                            @endif
                            <input name="password_confirmation" required placeholder="" type="password">
                        </div>
                        <div class="terms">
                            <label>
                                <input required value="" type="checkbox">
                                <span>Tôi đồng ý tạo tài khoản.</span></label>
                        </div>
                        <div class="login-btn-sec">
                            <button class=" btn login-btn">Đăng Ký</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="box-hav-accnt">
                <p>Bạn đã có tài khoản? <a href="{{ route('front_en_login') }}">Đăng Nhập</a></p>
            </div>
        </div>
    </div>
</section>
@endsection