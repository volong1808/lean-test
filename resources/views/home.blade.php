@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('content')
    @php
        $banner['title'] = $banner['title'] = trans('message.product_page');
        $banner['image'] =  !empty($page->image->origin) ? asset($page->image->origin) : asset('images/banner.jpg');
    @endphp
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <div class="category-filter">
        <div class="wrap">
            <div class="category-filter__inner">
                <div class="category-filter__overlay"></div>
                <a class="category-toggle" href="{{ route('product_category_all') }}">
                    <div class="category-toggle__inner">
                        <span class="category-toggle__label">{{ trans('message.category_view_all') }}</span>
                        <span class="category-toggle__icon icon"><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </div>
                </a>
                <ul class="category-filter__items">
                    @if ($categories->count() >0)
                        @foreach ($categories as $category)
                            <li class="category-filter__item">
                                <a href="{{ route('product_category' . $prefixLang, ['slug_cate' => $category->slug]) }}" class="category-filter__link">{{ $category->name }}</a>
                            </li>
                        @endforeach
                    @endif
                </ul>
                <a href="{{ route('fr_product_list' . $prefixLang) }}" class="button category-filter__button">
                    <div class="button__inner">
                        <div class="button__label">
                            {{ trans('message.product_view_all') }}
                        </div>
                        <div class="button__icon icon">
                            <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </a>

            </div>

        </div>
    </div>
    @include('frontend.includes.branch')
    <div class="block block--padding block--pattern">
        <div class="wrap">
            <div class="block__inner">
                <div class="block__header block__header--center">
                    <h2 class="block__title">{{ trans('message.service') }}</h2>
                    <span class="block__subtitle subtitle">{{ trans('message.service_description') }}</span>
                </div>
                <div class="block__body">
                    @if ($topService->count() > 0)
                    <div class="service">
                        @foreach ($topService as $service)
                        <div class="service__item">
                            <a href="{{ route('fr_service_detail' . $prefixLang, ['slug' => $service->slug]) }}" class="service-item">
                                <div class="service-item__header">
                                    <div class="service-item__icon-outer">
                                        <div class="service-item__icon-inner">
                                            <div class="service-item__icon icon">
                                                <img src="{{ !empty($service->image->origin) ? asset($service->image->origin) : '' }}" alt="{{ $service->name }}">
                                            </div>
                                        </div>
                                    </div>
                                    <h3 class="service-item__title">{{ $service->name }}</h3>
                                    <span class="service-item__arrow icon"><svg id="ico-arrow-right" viewBox="0 0 16 15.84"><path d="M0 11V4.89h8.08V0L16 7.92l-7.92 7.92V11z" fill="currentColor"></path></svg></span>
                                </div>
                                <div class="service-item__body">
                                    <p class="service-item__excerpt">{{ $service->description }}</p>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="block block--padding block--grey-lightest">
        <div class="wrap">
            <div class="block__inner">
                <div class="block__body">
                    @if ($topField->count() > 0)
                    <div class="branche">
                        @foreach ($topField as $field)
                        <div class="branche__item">
                            <a href="{{ route('fr_field_detail' . $prefixLang, $field->slug ) }}" target="" class="branche-item">
                                <figure class="branche-item__image">
                                    <img width="640" height="640"
                                         src="{{ !empty($field->image->origin) ? asset($field->image->origin) : '' }}"
                                         class="attachment-square-image size-square-image" alt="{{ $field->name }}"
                                         loading="lazy">
                                </figure>
                                <div class="branche-item__content">
                                    <div class="branche-item__label label">
                                        <span class="label__inner">{{ trans('message.field') }}</span>
                                    </div>
                                    <span class="branche-item__icon icon"></span>
                                    <h3 class="branche-item__title">{{ $field->name }}</h3>
                                </div>
                                <div class="branche-item__overlay"></div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>


                <div class="block__footer">

                    <a href="{{ route('fr_field_list' . $prefixLang) }}" target="" class="button">
                        <div class="button__inner">
                            <div class="button__label">
                                {{ trans('message.field_view_all') }}
                            </div>
                            <div class="button__icon icon">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                            </div>
                        </div>
                    </a>

                </div>


            </div>

        </div>

    </div>
    <div class="block block--padding block--grey-lightest">

        <div class="content-highlight">

            <div class="wrap">

                <div class="content-highlight__inner">

                    <div class="content-highlight__content">

                        <div class="content-highlight__content-inner box">

                            <h2 class="content-highlight__title">{{ $about->name }}</h2>
                            <span class="content-highlight__subtitle subtitle">{{ trans('message.about_description') }}</span>

                            <div class="content-highlight__text">
                                {!! $about->description !!}
                            </div>

                            <a href="{{ route('page', ['slug' => trans('message.about_slug')]) }}" target="" class="content-highlight__link">
								<span class="link-label">
                                    {{ trans('message.view_more_about') }}
                                </span>
                                <span class="link-icon icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>

                    <figure class="content-highlight__image">
                        <div class="content-highlight__overlay"></div>
                        <img width="1200" height="500"
                             src="{{ !empty($about->image->origin) ? asset($about->image->origin) : '' }}"
                             class="attachment-content-background-image size-content-background-image"
                             alt="{{ $about->name }}" loading="lazy" data-object-fit="cover"></figure>

                </div>

            </div>

        </div>

    </div>
    <div class="block block--padding block--grey-lightest">
        <div class="wrap">
            <div class="block__inner">
                <div class="block__header">
                    <h2 class="block__title">{{ trans('message.top_news') }}</h2>
                    <span class="block__subtitle">{{ trans('message.top_news_description') }}</span>
                </div>
                @if ($topNews->count() > 0)
                <div class="block__body">
                    <div class="post-items">
                        @foreach ($topNews as $new)
                        <div class="post-items__item">
                            <div class="post-item">
                                <div class="post-item__header">
                                    <div class="post-item__label label">
                                        <span class="label__inner">News</span>
                                    </div>
                                    <a href="{{ route('blog_detail' . $prefixLang, ['slug' => $new->slug]) }}"
                                       class="post-item__link">
                                        <figure class="post-item__image">
                                            <img width="450" height="263"
                                                 src="{{ !empty($new->image->thumb) ? asset($new->image->thumb) : '' }}"
                                                 class="attachment-post-item-image size-post-item-image wp-post-image"
                                                 alt="{{ $new->name }}" loading="lazy"
                                                 data-object-fit="cover">
                                            <div class="post-item__overlay"></div>
                                        </figure>
                                    </a>
                                </div>
                                <div class="post-item__body">
                                    <span class="post-item__date">{{ date('Y-M-d', strtotime($new->created_at)) }}</span>
                                    <a href="{{ route('blog_detail' . $prefixLang, ['slug' => $new->slug]) }}"
                                       class="post-item__link">
                                        <h3 class="post-item__title">{{ $new->name }}</h3>
                                    </a>
                                    <a href="{{ route('blog_detail' . $prefixLang, ['slug' => $new->slug]) }}"
                                       class="post-item__read-more">
			                            <span class="link-icon icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                                        <span class="post-item__label">{{ trans('message.new_view_more') }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    @include('frontend.includes.subscribe_new_letter')
@endsection
@section('script_page')
@endsection