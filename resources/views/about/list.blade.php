@extends('frontend.layouts.master')
@section('title_head')
    {{ $banner['title'] }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="bg-fff">
        <div class="container">
            <div id="map-link-bar">
                <ul>
                    <li><a href="{{ route('home') }}" title="{{ trans('message.home_page') }}">{{ trans('message.home_page') }}</a></li>
                    {{--                    <li><a href="{{ route('about') }}" title="{{ trans('message.about_page') }}">{{ trans('message.about_page') }}</a></li>--}}
                    <li class="active">{{ $name }}</li>
                </ul>
            </div>
        </div>
    </div>
    @if (!empty($aboutList))
        <div class="bg-fff" style="position:relative;">
            <div class="container">
                <div id="sticky-wrapper" class="sticky-wrapper"><div class="nav-scroll">
                        <div class="box">
                            <ul>
                                @foreach ($aboutList as $item)
                                    <li>
                                        <a href="{{ route('about_detail', $item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div></div>
            </div>
        </div>
        <!-- end about menu-->
    @endif
    <!-- content -->
    <div class="container">
        <div class="title">
            <div class="titH-4 border-bottom-double">{{ $page->name }}</div>
        </div>
        <div class="_detail-news">
            <div class="_detail">
                {!! $page->content !!}
            </div>
            <div class="btn-link-social">
                @include('frontend.includes.facebook_share')
                @include('frontend.includes.zalo')
            </div>
        </div>
    </div>
    <!-- end content-->
    <!-- end break crum
    @if (!empty($aboutList))
        @foreach ($aboutList as $key => $item)
            <?php
            $image = 'uploads/feedback/man-2.png';
            if (!empty($item->image)) {
                $image = $item->image->thumb;
            }
            ?>
            @if ($key%2 == 0)
                <div id="address-tag-{{ $key+1 }}" class="address-scroll">
                    <div class="box" style="background: url({{ asset($image) }}) center no-repeat">
                        <div class="container">
                            <div class="row">
                                <div class="box-o item-2">
                                    <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
                                        <div class="titH-3 sm-line-left" style="text-align: left">{{ $item->name }}</div>
                                        <div class="desc-o">
                                            {{ $item->description }}
                                        </div>
                                        <div class="btn-seemore">
                                            <a href="{{ route('about_detail', $item->slug) }}" title="{{ trans('message.about_page') }} chung">{{ trans('message.view_more') }}<i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div id="address-tag-{{ $key+1 }}" class="address-scroll">
                    <div class="box" style="background: url({{ asset($image) }}) center no-repeat">
                        <div class="container">
                            <div class="row">
                                <div class="box-o item-2">
                                    <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                                        <div class="titH-3 sm-line-left">{{ $item->name }}</div>
                                        <div class="desc-o">
                                            {{ $item->description }}
                                        </div>
                                        <div class="btn-seemore">
                                            <a href="{{ route('about_detail', $item->slug) }}" title="{{ trans('message.about_page') }} chung">{{ trans('message.view_more') }}<i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endif
    <!-- footer -->
@endsection