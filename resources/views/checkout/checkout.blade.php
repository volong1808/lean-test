@extends('frontend.layouts.master')
@section('title_head')
    {{ $banner['title'] }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <?php
    $name = null;
    $imageThumb = config('constants.product_image.PLACE_HOLDER');
    if (!empty($productDetail->post)) {
        $post = $productDetail->post;
        $name = $post->name;
        if (!empty($post->image)) {
            $image = $post->image;
            $imageThumb = $image->thumb;
        }
    }
    ?>
    <!-- ==============================================
    **Cart**
    =================================================== -->
    <section class="padding-lg">
        <form action="{{ route('checkout_post', $planDetail->id) }}" method="post">
            @csrf
            <div class="container">
                @if(session('checkoutSuccess'))
                    <div class="alert alert-success">
                        <strong>Thành công!</strong>  {{ session('checkoutSuccess') }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger">
                        <strong>Lỗi!</strong>  Đã sảy ra lỗi khi thao tác.
                    </div>
                @endif
                <div class="row">
                    <!-- Start Left -->
                    <div class="col-lg-8">
                        <div class="cart-table checkout-table table-responsive mb-0">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>{{ trans('message.product_page') }}</th>
                                    <th>Gói</th>
                                    <th>Giá Tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <table class="table product-name">
                                            <tr>
                                                <td>
                                                    <figure><img src="{{ asset($imageThumb) }}" alt=""></figure>
                                                </td>
                                                <td><p>{{ $name }}</p></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <div class="item-qty">
                                            <select class="custom_select package-select">
                                                @foreach ($planList as $planItem)
                                                    <?php
                                                    $planName = null;
                                                    if (!empty($planItem->post)) {
                                                        $planName = $planItem->post->name;
                                                    }
                                                    ?>
                                                    <option value="{{ route('checkout', $planItem->id) }}"
                                                            @if ($planItem->id == $planDetail->id) selected @endif>
                                                        {{ $planName }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td>{{ number_format($planDetail->price, 0, ',', '.') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="pay-outer">
                            @if (!empty($bankingOption) && !empty($bankingConfig))
                                <?php
                                $checkDefault = config('constants.PAYMENT_DEFAULT');
                                ?>
                                @foreach ($bankingConfig as $bankingItem)
                                    <div class="check-pay">
                                        <div class="radio-outer">
                                            <input type="radio" @if ($checkDefault == $bankingItem['inputName']) checked
                                                   @endif
                                                   data-note="#note-{{ $bankingItem['inputName'] }}"
                                                   class="js-choose-payment"
                                                   value="{{ $bankingItem['inputName'] }}"
                                                   name="banking_type" id="{{ $bankingItem['inputName'] }}">
                                            <label for="{{ $bankingItem['inputName'] }}">{{ $bankingItem['label'] }}</label>
                                        </div>
                                        @if (!empty($bankingOption[$bankingItem['inputName']]))
                                            <div class="payment-box {{ ($checkDefault == $bankingItem['inputName']) ? 'active' : '' }}"
                                                 id="note-{{ $bankingItem['inputName'] }}">
                                                <p>{!! nl2br($bankingOption[$bankingItem['inputName']]) !!}</p>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                            @if ($errors->has('banking_type'))
                                <label class="red-text">{{ $errors->first('banking_type') }}</label>
                            @endif
                        </div>
                        <div class="shipping-address">
                            <h2>Thông Tin Khách Hàng</h2>
                            <div class="contact-form-wrapper checkout-form">
                                <div class="row">
                                    <div class="col-md-6 input-col">
                                        <label>Tên<span>*</span></label>
                                        <input name="name" type="text" placeholder="Nhập Tên Khách Hàng" value="{{ $errors->any() ? old ('name') : $userInfo->name }}">
                                        @if ($errors->has('name'))
                                            <label class="red-text">{{ $errors->first('name') }}</label>
                                        @endif
                                    </div>
                                    <div class="col-md-6 input-col">
                                        <label>Điện Thoại<span>*</span></label>
                                        <input name="phone" type="text" placeholder="Nhập Số Điện Thoại" value="{{ $errors->any() ? old ('phone') : $userInfo->phone }}">
                                        @if ($errors->has('phone'))
                                            <label class="red-text">{{ $errors->first('phone') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 input-col">
                                        <label>Email</label>
                                        <input name="email" type="text" placeholder="Nhập Email"  value="{{ $errors->any() ? old ('email') : $userInfo->email }}">
                                        @if ($errors->has('email'))
                                            <label class="red-text">{{ $errors->first('email') }}</label>
                                        @endif
                                    </div>
                                    <div class="col-md-6 input-col">
                                        <label>Địa Chỉ</label>
                                        <input name="address" type="text" placeholder="Nhập Địa Chỉ" value="{{ $errors->any() ? old ('address') : $userInfo->address }}">
                                        @if ($errors->has('address'))
                                            <label class="red-text">{{ $errors->first('address') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 input-col">
                                        <label>Ghi Chú</label>
                                        <textarea name="message" placeholder="Nhập Nội Dung Cần Ghi Chú">
                                            {{ $userInfo->message }}
                                        </textarea>.
                                        @if ($errors->has('message'))
                                            <label class="red-text">{{ $errors->first('message') }}</label>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Left -->

                    <!-- Start Right -->
                    <div class="col-lg-4">
                        <div class="checkout-right">
                            <div class="cart-total">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Tổng</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>{{ trans('message.product_page') }}</th>
                                        <td>{{ $name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ $planDetail->post->name }}</th>
                                        <td>{{ $planDetail->time }} Năm</td>
                                    </tr>
                                    <tr>
                                        <th>Tổng Tiền</th>
                                        <td>{{ number_format($planDetail->price, 0, ',', '.') }} VNĐ</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="btn-outer">
                                                <button type="submit" class="btn checkout-btn btn-block">Đặt Mua
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End Right -->
                </div>
            </div>
        </form>
    </section>

@endsection

@section('script_page')
    <script type="application/javascript" language="JavaScript">
        $(document).ready(function () {
            $('.package-select').change(function () {
                var changePackageUrl = $(this).val();
                window.location = changePackageUrl;
            });

            $('.js-choose-payment').change(function () {
                $('.payment-box').removeClass('active');
                if ($(this).is(':checked')) {
                    var contentNote = $(this).attr('data-note');
                    $(contentNote).addClass('active');
                }
            })
        });
    </script>
@endsection
