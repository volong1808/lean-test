@extends('frontend.layouts.master')
@section('title_head')
    {{ $banner['title'] }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <?php
    $name = null;
    $imageThumb = config('constants.product_image.PLACE_HOLDER');
    if (!empty($productDetail->post)) {
        $post = $productDetail->post;
        $name = $post->name;
        if (!empty($post->image)) {
            $image = $post->image;
            $imageThumb = $image->thumb;
        }
    }
    ?>
    <!-- ==============================================
    **Cart**
    =================================================== -->
    <section class="padding-lg">
        <form action="{{ route('banking_online_post', $order->code) }}" method="post">
            @csrf
            <div class="container">
                @if(session('paymentSuccess'))
                    <div class="alert alert-success">
                        <strong>Thành công!</strong> {{ session('paymentSuccess') }}
                    </div>
                @endif
                @if(session('paymentError'))
                    <div class="alert alert-danger">
                        <strong>Lỗi!</strong> {{ session('paymentError') }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger">
                        <strong>Lỗi!</strong> Đã sảy ra lỗi khi thao tác.
                    </div>
                @endif
                <div class="row justify-content-center">
                    <!-- Start Right -->
                    <div class="col-8">
                        <div class="checkout-right">
                            <div class="cart-total">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Thông Tin Đơn Hàng</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>Mã Đơn Hàng</th>
                                        <td>{{ $order->code }}</td>
                                    </tr>
                                    <tr>
                                        <th>Sản Phẩm</th>
                                        <td>{{ $name }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ $planDetail->post->name }}</th>
                                        <td>{{ $planDetail->time }} Năm</td>
                                    </tr>
                                    <tr>
                                        <th>Tổng Tiền</th>
                                        <td>{{ number_format($order->total, 0, ',', '.') }} VNĐ</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Thông Tin Khách Hàng</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th>Tên</th>
                                        <td>{{ $order->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Điện Thoại</th>
                                        <td>{{ $order->phone_number }}</td>
                                    </tr>
                                    @if (!empty($order->email))
                                        <tr>
                                            <th>Email</th>
                                            <td>{{ $order->email }}</td>
                                        </tr>
                                    @endif
                                    @if (!empty($order->address))
                                        <tr>
                                            <th>Địa Chỉ</th>
                                            <td>{{ $order->address }}</td>
                                        </tr>
                                    @endif
                                    <?php
                                        $skipPayment = [
                                            config('constants.ORDER_STATUS.PAID'),
                                            config('constants.ORDER_STATUS.VN_PAY')
                                        ]
                                    ?>
                                    @if (!in_array($order->status, $skipPayment))
                                    <tr>
                                        <td colspan="2">
                                            <div class="btn-outer">
                                                <button type="submit" class="btn checkout-btn btn-block">Thanh Toán
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End Right -->
                </div>
            </div>
        </form>
    </section>

@endsection

@section('script_page')
@endsection
