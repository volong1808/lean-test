<div>
    <table cellpadding="0" cellspacing="0" width="700" align="center" border="0"
           style="font-family:&quot;open sans&quot;,sans-serif;font-size:12px;margin-top:10px">
        <tbody style="background-color:rgba(241,245,247,0.5);vertical-align:top;line-height:20px">
            <tr>
                <td style="background-color:rgb(255,255,255);padding:30px 40px;border:1px solid rgb(194,194,194);border-radius:7px">
                    <div style="border-bottom:1px solid rgb(229,229,229);padding-bottom:5px;margin-bottom:20px">
                        <h1 style="color:rgb(78,91,99);font-size:28px;margin:15px 0px 0px;font-weight:100">Email Đặt Mua Sản Phẩm</h1>
                        <h6 style="color:rgb(78,91,99);font-size:15px;margin:25px 0px 10px;font-weight:500">
                            {{ url('/') }} xin chào ban: {{ $order['name'] }}
                        </h6>
                        <p style="color:rgb(120,138,149);font-size:15px;margin:10px 0px 15px">
                            Bạn vừa đặt mua sản phẩm từ website  {{ url('/') }}
                        </p>
                    </div>
                    <h1 style="color:rgb(78,91,99);font-size:28px;margin:15px 0px 0px;font-weight:100">Thông Tin Sản Phẩm</h1>
                    <div style="border-bottom:1px solid rgb(229,229,229);padding-bottom:5px;margin-bottom:20px">
                        <h6 style="color:rgb(78,91,99);font-size:15px;margin:25px 0px 10px;font-weight:500">
                            Tên Sản Phẩm: {{ $order['product_name'] }}
                        </h6>
                        <p style="color:rgb(120,138,149);font-size:15px;margin:10px 0px 15px">
                            Số lượng: {{ $order['amount'] }}
                        </p>
                    </div>
                    <div>
                        <div style="display:inline-block;width:100%;margin-bottom:6px">
                            <label style="color:#788a95;font-size:15px;margin-right:1px">Tên khách hàng:</label>
                            <p style="display:inline-block;margin:0px;color:rgb(99,115,116);font-size:15px">
                                {{ $order['name'] }}
                            </p>
                        </div>
                        <div style="display:inline-block;width:100%;margin-bottom:6px">
                            <label style="color:#788a95;font-size:15px">Email:&nbsp;</label>
                            <p style="display:inline-block;margin:0px;color:rgb(99,115,116);font-size:15px">
                                <span class="m_-536528666603210140Object" role="link"
                                        id="m_-536528666603210140OBJ_PREFIX_DWT44_ZmEmailObjectHandler">
                                    <a href="mailto:{{ $order['email'] }}" target="_blank">
                                        {{ $order['email'] }}
                                    </a>
                                </span>
                            </p>
                        </div>
                        <div style="display:inline-block;width:100%;margin-bottom:6px">
                            <label style="color:#788a95;font-size:15px">Phone:&nbsp;</label>
                            <p style="display:inline-block;margin:0px;color:rgb(99,115,116);font-size:15px">
                                <span class="m_-536528666603210140Object" role="link"
                                        id="m_-536528666603210140OBJ_PREFIX_DWT45_com_zimbra_phone">
                                    <a href="callto: {{ $order['phone_number'] }}" target="_blank">
                                         {{ $order['phone_number'] }}
                                    </a>
                                </span>
                            </p>
                        </div>
                        @if (!empty($order['message']))
                        <div style="display:inline-block;width:100%;margin-bottom:6px">
                            <label style="color:#788a95;font-size:15px">Nội dung:&nbsp;</label>
                            <p style="display:inline-block;margin:0px;color:rgb(99,115,116);font-size:15px">
                                {{ $order['message'] }}
                            </p>
                        </div>
                        @endif
                        <div style="display:inline-block;width:100%;margin-bottom:6px">
                            <label style="color:#788a95;font-size:15px">Địa Chỉ:&nbsp;</label>
                            <p style="display:inline-block;margin:0px;color:rgb(99,115,116);font-size:15px">
                                {{ $order['address'] }}
                            </p>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>