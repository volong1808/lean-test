@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ $banner['title'] }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- ==============================================
    **FAQ**
    =================================================== -->
    <section class="faq-outer grey-bg padding-lg" id="faq-sec">
        @if (!empty($faqs))
        <div class="container">
            <div class="row">
                <div class="col-md-3 faq-left">
                    <ul>
                        <?php
                            $i = 0;
                            $faqContent = null;
                        ?>
                        @foreach ($faqs as $faqCate)
                            <li>
                                <a href="{{ route('faq', $faqCate->slug) }}">
                                    <div class="icon">
                                        @if (!empty($faqCate->image))
                                            <img class="faq-cat__img" src="{{ asset($faqCate->image->origin) }}">
                                        @else
                                            <span class="icon-general"></span>
                                        @endif
                                    </div>
                                    <div class="cnt-block">
                                        <h3>{{ $faqCate->name }}</h3>
                                        @if ($faqCate->name)
                                        <p>{!! nl2br($faqCate->description) !!}</p>
                                        @endif
                                    </div>
                                </a>
                            </li>
                             <?php
                                if ($i == 0) {
                                    $faqContent = $faqCate->posts;
                                }
                                if ($faqCate->slug == $categorySlug) {
                                    $faqContent = $faqCate->posts;
                                }
                                $i++;
                              ?>
                        @endforeach
                    </ul>
                </div>
                <div class="col-md-9 faq-right">
                    @if (!empty($faqContent))
                        <div id="accordion" role="tablist">
                            @foreach ($faqContent as $faq)
                                <!-- Start:accordion item 01 -->
                                <div class="card">
                                    <div class="card-header" role="tab" id="faq-header{{ $faq->id }}">
                                        <h5 class="mb-0"><a data-toggle="collapse" href="#faq{{ $faq->id }}"
                                                            aria-expanded="true"
                                                            aria-controls="collapseOne"> {{ $faq->name }}</a></h5>
                                    </div>
                                    <div id="faq{{ $faq->id }}" class="collapse" role="tabpanel"
                                         aria-labelledby="faq-header{{ $faq->id }}" data-parent="#accordion">
                                        <div class="card-body">
                                            <p>{!! $faq->description !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End:accordion item 01 -->
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @endif
    </section>
    @include('frontend.includes.sing_up')
@endsection
