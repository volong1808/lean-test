@extends('frontend.layouts.master')
@section('title_head')
    {{ $banner['title'] }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="bg-fff">
        <div class="container">
            <div id="map-link-bar">
                <ul>
                    <li><a href="{{ route('home') }}" title="{{ trans('message.home_page') }}">{{ trans('message.home_page') }}</a></li>
                    <li><a href="{{ route('about_list') }}" title="{{ trans('message.about_page') }}">{{ trans('message.about_page') }}</a></li>
                    <li class="active">{{ $name }}</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <!-- menu about -->
    @if (!empty($aboutList))
    <div class="bg-fff" style="position:relative;">
        <div class="container">
            <div id="sticky-wrapper" class="sticky-wrapper"><div class="nav-scroll">
                    <div class="box">
                        <ul>
                            <li class="{{ $page->slug == 'gioi-thieu-chung' ? 'active' : '' }}">
                                <a href="{{ route('about') }}" title="{{ trans('message.about_page') }} chung">{{ trans('message.about_page') }} chung</a>
                            </li>
                            @foreach ($aboutList as $item)
                            <li class="active">
                                <a href="{{ route('about_detail', $item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div></div>
        </div>
    </div>
    <!-- end about menu-->
    @endif
    <!-- content -->
    <div class="container">
        <div class="title">
            <div class="titH-4 border-bottom-double">{{ $page->name }}</div>
        </div>
        <div class="_detail-news">
            <div class="_detail">
                {!! $page->content !!}
            </div>
        </div>
    </div>
    <!-- end content-->
    <!-- footer -->
@endsection