@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' =>  $banner])
    <!-- break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home' . $prefixLang) }}">{{ trans('message.home_page') }}</a> » <span
                                        class="breadcrumb_last" aria-current="page">{{ trans('message.job') }}</span></span></span></p>
                </div>
            </div>
        </div>
    </div>
    @if ($news->count() > 0)
    <div class="block block--padding block--grey-lightest">
        <div class="wrap">
            <div class="block__wrapper">
                <div class="block__inner block__inner--small">
                    <div class="block__body">
                        <div class="post-items post-items--large">
                            @foreach ($news as $item)
                                <?php
                                $image = 'uploads/feedback/man-2.png';
                                if (!empty($item->image)) {
                                    $image = $item->image->thumb;
                                }
                                ?>
                                    <div class="post-items__item">
                                        <div class="post-item">
                                            <div class="post-item__header">
                                                <div class="post-item__label label">
                                                    <span class="label__inner">{{ trans('message.job') }}</span>
                                                </div>
                                                <a href="{{ route('fr_job_detail' . $prefixLang, $item->slug) }}"
                                                   class="post-item__link">
                                                    <figure class="post-item__image">
                                                        <img src="{{ asset($image) }}"
                                                             class="attachment-post-item-image size-post-item-image wp-post-image"
                                                             alt="{{ $item->name }}" loading="lazy"
                                                             data-object-fit="cover">
                                                        <div class="post-item__overlay"></div>
                                                    </figure>
                                                </a>
                                            </div>
                                            <div class="post-item__body">
                                                <span class="post-item__date">{{ date('Y M d', strtotime($item->created_at)) }}</span>
                                                <a href="{{ route('fr_job_detail' . $prefixLang, $item->slug) }}"
                                                   class="post-item__link">
                                                    <h3 class="post-item__title">{{ $item->name }}</h3>
                                                </a>
                                                <a href="{{ route('fr_job_detail' . $prefixLang, $item->slug) }}"
                                                   class="post-item__read-more">
                                                    <span class="link-icon icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                                                    <span class="post-item__label">{{ trans('message.new_view_more') }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        </div>

                    </div>
                    @include('frontend.includes.paging', ['pagination' => $news])
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
