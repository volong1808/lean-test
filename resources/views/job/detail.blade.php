@extends('frontend.layouts.master')
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home') }}">{{ trans('message.home_page') }}</a> »
                        <a href="{{ route('fr_job_list') }}">{{ trans('message.job') }}</a></span></span></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="block block--padding block--grey-lightest">

        <div class="wrap wrap--small">

            <div class="wysiwyg">
                {!! $newsDetail->content !!}
            </div>

        </div>

    </div>
    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>
    @include('frontend.includes.subscribe_new_letter')
    <!-- footer -->
@endsection