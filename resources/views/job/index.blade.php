@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', $banner)
    <!-- break crum -->
    <div class="bg-fff">
        <div class="container">
            <div id="map-link-bar">
                <ul>
                    <li><a href="{{ route('home') }}" title="{{ trans('message.home_page') }}">{{ trans('message.home_page') }}</a></li>
                    <li class="active">{{ $name }}</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="container">
        <div class="slider-news-id">
            <div class="list-rrm-v1">
                <ul class="list-item row">
                    @foreach ($news as $item)
                        <?php
                        $image = 'uploads/feedback/man-2.png';
                        if (!empty($item->image)) {
                            $image = $item->image->thumb;
                        }
                        ?>
                        <li>
                            <div class="item">
                                <a class="avt" href="{{ route('fr_job_detail' . $prefixLang, $item->slug) }}" title="{{ $item->name }}">
                                    <img src="{{ asset($image) }}" alt="{{ $item->name }}" title="{{ $item->name }}">
                                </a>
                                <div class="desc">
                                    <a href="{{ route('fr_job_detail' . $prefixLang, $item->slug) }}" title="{{ $item->name }}" class="titH-5">{{ $item->name }}</a>
                                    <div class="_desc">
                                        <p>{{ $item->description }}</p>
                                    </div>
                                    <a href="{{ route('fr_job_detail' . $prefixLang, $item->slug) }}" class="btn-seemore-2">{{ trans('message.detail') }} <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @include('frontend.includes.paging', ['pagination' => $news])
    </div>

@endsection
