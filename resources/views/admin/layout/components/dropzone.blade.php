<script src="{{ asset('backend\js\plugins\uploaders\dropzone.min.js') }}"></script>
<script>
    // Multiple files
    Dropzone.options.dropzoneRemove = {
        paramName: 'image',
        method: 'post',
        dictDefaultMessage: "Kéo thả file vào đây hoặc<span> CLICK</span> để tải lên",
        maxFilesize: 2, // MB
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        dictFileTooBig: "Kích thước File quá lớn (tối đa 2MiB).",
        dictRemoveFile: "Xoá",
        init: function() {
            var images = [];
                @if (! empty($gallery))
                @foreach($gallery as $image)
            var tmp = {
                    url: "{{ asset($image['image']) }}",
                    id: "{{ $image['id'] }}",
                    name: "{{ $image['name'] }}",
                    size: "{{ $image['size'] }}",
                };
            images.push(tmp);
                @endforeach
            var myDropZone = this;
            $.each(images, function (i, image) {
                var mockFile = {
                    name: image.name,
                    size: image.size,
                };
                myDropZone.options.addedfile.call(myDropZone, mockFile);
                myDropZone.options.thumbnail.call(myDropZone, mockFile, image.url);
                $(myDropZone.element.lastChild.lastChild).attr('data-dz-remove', image.id);
            });
            @endif
        },
        success: function (a) {
            var response = $.parseJSON(a.xhr.response);
            $('input[name={{ $this->security->get_csrf_token_name() }}]').each(function (i, input) {
                $(input).val(response.csrf);
            });
            $('.dz-remove').each(function (i, el) {
                if ($(el).attr('data-dz-remove') == '') {
                    $(el).attr('data-dz-remove', response.image_id);
                }
            });
        },
        removedfile: function (a) {
            var removeBtn = $($(a.previewElement)[0].lastChild);
            var csrf = $($('input[name={{ $this->security->get_csrf_token_name() }}]')[0]).val();
            var image_id = removeBtn.attr('data-dz-remove');
            var delete_link = '{{ site_url('admin/product_image/delete') }}';
            $.ajax({
                url: delete_link,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    'item_id': image_id,
                    '{{ $this->security->get_csrf_token_name() }}': csrf,
                },
                success: function (response) {
                    $('input[name={{ $this->security->get_csrf_token_name() }}]').each(function (i, input) {
                        $(input).val(response.csrf);
                        console.log(response);
                    });
                    var b;
                    return a.previewElement && null != (b = a.previewElement) && b.parentNode.removeChild(a.previewElement);
                }
            })
        },
    };
</script>
