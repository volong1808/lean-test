<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    <?php
                    $currentRoute = \Route::current()->getName();
                    $currentUrl = url()->current();
                    ?>
                    <li @if($currentRoute == 'admin_index') class="active" @endif>
                        <a href="{{ route('admin_index') }}">
                            <i class="icon-home4"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li @if(in_array($currentRoute, ['product_edit']))class="active"@endif>
                        <a href="#"><i class="icon-cart5"></i> <span>{{ trans('message.product_page') }}</span></a>
                        <ul>
                            <li @if($currentRoute == 'product_list') class="active" @endif><a href="{{ route('product_list') }}">Tất Cả Sản Phẩm</a></li>
                            <li @if($currentRoute == 'product_add') class="active" @endif><a href="{{ route('product_add') }}">Thêm Mới Sản Phẩm</a></li>
                            <li @if(strpos($currentRoute, 'product_category') !== false) class="active" @endif><a href="{{ route('product_category_list') }}">Danh Mục</a></li>
                        </ul>
                    </li>
                    <li @if(in_array($currentRoute, ['admin_order']))class="active"@endif>
                        <a href="{{ route('admin_order') }}"><i class="icon-cash4"></i> <span>Đơn Hàng</span></a>
                    </li>
                    <li @if(in_array($currentRoute, ['post_edit']))class="active"@endif>
                        <a href="#"><i class="icon-newspaper"></i> <span>Tin Tức</span></a>
                        <ul>
                            <li @if($currentRoute == 'post_list') class="active" @endif><a href="{{ route('post_list') }}">Tất Cả Tin Tức</a></li>
                            <li @if($currentRoute == 'post_add') class="active" @endif><a href="{{ route('post_add') }}">Thêm Mới Tin Tức</a></li>
                            <li @if(strpos($currentRoute, 'post_category') !== false) class="active" @endif><a href="{{ route('post_category_list') }}">Danh Mục</a></li>
                        </ul>
                    </li>
                    <li @if(in_array($currentRoute, ['service_edit']))class="active"@endif>
                        <a href="#"><i class="icon-newspaper"></i> <span>Dịch Vụ</span></a>
                        <ul>
                            <li @if($currentRoute == 'service_list') class="active" @endif><a href="{{ route('service_list') }}">Tất Cả Dịch Vụ</a></li>
                            <li @if($currentRoute == 'service_add') class="active" @endif><a href="{{ route('service_add') }}">Thêm Mới Dịch Vụ</a></li>
                        </ul>
                    </li>
                    <li @if(in_array($currentRoute, ['job_edit']))class="active"@endif>
                        <a href="#"><i class="icon-newspaper2"></i> <span>Tuyển Dụng</span></a>
                        <ul>
                            <li @if($currentRoute == 'job_list') class="active" @endif><a href="{{ route('job_list') }}">Tất Cả Việc Làm</a></li>
                            <li @if($currentRoute == 'job_add') class="active" @endif><a href="{{ route('job_add') }}">Thêm Mới</a></li>
                        </ul>
                    </li>
                    <li @if(in_array($currentRoute, ['field_edit']))class="active"@endif>
                        <a href="#"><i class="icon-newspaper2"></i> <span>Lĩnh Vực</span></a>
                        <ul>
                            <li @if($currentRoute == 'field_list') class="active" @endif><a href="{{ route('field_list') }}">Tất Cả Lĩnh Vực</a></li>
                            <li @if($currentRoute == 'field_add') class="active" @endif><a href="{{ route('field_add') }}">Thêm Mới</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-office"></i> <span>Chi Nhánh</span></a>
                        <ul>
                            <li @if($currentRoute == 'partner_list') class="active" @endif><a href="{{ route('partner_list') }}">Danh Sách</a></li>
                            <li @if($currentRoute == 'partner_add') class="active" @endif><a href="{{ route('partner_add') }}">Thêm Mới</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="icon-stack"></i> <span>Page</span></a>
                        <ul>
                            <li @if($currentRoute == 'page_list') class="active" @endif><a href="{{ route('page_list') }}">Danh Sách</a></li>
                            <li @if($currentRoute == '') class="active" @endif><a href="#">Thêm Mới</a></li>
                        </ul>
                    </li>
{{--                    <li>--}}
{{--                        <a href="#"><i class="icon-image2"></i> <span>Khách Hàng</span></a>--}}
{{--                        <ul>--}}
{{--                            <li @if($currentRoute == 'customer_list') class="active" @endif><a href="{{ route('customer_list') }}">Danh Sách</a></li>--}}
{{--                            <li @if($currentRoute == 'customer_add') class="active" @endif><a href="{{ route('customer_add') }}">Thêm Mới</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
                    <li  @if(in_array($currentRoute, ['faq_edit']))class="active"@endif>
                        <a href="#"><i class="icon-question3"></i> <span>FAQs</span></a>
                        <ul>
                            <li @if($currentRoute == 'faq_list') class="active" @endif><a href="{{ route('faq_list') }}">Danh Sách</a></li>
                            <li @if($currentRoute == 'faq_add') class="active" @endif><a href="{{ route('faq_add') }}">Thêm Mới</a></li>
                            <li @if(strpos($currentRoute, 'faq_category') !== false) class="active" @endif><a href="{{ route('faq_category_list') }}">Nhóm FQAs</a></li>
                        </ul>
                    </li>
                    <li @if($currentRoute == 'admin_contacts') class="active" @endif>
                        <a href="{{ route('admin_contacts') }}">
                            <i class="icon-envelop2"></i> <span>Liên Hệ</span>
                        </a>
                    </li>
                    <!-- /main -->
                    <?php $routeConfig =  route('config_website', config('constants.company_config.key')); ?>
                    <li @if($currentUrl == $routeConfig) class="active" @endif>
                        <a href="{{ $routeConfig }}">
                            <i class="icon-info22"></i> <span>Thông Tin Công Ty</span>
                        </a>
                    </li>
                    <?php $routeConfig =  route('config_website', config('constants.plugin_config.key')); ?>
                    <li @if($currentUrl == $routeConfig) class="active" @endif>
                        <a href="{{ $routeConfig }}">
                            <i class="icon-bell-plus"></i> <span>Script Plugins</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
<!-- /main sidebar -->
