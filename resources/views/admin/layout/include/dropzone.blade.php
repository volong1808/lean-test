    <div class="panel panel-flat">
    <div class="panel-body">
        <div class="form-group">
            <label class="control-label">{{ $label }}</label>
            <div class="dropzone dz-clickable" id="{{ $dropzoneId }}"
                 data-post-id="{{ isset($post) ? $post->id : 0 }}"
                 data-post-type="product">
            </div>
        </div>
    </div>
</div>
