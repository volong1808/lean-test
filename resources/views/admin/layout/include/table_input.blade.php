<div class="row mb-20">
    <div class="col-md-9"><label class="control-label">{{ $tableConfig['label'] }}</label></div>
    <div class="col-md-3 text-right"><button type="button" data-name="{{ $tableConfig['name'] }}" data-row="#table-row-{{ $tableConfig['name'] }}" data-table="#table-{{ $tableConfig['name'] }}" class="btn btn-primary js-add-row">Thêm Cột</button></div>
</div>
<table class="table table-bordered table-framed mb-20" id="table-{{ $tableConfig['name'] }}">
    <thead>
    <tr>
        <th class="w-50">#</th>
        @foreach($tableConfig['columns'] as $key => $column)
            <th>{{ $column['name'] }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @if (!empty($tableConfig['data']))

        @php
            $data = $tableConfig['data'];
        @endphp

        @foreach($data as $index => $row)
            <tr>
                <td>
                    <button type="button" class="btn btn-sm btn-danger js-delete-row"><i class="icon-trash"></i>
                    </button>
                </td>
                @foreach($tableConfig['columns'] as $key => $column)
                    @php
                        $input = $column['input'];
                        $columnName = $input['inputName'];
                        $input['inputName'] = $tableConfig['name'] ."[{$index}][{$input['inputName']}]";
                        $input['initValue'] = [$input['inputName'] => $row[$columnName]];
                        $input['label'] = '';
                    @endphp
                    <td>@include ('admin.layout.include.form_input', $input)</td>
                @endforeach
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
<table class="row-content-table hidden" id="table-row-{{ $tableConfig['name'] }}">
    <tr data-index="{index}">
        <td><button type="button" class="btn btn-sm btn-danger js-delete-row"><i class="icon-trash"></i></button></td>
        @foreach($tableConfig['columns'] as $key => $column)
            @php
                $input = $column['input'];
                $input['inputName'] = "{nameInput}[{index}][{$input['inputName']}]";
                $input['initValue'] = [$input['inputName'] => ''];
                $input['label'] = '';
            @endphp
            <td>@include ('admin.layout.include.form_input', $input)</td>
        @endforeach
    </tr>
</table>