<?php
$cssClass = '';
if (isset($classes)) {
    if (is_array($classes)) {
        foreach ($classes as $c) {
            $cssClass .= $c . ' ';
        }
    } else {
        $cssClass = $classes;
    }
}
$eAttributes = '';
if (isset($attributes)) {
    if (is_array($attributes)) {
        foreach ($attributes as $k => $v) {
            $eAttributes .= $k . '=' . $v . ' ';
        }
    }
}
?>
<div class="form-group @if($errors->first($inputName)) has-error @endif">
    @if (!empty($label))
    <label class="control-label">{{ $label }}</label>
    @endif
    @if ($inputType == 'text')
        <input type="text" class="form-control {{ $cssClass }}" name="{{ $inputName }}"
               value="{{ old($inputName, $initValue[$inputName]) }}"
               placeholder="{{ "Nhập {$label}" }}" id="input-{{ $inputName }}" {{ $eAttributes }}>
    @endif
    @if ($inputType == 'email')
        <input type="email" class="form-control {{ $cssClass }}" name="{{ $inputName }}"
               value="{{ old($inputName, $initValue[$inputName]) }}"
               placeholder="{{ "Nhập {$label}" }}" id="input-{{ $inputName }}" {{ $eAttributes }}>
    @endif
    @if ($inputType == 'password')
        <input type="password" class="form-control" name="{{ $inputName }}"
               value="{{ old($inputName, $initValue[$inputName]) }}"
               id="input-{{ $inputName }}">
    @endif
    @if ($inputType == 'password')
        <input type="password" class="form-control" name="{{ $inputName }}"
               value="{{ old($inputName, $initValue[$inputName]) }}"
               id="input-{{ $inputName }}">
    @endif
    @if($inputType == 'editor')
        <textarea name="{{ $inputName }}" class="summernote {{ $cssClass }}" {{ $eAttributes }}>
            {{ old($inputName, $initValue[$inputName]) }}
        </textarea>
    @endif
    @if($inputType == 'textarea')
        <textarea name="{{ $inputName }}" rows="3" cols="3" class="form-control {{ $cssClass }}" {{ $eAttributes }}
                  placeholder="{{ "Nhập {$label}" }}">{{ old($inputName, $initValue[$inputName]) }}</textarea>
    @endif
    @if($inputType == 'select')
        <?php $selected = old($inputName, $initValue[$inputName]); ?>
        <select name="{{ $inputName }}" class="form-control {{ $cssClass }}" {{ $eAttributes }}>
            <option value="">{{ "Chọn {$label}" }}</option>
            @foreach($options as $option)
                <option value="{{ $option['id'] }}" @if ($selected == $option['id']) selected @endif>{{ $option['name'] }}</option>
            @endforeach
        </select>
    @endif
    <span class="help-block">{{ $errors->first($inputName) }}</span>
    @if (isset($permalink) && $permalink)
        <div class="permalink @if($errors->first('slug')) has-error @endif">
            <div class="permalink-text">
                <span class="permalink-label">Permalink: </span>
                <span class="permalink-prefix">{{ url('/') }}/san-pham/</span>
                <span class="permalink-content" data-value="{{ old('slug', $initValue['slug']) }}">
                </span>
                <button type="button" class="btn btn-default btn-xs" id="btn-edit-permalink">Edit</button>
            </div>
            <div class="permalink-input hidden">
                <input type="text" class="form-control form-item-inline permalink-form-control" id="permalink"
                       value="{{ old('slug', $initValue['slug']) }}" name="slug">
                <button type="button" class="btn btn-default btn-xs" id="btn-permalink-ok">OK</button>
                <button type="button" class="btn btn-link btn-xs permalink__btn-cancel" id="btn-permalink-cancel">Cancel</button>
            </div>
            <div class="clearfix"></div>
            @if (!empty($errors->first($inputName)) && !empty(old($inputName, $initValue[$inputName]))
                    || isset($post)
                    || empty($errors->first($inputName)) && $errors->first('slug'))
            <span class="help-block">{{ $errors->first('slug') }}</span>
            @endif
        </div>
    @endif
</div>