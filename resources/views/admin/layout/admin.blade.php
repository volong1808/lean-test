<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-param" content="authenticity_token" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ (isset($title)) ? $title : 'Admin Manage' }}</title>

    <!-- favicon -->
    @include('include.favicon')

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/main.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    @yield('css')
</head>

<body>

@include('admin.layout.include.navbar')

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        @include('admin.layout.include.sidebar')

        <!-- Main content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
<!-- Core JS files -->
<script src="{{ asset('backend/js/core/libraries/jquery.min.js') }}"></script>
<script src="{{ asset('backend/js/core/libraries/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/editors/summernote/summernote.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/uploaders/dropzone.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/app.js') }}"></script>
<script src="{{ asset('backend/js/admin.js') }}"></script>
<!-- /core JS files -->
@yield('js')
</body>
</html>
