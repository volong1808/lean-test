@csrf
<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Câu Hỏi',
                    'inputType' => 'text',
                    'inputName' => 'name',
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Trả Lời',
                    'inputType' => 'textarea',
                    'inputName' => 'description',
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <div class="form-group mb-0">
                        <label class="control-label">Danh Mục</label>
                        @if ($categories->count() > 0)
                            @foreach ($categories as $category)
                                <?php
                                /* @var $category */
                                $children = $category->children()->get();
                                ?>
                                @if ($children->count() === 0)
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="styled" name="categories[]"
                                                   value="{{ $category->id }}"
                                                   @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                            {{ $category->name }}
                                        </label>
                                    </div>
                                @else
                                    <div class="radio">
                                        <label>
                                            <input type="radio" class="styled" name="categories[]"
                                                   value="{{ $category->id }}"
                                                   @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                            {{ $category->name }}
                                        </label>
                                    </div>
                                    <div class="ml-1">
                                        @foreach ($children as $child)
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" class="styled" name="categories[]"
                                                           value="{{ $child->id }}"
                                                           @if(in_array($child->id, $initValue['categories'])) checked @endif>
                                                    {{ $child->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Vị Trí Sắp Xếp',
                    'inputType' => 'text',
                    'inputName' => 'position',
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                            {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                            {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    @if (isset($post))
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    @else
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    @endif
                </div>
            </div>
        </div>
        <!-- /panel -->
    </div>
</div>
