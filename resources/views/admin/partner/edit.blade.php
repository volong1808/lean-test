@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Trang Cập Nhật Thông Tin Chi Nhánh</h1>
            </div>

            <div class="heading-elements">
                <a href="{{ route('partner_add') }}" class="btn btn-labeled bg-blue heading-btn">
                    <b><i class="icon-plus2"></i></b> Thêm Mới
                </a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li><a href="{{ route('partner_list') }}">Danh Sách Chi Nhánh</a></li>
                <li class="active">Cập Nhật</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        @if (session('success'))
            <div class="alert alert-success alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                {{ session('error') }}
            </div>
        @endif
        @if (!empty($errors->all()))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                <p>{{ config('constants.FORM_ERROR_MESSAGE') }}</p>
            </div>
        @endif
        <form action="{{ route('partner_do_edit', ['id' => $post->id]) }}" method="post" enctype="multipart/form-data">
            @include ('admin.partner.form')
        </form>
    </div>
    <!-- /content area -->
@endsection

@section('js')
@endsection
