@csrf
<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Tiêu Đề Chi Nhánh',
                    'inputType' => 'text',
                    'inputName' => 'name',
                    'permalink' => true,
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Nội Dung Bài Viết Chi Nhánh',
                    'inputType' => 'editor',
                    'inputName' => 'content',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Mô Tả Ngắn',
                    'inputType' => 'textarea',
                    'inputName' => 'description',
                    'permalink' => false,
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                            {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                            {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    @if (isset($post))
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    @else
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    @endif
                </div>
            </div>
        </div>
        <!-- /panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <?php
                    $imgsrc = empty($initValue['image']) ? asset(config('constants.product_image.PLACE_HOLDER')) : asset($initValue['image']->url)
                    ?>
                    <label class="control-label">Hình Đại Diện</label>
                    <img src="{{ $imgsrc }}" class="img-responsive input-file__image" id="file-image">
                    <input type="file" name="image" id="file" class="input-file" data-cover="#file-image" />
                    <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
                    <p class="help-block">
                        Kích thước tốt nhất {{ config('constants.product_image.WIDTH') }}
                        x{{ config('constants.product_image.HEIGHT') }} (px)</p>
                </div>
            </div>
        </div><!-- /panel -->
    </div>
</div>
