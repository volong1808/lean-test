<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Tên Danh Mục',
                    'inputType' => 'text',
                    'inputName' => 'name',
                    'permalink' => true,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <div class="form-group @if($errors->first('parent_id')) has-error @endif">
                    <label class="control-label" for="input-parent-id">Danh Mục Cha</label>
                    <select class="form-control w-25" name="parent_id" id="input-parent-id">
                        @foreach($parentCategories as $value => $text)
                            <option value="{{ $value }}" @if($initValue['parent_id'] == $value) selected @endif>{{ $text }}</option>
                        @endforeach
                    </select>
                    @if($errors->first('parent_id'))
                    <span class="help-block">{{ $errors->first('parent_id') }}</span>
                    @endif
                </div>


                <?php
                $inputData = [
                    'label' => 'Mô Tả Ngắn',
                    'inputType' => 'textarea',
                    'inputName' => 'description',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                                {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                                {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    @if (isset($category))
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    @else
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    @endif
                </div>
            </div>
        </div><!-- /panel -->
    </div>
</div>
