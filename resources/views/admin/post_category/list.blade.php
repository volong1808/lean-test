@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Trang Quản Lý Danh Mục Bài Viết</h1>
            </div>

            <div class="heading-elements">
                <a href="{{ route('post_category_add') }}" class="btn btn-labeled bg-blue heading-btn">
                    <b><i class="icon-plus2"></i></b> Thêm Mới
                </a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li class="active">Danh Sách Danh Mục</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
                @endif
                <!-- table form -->
                <form action="{{ route('post_category_bulk_action') }}" method="post">
                    @csrf
                    <div class="row table-control">
                        <div class="col-md-6">
                            <select name="action" class="form-control form-item-inline">
                                <option value="{{ config('constants.BULK_ACTION.none') }}">Tác Vụ</option>
                                <option value="{{ config('constants.BULK_ACTION.delete') }}">Xóa Đã Chọn</option>
                            </select>
                            <button type="submit" href="#" class="btn btn-default">Áp Dụng</button>
                        </div>
                    </div>
                    <!-- table -->
                    <div class="table-responsive">
                        <table class="table table-framed table-hover">
                            <thead>
                            <tr>
                                <th width="50px">
                                    <label for="all-checkbox" class="hidden">Check All</label>
                                    <input type="checkbox" id="all-checkbox" class="styled" value="1">
                                </th>
                                <th>Tên Danh Mục</th>
                                <th>Danh Mục Cha</th>
                                <th>Trạng Thái</th>
                                <th>Thời Gian</th>
                                <th width="120px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (empty($categories))
                                <tr>
                                    <td colspan="6"><strong>Hiện tại chưa có danh mục nào</strong></td>
                                </tr>
                            @else
                                <?php
                                /* @var $categories */
                                $currentOffset = ($categories->currentpage() - 1) * $categories->perpage() + 1;
                                $toOffset = ($categories->currentpage() - 1) * $categories->perpage() + $categories->count();
                                ?>
                                @foreach($categories as $category)
                                    <?php
                                    /* @var $category */
                                    $parentCategory = $category->parent;
                                    ?>
                                    <tr>
                                        <td>
                                            <input name="checkedPost[]" type="checkbox" class="styled table-checkbox"
                                                   value="{{ $category->id }}">
                                        </td>
                                        <td>
                                            <a href="{{ route('post_category_edit', ['id' => $category->id]) }}">
                                                {{ $category->name }}
                                            </a>
                                        </td>
                                        <td>
                                            @if(!empty($parentCategory))
                                                {{ $parentCategory->name }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($category->state == config('constants.POST_STATE.PUBLISHED'))
                                                <span class="label label-info">Hiển Thị</span>
                                            @else
                                                <span class="label label-danger">Không Hiển Thị</span>
                                            @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($category->updated_at)->format('d/m/Y') }}</td>
                                        <td>
                                            <a href="{{ route('post_category_edit', ['id' => $category->id]) }}" class="text-warning" data-popup="tooltip" title="Chỉnh Sửa">
                                                <i class="icon-pencil7"></i>
                                            </a>
                                            <a href="#" class="text-black" data-toggle="modal" title="Xoá"  data-popup="tooltip"
                                                    data-target="#modal-delete" data-id="{{ $category->id }}">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /table -->
                    <!-- table footer -->
                    <div class="row table-footer">
                        <div class="col-md-6">
                            <p class="summary-table-result">
                                @if($categories->count() > 0)
                                    Hiển thị từ <strong>{{ $currentOffset }}</strong>
                                    tới <strong>{{ $toOffset }}</strong>
                                    của <strong>{{ $categories->total() }}</strong> kết quả.
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            {{ $categories->links() }}
                        </div>
                    </div>
                </form>
                <!-- /table footer -->
            </div>
        </div>
        <!-- /simple panel -->
    </div>
    <!-- /content area -->
    @include('admin.layout.include.modal_delete', ['url' => route('post_category_delete')])
@endsection
