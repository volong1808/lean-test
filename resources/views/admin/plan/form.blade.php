@csrf
<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Tên Gói Sản Phẩm',
                    'inputType' => 'text',
                    'inputName' => 'name',
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <div class="form-group w200">
                    <label class="control-label">Sản Phẩm</label>
                    <select name="product_id" class="form-control form-item-inline">
                        @isset($products)
                            @foreach($products as $product)
                                <option
                                    value="{{ $product->id }}"
                                    @if($product->id == old('product_id', $initValue['product_id'])) selected @endif>
                                    {{ $product->post->name }}
                                </option>
                            @endforeach
                        @endisset
                    </select>
                    <span class="help-block">{{ $errors->first('product_id') }}</span>
                </div>
                <?php
                $inputData = [
                    'label' => 'Mô Tả Ngắn',
                    'inputType' => 'textarea',
                    'inputName' => 'description',
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <div class="form-group @if($errors->first('price') OR $errors->first('time')) has-error @endif">
                    <label class="control-label">Giá (VNĐ) / Thời Gian Sử Dụng (Năm)</label>
                    <div class="form-input-group">
                        <input type="text" class="form-control form-inline w200" name="price"
                               value="{{ old('price', $initValue['price']) }}">
                        <span> / </span>
                        <input type="text" class="form-control form-inline w100" name="time"
                               value="{{ old('time', $initValue['time']) }}">
                    </div>
                    <span class="help-block">
                        {{ empty($errors->first('price')) ? $errors->first('time') : $errors->first('price') }}
                    </span>
                </div>

                <div class="form-group mb-0">
                    <label class="control-label">Khuyến Nghị Mua</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="is_recommend" class="styled" value="1"
                                {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Có
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="is_recommend" class="styled" value="0"
                                {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <!-- /panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Thuộc Tính</label>
                    <div class="checkbox-group">
                        @if ($categories->count() > 0)
                            @foreach ($categories as $category)
                                <?php
                                /* @var $category */
                                $children = $category->children()->get();
                                ?>
                                @if ($children->count() === 0)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="styled" name="categories[]"
                                                   value="{{ $category->id }}"
                                                   @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                            {{ $category->name }}
                                        </label>
                                    </div>
                                @else
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="styled" name="categories[]"
                                                   value="{{ $category->id }}"
                                                   @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                            {{ $category->name }}
                                        </label>
                                    </div>
                                    <div class="ml-1">
                                        @foreach ($children as $child)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="styled" name="categories[]"
                                                           value="{{ $child->id }}"
                                                           @if(in_array($child->id, $initValue['categories'])) checked @endif>
                                                    {{ $child->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach
                        @endif
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Vị Trí Sắp Xếp',
                    'inputType' => 'text',
                    'inputName' => 'position',
                ];
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                            {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                            {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    @if (isset($post))
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    @else
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    @endif
                </div>
            </div>
        </div>
        <!-- /panel -->
    </div>
</div>
