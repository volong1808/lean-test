<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                'label' => 'Tên Bài Viết',
                'inputType' => 'text',
                'inputName' => 'name',
                'permalink' => true,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Nội Dung Bài Viết',
                    'inputType' => 'editor',
                    'inputName' => 'content',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Mô Tả Ngắn',
                    'inputType' => 'textarea',
                    'inputName' => 'description',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                                {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                                {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group mb-0">
                <label class="control-label">Ngôn Ngữ</label>
                <?php $langList = config('constants.LANG') ?>
                @foreach ($langList as $key => $langItem)
                    <div class="radio">
                        <label>
                            <input type="radio" disabled class="styled" value="{{ $key }}"
                                    {{ ($initValue['lang'] == $key) ? 'checked' : '' }}>
                            {{ $langItem }}
                        </label>
                    </div>
                @endforeach
                <input type="hidden" name="lang" value="{{ !empty($initValue['lang']) ? $initValue['lang'] : '' }}">
                <input type="hidden" name="parrent_id" value="{{ !empty($initValue['parent_id']) ? $initValue['parent_id'] : '' }}">
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    @if (isset($post))
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    @else
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    @endif
                </div>
            </div>
        </div><!-- /panel -->
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Danh Mục</label>
                    @if ($categories->count() > 0)
                        @foreach ($categories as $category)
                            <?php
                            /* @var $category */
                            $children = $category->children()->get();
                            ?>
                            @if ($children->count() === 0)
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled" name="categories[]"
                                               value="{{ $category->id }}"
                                               @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                        {{ $category->name }}
                                    </label>
                                </div>
                            @else
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled" name="categories[]"
                                               value="{{ $category->id }}"
                                               @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                        {{ $category->name }}
                                    </label>
                                </div>
                                <div class="ml-1">
                                    @foreach ($children as $child)
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="styled" name="categories[]"
                                                       value="{{ $child->id }}"
                                                       @if(in_array($child->id, $initValue['categories'])) checked @endif>
                                                {{ $child->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /panel -->
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <?php
                    $imgsrc = empty($initValue['image']) ? asset(config('constants.product_image.PLACE_HOLDER')) : asset($initValue['image']->url)
                    ?>
                    <label class="control-label">Hình Đại Diện</label>
                    <img src="{{ $imgsrc }}" class="img-responsive input-file__image" id="file-image">
                    <input type="file" name="image" id="file" class="input-file" data-cover="#file-image" />
                    <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
                    <p class="help-block">
                        Kích thước tốt nhất {{ config('constants.IMAGE_SIZE.POST.width') }}
                        x{{ config('constants.IMAGE_SIZE.POST.height') }} (px)</p>
                </div>
            </div>
        </div><!-- /panel -->
    </div>
</div>
@include ('admin.layout.include.seo_form')
