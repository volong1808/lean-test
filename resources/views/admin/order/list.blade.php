@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>Trang Quản Lý Đơn Hàng</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('admin_index') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li class="active"><a href="{{ route('admin_contacts') }}">Danh Sách Đơn Hàng</a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <!-- Table -->
        <div class="panel panel-flat">
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('orderErrors'))
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('orderErrors') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
            @endif<!-- table form -->
                <form action="{{ route('order_bulk_action') }}" method="post">
                    @csrf
                    <div class="row table-control">
                        <div class="col-md-6">
                            <select name="action" class="form-control form-item-inline">
                                <option value="{{ config('constants.BULK_ACTION.none') }}">Tác Vụ</option>
                                <option value="{{ config('constants.BULK_ACTION.delete') }}">Xóa Đã Chọn</option>
                            </select>
                            <button type="submit" href="#" class="btn btn-default">Áp Dụng</button>
                        </div>
                        <div class="col-md-6 text-right">
                            <input class="form-control form-item-inline" value="" placeholder="Nhập từ khóa...">
                            <button type="button" href="#" class="btn btn-primary">Tìm Kiếm</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-framed">
                            <thead>
                                <tr>
                                    <th width="50px">
                                        <label for="all-checkbox" class="hidden">Check All</label>
                                        <input type="checkbox" id="all-checkbox" class="styled" value="1">
                                    </th>
                                    <th width="100">Mã Đơn Hàng</th>
                                    <th>Thông Tin Khách Hàng</th>
                                    <th>Gói</th>
                                    <th>Giá</th>
                                    <th>Hình Thức Thanh Toán</th>
                                    <th>Trạng Thái</th>
                                    <th>Ngày Tạo</th>
                                    <th width="105"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @if ($orders->count() > 0)
                                <?php
                                /* @var $orders */
                                $currentOffset = ($orders->currentpage() - 1) * $orders->perpage() + 1;
                                $toOffset = ($orders->currentpage() - 1) * $orders->perpage() + $orders->count();
                                $statusList = config('constants.ORDER_STATUS_LABEL');
                                $paymentListType = config('option.banking');
                                ?>
                                @foreach($orders as $order)
                                    <?php
                                        $planName = '';
                                        $price = null;
                                        $paymentType = '';
                                        if (!empty($order->plan->post)) {
                                            $planName = $order->plan->post->name;
                                        }

                                        if (!empty($order->plan->price)) {
                                            $price = $order->plan->price;
                                        }

                                        if (!empty($paymentListType[$order->payment_type])) {
                                            $paymentType = $paymentListType[$order->payment_type]['label'];
                                        }
                                    ?>
                                    <tr>
                                        <td>
                                            <input name="checkedPost[]" type="checkbox" class="styled table-checkbox"
                                                   value="{{ $order->id }}">
                                        </td>
                                        <td>{{ $order->code }}</td>
                                        <td>
                                            <span><strong>{{ $order->name }}</strong></span>
                                            <br>
                                            <span>Điện Thoại:
                                                <a href="tel:{{ $order->phone_number }}">{{ $order->phone_number }}</a>
                                            </span><br>
                                            <span>Email:
                                                <a href="mailto:{{ $order->email }}">{{ $order->email }}</a>
                                            </span>
                                        </td>
                                        <td>
                                            {{ $planName }}
                                        </td>
                                        <td>
                                            {{ number_format($price, 0, ',', '.') }}<sup>đ</sup>
                                        </td>
                                        <td>
                                            {{ $paymentType }}
                                        </td>
                                        <td>
                                            <a href="#"
                                               class="js-change-stage"
                                               data-name="{{ $order->code }}"
                                               data-satge="{{ $order->status }}"
                                               data-toggle="modal"
                                               data-target="#modal-change-stage"
                                               data-id="{{ $order->id }}">
                                                @if ($order->status == 5)
                                                    <span class="label label-block label-default">{{ $statusList[$order->status] }}</span>
                                                @elseif ($order->status == 2)
                                                    <span class="label label-block label-success">{{  $statusList[$order->status] }}</span>
                                                @elseif ($order->status == 3)
                                                    <span class="label label-block label-warning">{{  $statusList[$order->status] }}</span>
                                                @else
                                                    <span class="label label-block label-primary">{{  $statusList[$order->status] }}</span>
                                                @endif
                                            </a>
                                        </td>
                                        <td>
                                            {{ Carbon\Carbon::parse($order->created_at)->format('d/m/Y') }}
                                        </td>
                                        <td>
                                            <a href="#" class="text-danger"
                                                    data-toggle="modal"
                                                    data-target="#modal-delete" data-id="{{ $order->id }}">
                                                <i class="icon icon-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            @if ($orders->total() == 0)
                                <tr>
                                    <td colspan="6">
                                        Hiện tại không có kết quả nào phù hợp.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="row table-footer">
                        <div class="col-md-6">
                            <p class="summary-table-result">
                                @if($orders->count() > 0)
                                    Hiển thị từ <strong>{{ $currentOffset }}</strong>
                                    tới <strong>{{ $toOffset }}</strong>
                                    của <strong>{{ $orders->total() }}</strong> kết quả.
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            {{ $orders->links() }}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
    @include('admin.order.modal_change_stage', ['url' => route('order_chang_stage')])
    @include('admin.layout.include.modal_delete', ['url' => route('order_delete')])
@endsection
@section('js')
    <script>
        $('#modal-change-stage').on('show.bs.modal', function (e) {
            var itemName = $(e.relatedTarget).attr('data-name');
            $(e.currentTarget).find('input[name="code"]').val(itemName);
            $(e.currentTarget).find('#name-item').html(itemName);
        })
    </script>
@endsection
