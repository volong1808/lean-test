@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>{{ $title }}</h1>
            </div>

            <div class="heading-elements"></div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li class="active">Thông Tin Công Ty</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <div class="content">
        @if (!empty($errors->all()))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                <p>{{ config('constants.FORM_ERROR_MESSAGE') }}</p>
            </div>
        @endif
            @if(session('optionSaveSuccess'))
                <div class="alert alert-success alert-styled-left alert-dismissable">
                    <button type="button" class="close close-alert" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    <p>{{ session('optionSaveSuccess') }}</p>
                </div>
            @endif
        <form action="{{ route('config_website_post', $optionKey) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-9">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            @foreach ($configForm as $inputData)
                                @if ($inputData['inputType'] != 'file' && $inputData['inputType'] != 'radio')
                                    @include ('admin.layout.include.form_input', $inputData)
                                @else
                                    <?php
                                        $rightInput[$inputData['inputType']][] = $inputData;
                                    ?>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    @if (!empty($rightInput['radio']))
                        @foreach ($rightInput['radio'] as $radio)
                            <div class="panel-body panel panel-flat">
                                <div class="form-group mb-0">
                                    <label class="control-label">{{ $radio['label'] }}</label>
                                    @foreach($radio['item'] as $key => $item)
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="{{ $radio['inputName'] }}" class="styled"
                                                       value="{{ $key }}"
                                                        {{ $initValue[$radio['inputName']] == $key ? 'checked'  : '' }}>
                                                {{ $item }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <!-- panel -->
                    <div class="panel panel-flat">
                        <div class="panel-footer">
                            <div class="heading-elements">
                                <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-left">
                                    Cập Nhật
                                </button>
                                <?php $routeConfig =  route('config_website', config('constants.company_config_en.key')); ?>
                                <a href="{{ $routeConfig }}" type="submit" class="btn btn-default heading-btn pull-right">
                                    Cập Nhật Tiếng Anh
                                </a>
                            </div>
                        </div>
                    </div><!-- /panel -->
                        @if (!empty($rightInput['file']))
                            @foreach ($rightInput['file'] as $fileInput)
                                <div class="panel panel-flat">
                                    <div class="panel-body">
                                        <div class="form-group mb-0">
                                            <?php
                                            $imgsrc = empty($initValue[$fileInput['inputName']]) ? asset(config('constants.product_image.PLACE_HOLDER')) : asset($initValue[$fileInput['inputName']])
                                            ?>
                                            <label class="control-label">{{ $fileInput['label'] }}</label>
                                            <img src="{{ $imgsrc }}" class="img-responsive input-file__image"
                                                 id="file-image">
                                            <input type="file" name="{{ $fileInput['inputName'] }}" id="file"
                                                   class="input-file" data-cover="#file-image"/>
                                            <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
                                            <p class="help-block">
                                                Dung lượng tối đa khi tải lên 2M</p>
                                        </div>
                                    </div>
                                </div><!-- /panel -->
                            @endforeach
                        @endif

                </div>
            </div>
        </form>
    </div>
@endsection