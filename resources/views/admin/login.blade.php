<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | AutoTreo.com</title>

    <!-- favicon -->
    @include('include.favicon')

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/core.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('backend/css/main.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
</head>

<body class="login-container">

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Simple login form -->
                <form method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <img src="{{ asset('images/logo.png') }}" alt="Logo" class="login-logo">
                        </div>

                        @if($errors->has('username') || $errors->has('password'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ $errors->first('username') }}
                            </div>
                        @endif

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" placeholder="Tên Đăng Nhập" name="username"
                                   value="{{ old('username') }}" required autofocus autocomplete="username">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" class="form-control" placeholder="Mật Khẩu"
                                   name="password" required >
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <input class="form-check-input" type="checkbox" name="remember"
                                   id="remember" checked>
                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Đăng Nhập <i class="icon-circle-right2 position-right"></i></button>
                        </div>
                    </div>
                </form>
                <!-- /simple login form -->


                <!-- Footer -->
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

<!-- Core JS files -->
<script src="{{ asset('backend/js/plugins/loaders/pace.min.js') }}"></script>
<script src="{{ asset('backend/js/core/libraries/jquery.min.js') }}"></script>
<script src="{{ asset('backend/js/core/libraries/bootstrap.min.js') }}"></script>
<script src="{{ asset('backend/js/plugins/loaders/blockui.min.js') }}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script src="{{ asset('assets/js/app.js') }}"></script>
<!-- /theme JS files -->
</body>
</html>
