@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Trang Thêm Mới Feedback</h1>
            </div>

            <div class="heading-elements"></div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li><a href="{{ route('feedback_list') }}">Danh Sách Feedback</a></li>
                <li class="active">Thêm Mới</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->

    <!-- Dropzone -->
    <?php
    $dropzoneData = [
        'label' => 'Gallery Hình Ảnh',
        'dropzoneId' => 'gallery-dropzone',
    ]
    ?>
    <div class="content">
        @if (!empty($errors->all()))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                <p>{{ config('constants.FORM_ERROR_MESSAGE') }}</p>
            </div>
        @endif
        <form action="{{ route('feedback_add_submit') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include ('admin.feedback.form')
        </form>
    </div>
    <!-- /content area -->
@endsection