@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>Trang Quản Lý Liên Hệ</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('admin_index') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li class="active"><a href="{{ route('admin_contacts') }}">Danh Sách Liên Hệ</a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <!-- Table -->
        <div class="panel panel-flat">
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
            @endif<!-- table form -->
                <form action="{{ route('contact_bulk_action') }}" method="post">
                    @csrf
                    <div class="row table-control">
                        <div class="col-md-6">
                            <select name="action" class="form-control form-item-inline">
                                <option value="{{ config('constants.BULK_ACTION.none') }}">Tác Vụ</option>
                                <option value="{{ config('constants.BULK_ACTION.delete') }}">Xóa Đã Chọn</option>
                            </select>
                            <button type="submit" href="#" class="btn btn-default">Áp Dụng</button>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-framed">
                            <thead>
                            <tr>
                                <th width="50px">
                                    <label for="all-checkbox" class="hidden">Check All</label>
                                    <input type="checkbox" id="all-checkbox" class="styled" value="1">
                                </th>
                                <th width="300">Tên</th>
                                <th>Thông Tin Liên Hệ</th>
                                <th width="110">Trạng Thái</th>
                                <th>Ngày Gửi</th>
                                <th width="105"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($contacts->count() > 0)
                                <?php
                                /* @var $contacts */
                                $currentOffset = ($contacts->currentpage() - 1) * $contacts->perpage() + 1;
                                $toOffset = ($contacts->currentpage() - 1) * $contacts->perpage() + $contacts->count();
                                ?>
                                @foreach($contacts as $contact)
                                    <tr>
                                        <td>
                                            <input name="checkedPost[]" type="checkbox" class="styled table-checkbox"
                                                   value="{{ $contact->id }}">
                                        </td>
                                        <td>{{ $contact->name }}</td>
                                        <td>
                                            @isset ($contact->phone_number)
                                                <span>SĐT: {{ $contact->phone_number }}</span> <br>
                                            @endisset
                                            <span>Email:
                                            <a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a>
                                        </span>
                                        </td>
                                        <td>
                                            @if ($contact->status == 1)
                                                <span class="label label-block label-primary">Mới</span>
                                            @elseif ($contact->status == 2)
                                                <span class="label label-block label-success">Đã feedback</span>
                                            @else
                                                <span class="label label-block label-default">Đã hủy</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ Carbon\Carbon::parse($contact->created_at)->format('d/m/Y') }}
                                        </td>
                                        <td>
                                            <a href="{{ route('contact_show', $contact->id) }}"
                                               class="text-primary">
                                                <i class="icon-info3"></i>
                                            </a>
                                            <a href="#" class="text-danger"
                                                    data-toggle="modal"
                                                    data-target="#modal-delete" data-id="{{ $contact->id }}">
                                                <i class="icon icon-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            @if ($contacts->total() == 0)
                                <tr>
                                    <td colspan="6">
                                        Hiện tại không có kết quả nào phù hợp.
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="row table-footer">
                        <div class="col-md-6">
                            <p class="summary-table-result">
                                @if($contacts->count() > 0)
                                    Hiển thị từ <strong>{{ $currentOffset }}</strong>
                                    tới <strong>{{ $toOffset }}</strong>
                                    của <strong>{{ $contacts->total() }}</strong> kết quả.
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            {{ $contacts->links() }}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /content area -->
    @include('admin.layout.include.modal_delete', ['url' => route('contact_delete')])
@endsection
@section('page_js')
    <script>
        $('#delete-contact-modal').on('show.bs.modal', function (e) {
            var contactId = $(e.relatedTarget).attr('data-id');
            $(e.currentTarget).find('input[name="id"]').val(contactId);
        })
    </script>
@endsection
