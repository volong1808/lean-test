@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>Trang Quản Lý Liên Hệ</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ route('admin_index') }}"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li><a href="{{ route('admin_contacts') }}">Danh Sách Liên Hệ</a></li>
                <li class="active">Chi Tiết Liên Hệ</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <!-- Table -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">Thông Tin Liên Hệ</h6>
            </div>
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
                @endif
                <div class="form-group">
                    <label class="text-semibold control-label">Họ Và Tên:</label>
                    <div class="form-control-static">{{ $contact->name }}</div>
                </div>
                <div class="form-group">
                    <label class="text-semibold control-label">Địa Chỉ Email:</label>
                    <div class="form-control-static">
                        <a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a>
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-semibold control-label">Số Điện Thoại:</label>
                    <div class="form-control-static">{{ $contact->phone_number }}</div>
                </div>
                <div class="form-group">
                    <label class="text-semibold control-label">Nội Dung:</label>
                    <div class="form-control-static">{{ $contact->message }}</div>
                </div>
                <div class="form-group">
                    <label class="text-semibold control-label">Ngày Gởi:</label>
                    <div class="form-control-static">
                        {{ Carbon\Carbon::parse($contact->created_at)->format('d/m/Y') }}
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-semibold control-label" for="status-select">Trạng Thái:</label>
                    <div class="form-control-static">
                        <div class="status-input">
                            <select class="select form-control" id="status-select" name="status">
                                <option value="1" {{ ($contact->status == 1) ? 'selected' : '' }} disabled>Mới</option>
                                <option value="2" {{ ($contact->status == 2) ? 'selected' : '' }}>Đã Phản Hồi</option>
                                <option value="3" {{ ($contact->status == 3) ? 'selected' : '' }}>Đã Hủy</option>
                            </select>
                        </div>
                        <div class="status-message">
                            <span id="response-message"></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="form-group">
                    <a href="{{ route('admin_contacts') }}" class="btn btn-default btn-icon">
                        <i class="icon-arrow-left5 position-left"></i> Quay Lại
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
      var responseMessage = $('#response-message');
      $('#status-select').on('change', function(e) {
        var status = $($(e)[0].currentTarget).val();
        var data = {
          id: "{!! $contact->id !!}",
          status: status,
          _token: "{{ csrf_token() }}",
        };
        $.ajax({
          url: '{{ route('contact_update') }}',
          type: 'POST',
          data: data,
          success: function(data) {
            responseMessage.addClass('text-success');
            responseMessage.html(data);
          },
          error: function(data) {
            responseMessage.addClass('text-danger');
            responseMessage.removeClass('text-success');
            responseMessage.html(data.responseJSON);
          },
        });
      });
    </script>
@endsection
