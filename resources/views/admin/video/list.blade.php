@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Trang Quản Lý Video Hướng Dẫn</h1>
            </div>

            <div class="heading-elements">
                <a href="{{ route('video_add') }}" class="btn btn-labeled bg-blue heading-btn">
                    <b><i class="icon-plus2"></i></b> Thêm Mới
                </a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li class="active">Danh Sách Slide</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
        <!-- Simple panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger alert-styled-left alert-dismissable">
                        <button type="button" class="close close-alert" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        {{ $errors->first() }}
                    </div>
                @endif
                <!-- table form -->
                <form action="{{ route('video_bulk_action') }}" method="post">
                    @csrf
                    <div class="row table-control">
                        <div class="col-md-6">
                            <select name="action" class="form-control form-item-inline">
                                <option value="{{ config('constants.BULK_ACTION.none') }}">Tác Vụ</option>
                                <option value="{{ config('constants.BULK_ACTION.delete') }}">Xóa Đã Chọn</option>
                            </select>
                            <button type="submit" href="#" class="btn btn-default">Áp Dụng</button>
                        </div>
                        <div class="col-md-6 text-right">
                            <?php $langList = config('constants.LANG') ?>
                            @foreach ($langList as $key => $langItem)
                                <a href="{{ route('video_list', ['lang' => $key]) }}" class="btn-sm"><img style="width: 25px; height: 15px" src="{{ asset('images/' . $key . '_flag.png') }}"> {{ $langItem }}</a>
                            @endforeach
                        </div>
                    </div>
                    <!-- table -->
                    <div class="table-responsive">
                        <table class="table table-framed table-hover">
                            <thead>
                            <tr>
                                <th width="50px">
                                    <label for="all-checkbox" class="hidden">Check All</label>
                                    <input type="checkbox" id="all-checkbox" class="styled" value="1">
                                </th>
                                <th>Tên Video</th>
                                <th>Đường Dẫn</th>
                                <th>Trạng Thái</th>
                                <th>Thời Gian</th>
                                <th>Ngôn Ngữ</th>
                                <th width="120px"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (empty($posts))
                                <tr>
                                    <td colspan="6"><strong>Hiện tại chưa có video hướng dẫn nào.</strong></td>
                                </tr>
                            @else
                                <?php
                                /* @var $posts */
                                $currentOffset = ($posts->currentpage() - 1) * $posts->perpage() + 1;
                                $toOffset = ($posts->currentpage() - 1) * $posts->perpage() + $posts->count();
                                ?>
                                @foreach($posts as $post)
                                    <tr>
                                        <td>
                                            <input name="postID[]" type="checkbox" class="styled table-checkbox"
                                                   value="{{ $post->id }}">
                                        </td>
                                        <td>
                                            <a href="{{ route('video_edit', ['id' => $post->id]) }}">
                                                {{ $post->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <?php
                                            $video = $post->video;
                                            ?>
                                            @if (isset($video))
                                                {{ $video->url }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($post->state == config('constants.POST_STATE.PUBLISHED'))
                                                <span class="label label-info">Hiển Thị</span>
                                            @else
                                                <span class="label label-danger">Không Hiển Thị</span>
                                            @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::parse($post->updated_at)->format('d/m/Y') }}</td>
                                        <td>
                                            @if (!empty($post->langPost))
                                                <a href="{{ route('video_edit', $post->langPost->id) }}" title="Chỉnh sửa ngôn ngữ"><img style="width: 30px; height: 20px" src="{{ asset('images/en_flag.png') }}"></a>
                                            @else
                                                @if ($post->lang == config('constants.LANGUAGE_DEFAULT'))
                                                    <a href="{{ route('video_add', ['lang' => 'en', 'parrent_id' => $post->id]) }}"><i class="icon-plus3"></i></a>
                                                @else
                                                    <a href="{{ route('video_edit', $post->parrent_post_id) }}" title="Chỉnh sửa ngôn ngữ"><img style="width: 30px; height: 20px" src="{{ asset('images/vi_flag.png') }}"></a>
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('video_edit', ['id' => $post->id]) }}" class="text-warning" data-popup="tooltip" title="Chỉnh Sửa">
                                                <i class="icon-pencil7"></i>
                                            </a>
                                            <a href="#" class="text-black" data-toggle="modal" title="Xoá"  data-popup="tooltip"
                                                    data-target="#modal-delete" data-id="{{ $post->id }}">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /table -->
                    <!-- table footer -->
                    <div class="row table-footer">
                        <div class="col-md-6">
                            <p class="summary-table-result">
                                @if($posts->count() > 0)
                                    Hiển thị từ <strong>{{ $currentOffset }}</strong>
                                    tới <strong>{{ $toOffset }}</strong>
                                    của <strong>{{ $posts->total() }}</strong> kết quả.
                                @endif
                            </p>
                        </div>
                        <div class="col-md-6 text-right">
                            {{ $posts->links() }}
                        </div>
                    </div>
                </form>
                <!-- /table footer -->
            </div>
        </div>
        <!-- /simple panel -->
    </div>
    <!-- /content area -->
    @include('admin.layout.include.modal_delete', ['url' => route('video_delete')])
@endsection
