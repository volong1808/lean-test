@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Trang Thêm Mới Bài Viết</h1>
            </div>

            <div class="heading-elements"></div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li><a href="{{ route('job_list') }}">Danh Sách Bài Viết</a></li>
                <li class="active">Thêm Mới</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->

    <!-- Dropzone -->
    <?php
    $dropzoneData = [
        'label' => 'Gallery Hình Ảnh',
        'dropzoneId' => 'gallery-dropzone',
    ]
    ?>
    <div class="content">
        @if (!empty($errors->all()))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                <p>{{ config('constants.FORM_ERROR_MESSAGE') }}</p>
            </div>
        @endif
        <form action="{{ route('job_add_submit') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include ('admin.job.form')
        </form>
    </div>
    <!-- /content area -->
@endsection

@section('js')
    @include ('admin.layout.include.seo_form_js')
@endsection
