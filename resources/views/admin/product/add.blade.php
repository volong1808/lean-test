@extends('admin.layout.admin')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h1>Trang Thêm Mới Sản Phẩm</h1>
            </div>

            <div class="heading-elements"></div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-component">
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('admin_index') }}">
                        <i class="icon-home2 position-left"></i> Dashboard
                    </a>
                </li>
                <li><a href="{{ route('product_list') }}">Danh Sách Sản Phẩm</a></li>
                <li class="active">Thêm Mới</li>
            </ul>
            <ul class="breadcrumb-elements"></ul>
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->

    <!-- Dropzone -->
    <?php
    $dropzoneData = [
        'label' => 'Gallery Hình Ảnh',
        'dropzoneId' => 'gallery-dropzone',
        'hasMockItem' => true,
        'removeRoute' => route('delete_gallery_ajax'),
    ];
    if (!empty($gallery)) {
        $dropzoneData['initDzData'] = $gallery;
        $dropzoneData['removeRoute'] = route('delete_gallery_ajax');
    }
    ?>
    <div class="content">
        @if (!empty($errors->all()))
            <div class="alert alert-danger alert-styled-left alert-dismissable">
                <button type="button" class="close close-alert" data-dismiss="alert">
                    <span>×</span>
                </button>
                <p>{{ config('constants.FORM_ERROR_MESSAGE') }}</p>
            </div>
        @endif
        <form action="{{ route('product_add_submit') }}" method="post" enctype="multipart/form-data">
        @include ('admin.product.form', ['dropzoneData' => $dropzoneData])
        </form>
    </div>
    <!-- /content area -->
@endsection

@section('js')
    @include ('admin.layout.include.dropzone_js', $dropzoneData)
    @include ('admin.layout.include.seo_form_js')
    <script>
        function setBeforUnload() {
            if (window.onbeforeunload == null) {
                $(window).on("beforeunload", function () {
                    return "Are you sure you want to leave?";
                });
            };
        }
        $(document).ready(function () {
            $('input').on('input', function () {
                setBeforUnload();
            });
            $('textarea').on('keypress', function () {
                setBeforUnload();
            });
            $('.note-editable').on('DOMSubtreeModified', function () {
                setBeforUnload();
                });

            $('.permalink').hide();
            if ($('#permalink').val().length > 0 || $('.permalink > .help-block').text().length > 0) {
                $('.permalink').show();
            }
            $('#input-name').focusout(function () {
                if ($('#permalink').val().length > 0) {
                    return;
                }
                $('.permalink').show();
                var permalinkInput = $('#permalink');
                var permalinkContent = $('.permalink-content');
                var permalink = $('#input-name').val();
                var slug = slugify(permalink);
                console.log(slug);
                processDisplayPermalink(slug);
                permalinkContent.data('value', slug);
                permalinkInput.val(slug);
                hideEditPermalink();
            });

            var submitBtn = $('#submitFormBtn');

            submitBtn.on('mousedown', function () {
                $(window).off('unload');
                $(window).off('beforeunload');
            });

            submitBtn.on('mouseleave', function () {
                setBeforUnload();
                $(window).on('unload', function () {
                    $imageIds = [];
                    $('.dz-remove').each(function (i, el) {
                        $imageIds.push($(el).attr('data-dz-remove'));
                    });
                    $.ajax({
                        url: '{{ route('clear_gallery_ajax') }}',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            'imageIds': $imageIds,
                        },
                        async: false,
                        success: function (response) {}
                    });
                });
            });
        });

        $(window).on('unload', function () {
            $imageIds = [];
            $('.dz-remove').each(function (i, el) {
                $imageIds.push($(el).attr('data-dz-remove'));
            });
            $.ajax({
                url: '{{ route('clear_gallery_ajax') }}',
                type: 'post',
                dataType: 'json',
                data: {
                    'imageIds': $imageIds,
                },
                async: false,
                success: function (response) {}
            });
        });
    </script>
@endsection
