@csrf
<div class="row">
    <div class="col-md-9">
        <div class="panel panel-flat">
            <div class="panel-body">
                <?php
                $inputData = [
                    'label' => 'Tên Sản Phẩm',
                    'inputType' => 'text',
                    'inputName' => 'name',
                    'permalink' => true,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Nội Dung Sản Phẩm',
                    'inputType' => 'editor',
                    'inputName' => 'content',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Hảng Sản Xuất',
                    'inputType' => 'select',
                    'inputName' => 'change_log',
                    'permalink' => false,
                    'options' => $partner
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Xuất Xứ',
                    'inputType' => 'text',
                    'inputName' => 'producer',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Model',
                    'inputType' => 'text',
                    'inputName' => 'model',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)

                <?php
                $inputData = [
                    'label' => 'Link DownLoad',
                    'inputType' => 'text',
                    'inputName' => 'url',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <?php
                $inputData = [
                    'label' => 'Giá Sản Phẩm',
                    'inputType' => 'text',
                    'inputName' => 'price',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <?php
                $inputData = [
                    'label' => 'Giảm Giá',
                    'inputType' => 'text',
                    'inputName' => 'discount_price',
                    'permalink' => false,
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <?php
                $inputData = [
                    'label' => 'Lĩnh Vực',
                    'inputType' => 'select',
                    'inputName' => 'field_id',
                    'permalink' => false,
                    'options' => $fields
                ]
                ?>
                @include ('admin.layout.include.form_input', $inputData)
                <?php
                $tableConfig = [
                    'label' => 'Thuộc Tính Tóm Tắt',
                    'data' => !empty($initValue['properties_description']) ? json_decode($initValue['properties_description'], true) : null,
                    'name' => 'properties_description',
                    'columns' => [
                        'value' => [
                            'name' => 'Giá Trị',
                            'input' => [
                                'inputType' => 'text',
                                'inputName' => 'value',
                            ]
                        ]
                    ]
                ]
                ?>
                @include ('admin.layout.include.table_input', $tableConfig)
                <?php
                $tableConfig = [
                    'label' => 'Thuộc Tính',
                    'data' => !empty($initValue['properties']) ? json_decode($initValue['properties'], true) : null,
                    'name' => 'properties',
                    'columns' => [
                        'name' => [
                            'name' => 'Tên',
                            'input' => [
                                'inputType' => 'text',
                                'inputName' => 'name',
                            ]
                        ],
                        'value' => [
                            'name' => 'Giá Trị',
                            'input' => [
                                'inputType' => 'text',
                                'inputName' => 'value',
                            ]
                        ]
                    ]
                ]
                ?>
                @include ('admin.layout.include.table_input', $tableConfig)
            </div>
        </div>
        @isset ($dropzoneData)
            @include('admin.layout.include.dropzone', $dropzoneData)
        @endisset

    </div>
    <div class="col-md-3">
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Trạng Thái</label>
                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="1"
                            {{ ($initValue['state'] == config('constants.POST_STATE.PUBLISHED')) ? 'checked' : '' }}>
                            Hiển Thị
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="state" class="styled" value="0"
                            {{ ($initValue['state'] == config('constants.POST_STATE.UNPUBLISHED')) ? 'checked' : '' }}>
                            Không Hiển Thị
                        </label>
                    </div>
                </div>
                <div class="form-group mb-0">
                    <label class="control-label">Vị Trí</label>
                    <input type="number" class="form-control" name="order" value="{{ old('order', $initValue['order']) }}">
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Ngôn Ngữ</label>
                    <?php $langList = config('constants.LANG') ?>
                    @foreach ($langList as $key => $langItem)
                        <div class="radio">
                            <label>
                                <input type="radio" disabled class="styled" value="{{ $key }}"
                                        {{ ($initValue['lang'] == $key) ? 'checked' : '' }}>
                                {{ $langItem }}
                            </label>
                        </div>
                    @endforeach
                    <input type="hidden" name="lang" value="{{ !empty($initValue['lang']) ? $initValue['lang'] : '' }}">
                    <input type="hidden" name="parrent_id" value="{{ !empty($initValue['parent_id']) ? $initValue['parent_id'] : '' }}">
                </div>
            </div>
            <div class="panel-footer">
                <div class="heading-elements">
                    @if (isset($post))
                        <button id="submitFormBtn" type="submit" class="btn btn-warning heading-btn pull-right">
                            Cập Nhật
                        </button>
                    @else
                        <button id="submitFormBtn" type="submit" class="btn btn-primary heading-btn pull-right">
                            Thêm Mới
                        </button>
                    @endif
                </div>
            </div>
        </div><!-- /panel -->
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <label class="control-label">Danh Mục Sản Phẩm</label>
                    @if ($categories->count() > 0)
                        @foreach ($categories as $category)
                            <?php
                            $children = $category->children()->get();
                            ?>
                            @if ($children->count() === 0)
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled" name="categories[]"
                                               value="{{ $category->id }}"
                                        @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                        {{ $category->name }}
                                    </label>
                                </div>
                            @else
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="styled" name="categories[]"
                                               value="{{ $category->id }}"
                                               @if(in_array($category->id, $initValue['categories'])) checked @endif>
                                        {{ $category->name }}
                                    </label>
                                </div>
                                <div class="ml-1">
                                @foreach ($children as $child)
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="styled" name="categories[]"
                                                   value="{{ $child->id }}"
                                                   @if(in_array($child->id, $initValue['categories'])) checked @endif>
                                            {{ $child->name }}
                                        </label>
                                    </div>
                                @endforeach
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <!-- /panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
            <?php
            $inputData = [
                'label' => 'Mô Tả Ngắn',
                'inputType' => 'textarea',
                'inputName' => 'description',
                'permalink' => false,
            ]
            ?>
            @include ('admin.layout.include.form_input', $inputData)
            </div>
        </div>
        <!-- panel -->
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="form-group mb-0">
                    <?php
                    $imgsrc = empty($initValue['image']) ? asset(config('constants.product_image.PLACE_HOLDER')) : asset($initValue['image']->url)
                    ?>
                    <label class="control-label">Hình Đại Diện</label>
                    <img src="{{ $imgsrc }}" class="img-responsive input-file__image" id="file-image">
                    <input type="file" name="image" id="file" class="input-file" data-cover="#file-image" />
                    <label class="input-file__label btn" for="file">Chọn hình ảnh</label>
                    <p class="help-block">
                        Kích thước tốt nhất {{ config('constants.product_image.WIDTH') }}
                        x{{ config('constants.product_image.HEIGHT') }} (px)</p>
                </div>
            </div>
        </div><!-- /panel -->
    </div>
</div>
@include ('admin.layout.include.seo_form')

@section('js')
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-add-row').click(function () {
                let rowContent =  $(this).data('row');
                let table = $(this).data('table');
                let nameInput = $(this).data('name');
                let rowCount = $(table +' tbody  tr').length;
                let index = 0;
                if (rowCount > 0) {
                    $(table +' tbody  tr').each(function () {
                        let indexRow = $(this).data('index');
                        if (index < indexRow) {
                            index = indexRow;
                        }
                    });
                }
                index = index + 1;
                let row = $(rowContent).find('tbody').html();
                let regx = "/{index}/g";
                let regxName = "/{nameInput}/g";
                row = row.replace(eval(regx), index);
                row = row.replace(eval(regxName), nameInput);
                $(table).find('tbody').append(row.replace(eval(regx), index));
            });

            $(document).on('click', '.js-delete-row', function () {
                $(this).parent('td').parent('tr').remove();
            })
        })
    </script>
@endsection
