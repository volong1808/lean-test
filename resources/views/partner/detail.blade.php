@extends('frontend.layouts.master')
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home') }}">{{ trans('message.home_page') }}</a> »
                        <a href="{{ route('fr_job_list') }}">{{ trans('message.partner') }}</a></span></span></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="block block--padding block--grey-lightest">

        <div class="wrap">

            <div class="wysiwyg">

                <div class="box">

                    <h2 class="p1"><span class="s1"><b>{{ $newsDetail->name }}</b></span></h2>
                    <p class="p1"></p><p class="intro">{{ $newsDetail->description }}</p>
                    {!! $newsDetail->content !!}

                </div>

            </div>

        </div>

    </div>
    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>
    @include('frontend.includes.branch')
    <!-- footer -->
@endsection