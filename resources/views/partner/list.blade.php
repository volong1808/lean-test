@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home') }}">{{ trans('message.home_page') }}</a> » <span
                                        class="breadcrumb_last" aria-current="page">{{ trans('message.branch') }}</span></span></span></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    @if ($news->count() > 0)
    <div class="block block--padding block--grey-lightest">

        <div class="wrap">

            <div class="brand brand--large">
                <div class="brand__items">
                    @foreach ($news as $item)
                        <?php
                        $image = 'uploads/feedback/man-2.png';
                        if (!empty($item->image)) {
                            $image = $item->image->origin;
                        }
                        ?>
                        <div class="brand__item">

                            <a href="{{ route('branch_detail', ['slug' => $item->slug]) }}" class="brand-item">

                                <figure class="brand-item__image">
                                    <img src="{{ asset($image) }}" alt="{{ $item->name }}" class="attachment-brand-logo size-brand-logo wp-post-image" loading="lazy">                                    </figure>

                            </a>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
{{--        <div class="pagination pagination--products text-center">--}}
{{--            @include('frontend.includes.paging', ['pagination' => $news])--}}
{{--        </div>--}}
    </div>
    @endif
    <div class="block block--padding block--grey-lightest">

        <div class="wrap">

            <div class="wysiwyg">

                <div class="box">

                    <h2 class="p1"><span class="s1"><b>{{ $page->name }}</b></span></h2>
                    <p class="p1">
                    </p><p class="intro">
                        {{ $page->description }}</p>
                    <p></p>
                    <p class="p1">{!! $page->content !!}</p>

                </div>

            </div>

        </div>

    </div>
    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>

    @include('frontend.includes.subscribe_new_letter')
@endsection
