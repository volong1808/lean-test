@extends('frontend.layouts.master')
@section('title_head')
    {{ $banner['title'] }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- ==============================================
    **Blog section**
    =================================================== -->
    <section class="blog-section">
        <div class="container">
            @if (!empty($currentVideo) && !empty($currentVideo->video))
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <div class="blog-content blog-detailed">
                        <div class="video">
                            <div class="video__description">
                                {!!  $currentVideo->description !!}
                            </div>
                            <div class="video__play">
                                <?php $youTubeId = getYoutubeIdFromUrl($currentVideo->video->link) ?>
                                <iframe width="100%" height="400" src="https://www.youtube.com/embed/{{ $youTubeId }}?rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <h4 class="video__title">{{ $currentVideo->name }}</h4>
                            <div class="video__content">
                                {!!  $currentVideo->content !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    @if (!empty($posts))
                        <div class="video__lisst">
                            @foreach ($posts as $post)

                                <?php
                                    $name = $post->name;
                                    $slug = $post->slug;
                                    $linkYoutube = null;
                                    if (!empty($post->video)) {
                                        $linkYoutube = $post->video->link;
                                    }
                                ?>
                                <div class="video-item">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="video-item__image">
                                                <?php $imageUrl = getYoutubeThumbFromUrl($linkYoutube, 'maxresdefault')?>
                                                @if (!empty($imageUrl))
                                                    <a class="video-item__link" href="{{ route('support', $slug) }}"><img src="{{ $imageUrl }}" alt="{{ $name }}"></a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <h4 class="video-item__title"><a class="video-item__link" href="{{ route('support', $slug) }}">{{ $name }}</a></h4>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            @endif
        </div>
    </section>
    @include('frontend.includes.sing_up')
@endsection