@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ $banner['title'] }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])

    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home') }}">{{ trans('message.home_page') }}</a> » <span
                                        class="breadcrumb_last" aria-current="page">{{ trans('message.rental') }}</span></span></span></p>
                </div>
            </div>
        </div>
    </div>

    <div class="block block--padding block--grey-lightest">

        <div class="wrap">

            <div class="wysiwyg">

                <div class="box">
                    {!! $page->content !!}
                </div>

            </div>

        </div>

    </div>

    @include('frontend.includes.subscribe_new_letter')
@endsection
@section('script_page')
@endsection