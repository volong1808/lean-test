@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home') }}">{{ trans('message.home_page') }}</a> » <span
                                        class="breadcrumb_last" aria-current="page">{{ trans('message.field') }}</span></span></span></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="block block--padding block--grey-lightest">

        <div class="wrap">

            <div class="wysiwyg">

                <div class="box">

                    <h2 class="p1"><span class="s1"><b>{{ $page->name }}</b></span></h2>
                    <p class="p1"></p><p class="intro">{{ $page->description }}</p>
                    {!! $page->content !!}

                </div>

            </div>

        </div>

    </div>
    <div class="block block--padding block--grey-lightest">
        <div class="wrap">
            <div class="block__inner">
                <div class="block__body">
                    @if ($fields->count() > 0)
                        <div class="branche">
                            @foreach ($fields as $field)
                                <div class="branche__item">
                                    <a href="{{ route('fr_field_detail', $field->slug ) }}" target="" class="branche-item">
                                        <figure class="branche-item__image">
                                            <img width="640" height="640"
                                                 src="{{ !empty($field->image->origin) ? asset($field->image->origin) : '' }}"
                                                 class="attachment-square-image size-square-image" alt="{{ $field->name }}"
                                                 loading="lazy">
                                        </figure>
                                        <div class="branche-item__content">
                                            <div class="branche-item__label label">
                                                <span class="label__inner">{{ trans('message.field') }}</span>
                                            </div>
                                            <span class="branche-item__icon icon"></span>
                                            <h3 class="branche-item__title">{{ $field->name }}</h3>
                                        </div>
                                        <div class="branche-item__overlay"></div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>

    @include('frontend.includes.subscribe_new_letter')

@endsection
