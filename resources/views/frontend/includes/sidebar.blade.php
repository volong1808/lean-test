<aside class="col-md-4 col-lg-3">
    <div class="blog-sidebar">

        <!--Start Categories-->
        <div class="cmn-box">
            <h4>Danh Mục</h4>
            <ul>
                <li><a href="#">Hướng Dẫn <span class="count">65</span></a></li>
                <li><a href="#">Videos <span class="count">25</span></a></li>
                <li><a href="#">Blog <span class="count">48</span></a></li>
            </ul>
        </div>
        <!--End Categories-->

        <!--Start Recent Articles-->
        <div class="cmn-box archive">
            <h4>Bài Viết Mới</h4>
            @if (!empty($newPost))
                <?php
                    if (empty($routeDetail)) {
                        $routeDetail = 'blog_detail';
                    }
                ?>
                @foreach ($newPost as $item)
                    <?php
                    $img = $item->image;
                    $time = date('d-m-Y', strtotime($item->created_at));
                    $url = route($routeDetail, $item->slug);
                    ?>
                    <div class="article-box">
                        <figure class="article-pic">
                            <a href="{{ $url }}">
                                <img class="img-fluid"
                                     src="{{ isset($img) ? asset($img->origin) : asset(config('constants.post_image.PLACE_HOLDER')) }}"
                                     alt="{{ isset($img) ? $img->alt : '' }}">
                            </a>
                        </figure>
                        <a href="{{ $url }}"><p>{{ $item->name }}</p></a>
                        <p class="time">{{ $time }}</p>
                    </div>
                @endforeach
            @endif
        </div>
        <!--End Recent Articles-->

    </div>
</aside>
