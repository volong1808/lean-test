<div class="bottom">

    <div class="bottom__inner">

        <div class="wrap">

            <div class="bottom__items">

                <div class="bottom__item">

                    <div class="bottom__menus">
                        <div class="bottom__menu">
                            <div class="bottom-menu">
                                <h3 class="bottom-menu__heading">{{ trans('message.general_contact') }}</h3>
                                <address class="bottom-menu__adress">{!! $company['address'] !!}</address>
                                <ul class="contact-options">
                                    <li class="contact-options__option">
                                        <a href="tel:{!! $company['phone'] !!}" class="contact-options__link">
                                                <span class="contact-options__icon icon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                                            <span class="contact-options__label">{{ $company['phone'] }}</span>
                                        </a>
                                    </li>
                                    <li class="contact-options__option">
                                        <a href="mailto:{{ $company['email'] }}"
                                           class="contact-options__link">
                                                <span class="contact-options__icon icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                            <span class="contact-options__label">{{ $company['email'] }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="bottom__menu bottom__menu--large">
                            <div class="bottom-menu">
                                <h3 class="bottom-menu__heading">{{ trans('message.service') }}</h3>
                                <p class="bottom-menu__text">{{ trans('message.for_all_question') }}</p>
                                <ul class="contact-options">
                                    <li class="contact-options__option">
                                        <a href="tel:{{ $company['hot_line'] ?? '' }}" class="contact-options__link">
                                                <span class="contact-options__icon icon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                                            <span class="contact-options__label">{{ $company['hot_line'] ?? '' }}</span>
                                        </a>

                                    </li>


                                    <li class="contact-options__option">

                                        <a href="mailto:{{ $company['email_sale'] }}"
                                           class="contact-options__link">
                                                <span class="contact-options__icon icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                            <span class="contact-options__label">{{ $company['email_sale'] ?? '' }}</span>
                                        </a>

                                    </li>


                                </ul>


                                <a href="{{ route('fr_service_list' . $prefixLang) }}" class="bottom-menu__cto">
                                    <span class="bottom-menu__cto-label">{{ trans('message.service_view_all') }}</span>
                                    <span class="bottom-menu__cto-icon icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                                </a>


                            </div>

                        </div>

                        <div class="bottom__menu">
                            <div class="bottom-menu">
                                <h3 class="bottom-menu__heading">{{ trans('message.social_media') }}</h3>
                                <ul class="bottom-menu__social-items">
                                    <li class="bottom-menu__social-item bottom-menu__social-item--youtube">
                                        <a href="{{ $company['youtube'] }}"
                                           class="bottom-menu__social-link">
                                                <span class="bottom-menu__social-icon icon"><i class="fa fa-youtube-play" aria-hidden="true"></i></span>
                                            <span class="bottom-menu__social-label">Youtube</span>
                                        </a>
                                    </li>
                                    <li class="bottom-menu__social-item bottom-menu__social-item--twitter">
                                        <a href="{{ $company['twitter'] }}"
                                           class="bottom-menu__social-link">
                                                <span class="bottom-menu__social-icon icon"><i class="fa fa-twitter" aria-hidden="true"></i></span>
                                            <span class="bottom-menu__social-label">Twitter</span>
                                        </a>
                                    </li>
                                    <li class="bottom-menu__social-item bottom-menu__social-item--linkedin">

                                        <a href="{{ $company['linkedin'] }}"
                                           class="bottom-menu__social-link">
                                                <span class="bottom-menu__social-icon icon"><i class="fa fa-linkedin" aria-hidden="true"></i></span>
                                            <span class="bottom-menu__social-label">LinkedIn</span>
                                        </a>

                                    </li>


                                </ul>

                            </div>


                        </div>

                    </div>

                </div>


                <div class="bottom__item bottom__item--image">

                    <figure class="bottom__image-container">
                        <img width="400" height="270"
                             src="./Home - GMS Instruments_files/GMS_Instruments_Headquarters_2-400x270.jpg"
                             class="attachment-footer-image size-footer-image" alt="GMS Instruments Office"
                             loading="lazy"
                             srcset="https://gms-instruments.com/wp-content/uploads/2019/10/GMS_Instruments_Headquarters_2-400x270.jpg 400w, https://gms-instruments.com/wp-content/uploads/2019/10/GMS_Instruments_Headquarters_2-98x65.jpg 98w"
                             sizes="(max-width: 400px) 100vw, 400px"></figure>

                </div>


            </div>

        </div>

    </div>

</div>
<footer class="footer">

    <div class="wrap">

        <div class="footer__items">

            <div class="footer__item footer__item--copyright">
                <p class="footer__copyright-label">Copyright 2020 © - Lean Tes</p>
            </div>

            <ul class="footer__item footer__item--menu">
                <li class="footer__menu-item"><a href="{{ route('page' . $prefixLang, ['page_slug' => trans('message.shipping_and_return_slug')]) }}"
                                                 class="footer__menu-link" target="_blank"
                                                 rel="noopener noreferrer">{{ trans('message.shipping_and_return') }}</a></li>
                <li class="footer__menu-item"><a href="{{ route('page' . $prefixLang, ['page_slug' => trans('message.general_terms_and_conditions_slug')]) }}"
                                                 class="footer__menu-link" target="_blank"
                                                 rel="noopener noreferrer">{{ trans('message.shipping_and_return') }}</a></li>
                <li class="footer__menu-item"><a href="{{ route('page' . $prefixLang, ['page_slug' => trans('message.privacy_policy_slug')]) }}"
                                                 class="footer__menu-link" target="_blank"
                                                 rel="noopener noreferrer">{{ trans('message.privacy_policy') }}</a></li>
            </ul>

        </div>

    </div>

</footer><!-- .footer -->