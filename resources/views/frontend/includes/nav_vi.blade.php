<!-- header !-->
<header>
    <div class="header-top">
        <div class="container">
            <div class="box">
                <ul class="lang">
                    <li>
                        <a @if ($lang == 'vi') class="active" @endif href="{{ route('change_lang', ['lang' => 'vi']) }}"  title="{{ trans('message.lang_vi') }}"><img class="flg_lang" src="{{ asset('images/vi_flag.png') }}"></a>
                    </li>
                    <li>
                        <a @if ($lang == 'en') class="active" @endif href="{{ route('change_lang', ['lang' => 'en']) }}" title="{{ trans('message.lang_en') }}"><img class="flg_lang" src="{{ asset('images/en_flag.png') }}"></a>
                    </li>
                    <li>
                        <a @if ($lang == 'cn') class="active" @endif href="{{ route('change_lang', ['lang' => 'cn']) }}" title="{{ trans('message.lang_cn') }}"><img class="flg_lang" src="{{ asset('images/cn_flag.png') }}"></a>
                    </li>
                </ul>
                <ul class="contact-1">
                    <li class="website">
                        <a href="#" title="https://exco.com.vn">exco.com.vn</a>
                    </li>
                    <li class="hotline">
                        <a href="tel:{{ $company['phone'] }}" title="{{ $company['phone'] }}">{{ $company['phone'] }}</a>
                    </li>
                    <li class="email">
                        <a href="{{ $company['email'] }}" title="{{ $company['email'] }}">{{ $company['email'] }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="container">
            <div class="box">
                <div class="logo">
                    <a href="{{ route('home') }}" title="{{ trans('message.home_page') }}" class="img"><img src="{{ asset($company['logo']) }}"
                                                                            alt="{{ trans('message.home_page') }}" title="{{ trans('message.home_page') }}"></a></div>
                <div id="sticker">
                    <div class="sticker-container">
                        <div class="overlay-sticker"></div>
                        <div class="warpper">
                            <div class="head-sticker">
                                <p>Menu</p>
                                <div id="touch-out">
                                </div>
                                <div class="frm-seach">
                                    <form action="{{ route('fr_product_search') }}">
                                        <input type="text" name="k" placeholder="{{ trans('message.keyword') }}">
                                        <button></button>
                                    </form>
                                </div>
                            </div>
                            <div class="body-sticker">
                                <ul class="tbl">
                                    <li class="tbl-cell">
                                        <a href="{{ route('home') }}" title="{{ $company['name'] }}">{{ trans('message.home_page') }}</a>
                                    </li>
                                    <li class="tbl-cell">
                                        <a href="{{ route('about') }}" title="{{ trans('message.about_page') }}">{{ trans('message.about_page') }}</a>
                                        @if (!empty($aboutList))
                                        <ul>
                                            @foreach ($aboutList as $item)
                                            <li>
                                                <a href="{{ route('about_detail', $item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    <li class="tbl-cell menu-mega">
                                        <a href="{{ route('fr_product_list') }}" title="{{ trans('message.product_page') }}">{{ trans('message.product_page') }}</a>
                                        @if (!empty($productMenu))
                                            @include('frontend.includes.menu_product', ['productMenu' => $productMenu])
                                        @endif
                                    </li>
                                    <li class="tbl-cell">
                                        <a href="{{ route('fr_service_list') }}" title="{{ trans('message.service') }}">{{ trans('message.service') }}</a>
                                        @if (!empty($serviceList))
                                            <ul>
                                                @foreach ($serviceList as $item)
                                                    <li>
                                                        <a href="{{ route('fr_service_detail', $item->slug) }}" title="{{ $item->name }}">{{ $item->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                    <li class="tbl-cell">
                                        <a href="{{ route('news_list') }}" title="{{ trans('message.news') }}">{{ trans('message.news') }}</a>
                                    </li>
                                    <li class="tbl-cell">
                                        <a href="#" title="Đối Tác">{{ trans('message.partner') }}</a>
                                        <ul>
                                            <li>
                                                <a href="{{ route('front_customer_list') }}" title="Khách hàng">{{ trans('message.customer') }}</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="tbl-cell">
                                        <a href="{{ route('fr_job_list') }}" title="{{ trans('message.job') }}">{{ trans('message.job') }}</a>
                                    </li>
                                    <li class="tbl-cell">
                                        <a href="{{ route('contact') }}" title="Liên hệ">{{ trans('message.contact') }}</a>
                                    </li>
                                    <li class="tbl-cell mb-hidden">
                                        <a href="javascript:" rel="nofollow" class="btn-search"></a>
                                        <div class="form-search">
                                            <form action="{{ route('fr_product_search') }}" method="get">
                                                <input type="text" name="k" placeholder="{{ trans('message.keyword') }}">
                                                <button></button>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="touch-on">
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- end header -->