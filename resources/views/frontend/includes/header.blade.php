<header class="header">

    <div class="header__main">

        <div class="header-main">

            <div class="header-main__wrap">

                <div class="header-main__inner">

                    <div class="header-main__primary js-primary-header">

                        <div class="header-main__logo">

                            <figure class="logo">


                                <div class="logo__mobile"></div>

                                <div class="logo__desktop"><a href="{{ route('home' . $prefixLang) }}"><img alt="{{ $company['name'] }}"
                                                                   src="{{ asset($company['logo']) }}"></a></div>


                            </figure>

                        </div>
                        <div class="header-main__button header-main__button--navigation">

                            <button class="navigation-button js-navigation-button">

                                <span class="navigation-button__label">Menu</span>

                                <span class="navigation-button__icon">
			<span class="navigation-button__line"></span>
			<span class="navigation-button__line"></span>
			<span class="navigation-button__line"></span>
		</span>

                            </button>

                        </div>

                        <div class="header-main__mobile-language-switcher">


                            <div class="language-switcher js-switch">
                                <a href="{{ route('change_lang', ['lang' => 'en']) }}"
                                   class="language-switcher__item @if ($lang == 'en') language-switcher__item--active @endif">EN</a>
                                <a href="{{ route('change_lang', ['lang' => 'vi']) }}"
                                   class="language-switcher__item @if ($lang == 'vi') language-switcher__item--active @endif ">VI</a>
                            </div>
                        </div>

                    </div>

                    <div class="header-main__secondary">

                        <div class="header-main__navigation">

                            <ul id="menu-top-menu" class="secondary-menu">
                                <!--<li id="menu-item-279" class="secondary-menu__item"><a
                                            href="{{ route('page', ['page_slug' => trans('message.about_slug')]) }}"
                                            class="secondary-menu__link">{{ trans('message.about_page') }}</a></li>-->
                                <li id="menu-item-6988" class="secondary-menu__item"><a
                                            href="{{ route('blog_list') }}"
                                            class="secondary-menu__link">{{ trans('message.news') }}</a></li>
                                <li id="menu-item-6825" class="secondary-menu__item"><a
                                            href="{{ route('fr_job_list') }}"
                                            class="secondary-menu__link">{{ trans('message.job') }}</a></li>
                            </ul>
                        </div>
                        <div class="header-main__language-switcher">


                            <div class="language-switcher js-switch">
                                <a href="{{ route('change_lang', ['lang' => 'en']) }}"
                                   class="language-switcher__item @if ($lang == 'en') language-switcher__item--active @endif">EN</a>
                                <a href="{{ route('change_lang', ['lang' => 'vi']) }}"
                                   class="language-switcher__item @if ($lang == 'vi') language-switcher__item--active @endif">VI</a>
                            </div>
                        </div>

                    </div>

                    <div class="header-main__search">

                        <div class="search">
                            <form role="search" method="get" class="search__form"><input autocomplete="off" name="s"
                                                                                         type="search"
                                                                                         placeholder="{{ trans('message.keyword') }}"
                                                                                         class="search__input">
                                <button type="submit" aria-label="Search" class="search__button">
                                    <span class="search__button-icon icon"><i class="fa fa-search"
                                                                              aria-hidden="true"></i></span>
                                </button>
                            </form> <!---->
                        </div>

                    </div>

                </div><!-- .header-main__inner -->

            </div><!-- .header-main__wrap -->

        </div><!-- .header-main -->

    </div><!-- .header__main -->

    <div class="header__navigation">

        <nav class="navigation">

            <div class="navigation__inner">

                <div class="navigation__wrap">

                    <ul id="menu-navigatie" class="navigation-menu">
                        <li id="menu-item-113" class="navigation-menu__item navigation-menu__item--active"><a
                                    href="{{ route('home') }}" aria-current="page"
                                    class="navigation-menu__link">{{ trans('message.home_page') }}</a></li>
                        <li id="menu-item-113" class="navigation-menu__item"><a
                                    href="{{ route('page' . $prefixLang, ['page_slug' => trans('message.about_slug')]) }}" aria-current="page"
                                    class="navigation-menu__link">{{ trans('message.about_page') }}</a></li>
                        <li id="menu-item-153" class="navigation-menu__item"><a
                                    href="{{ route('fr_product_list' . $prefixLang) }}"
                                    class="navigation-menu__link">{{ trans('message.product_page') }}</a></li>
                        <li id="menu-item-278" class="navigation-menu__item"><a
                                    href="{{ route('page' . $prefixLang, ['page_slug' => trans('message.service_slug')]) }}"
                                    class="navigation-menu__link">{{ trans('message.service') }}</a></li>
                        <!--<li id="menu-item-8104" class="navigation-menu__item"><a
                                    href="{{ route('blog_list' . $prefixLang) }}"
                                    class="navigation-menu__link">{{ trans('message.news') }}</a>
                        </li>
                        <li id="menu-item-290" class="navigation-menu__item"><a
                                    href="{{ route('fr_job_list' . $prefixLang) }}"
                                    class="navigation-menu__link">{{ trans('message.job') }}</a>
                        </li>-->
                        <li id="menu-item-116" class="navigation-menu__item"><a
                                    href="{{ route('contact' . $prefixLang) }}"
                                    class="navigation-menu__link">{{ trans('message.contact') }}</a></li>
                    </ul>
                    <ul id="menu-top-menu-1" class="secondary-menu">
                        <li class="secondary-menu__item"><a
                                    href="{{ route('page', ['page_slug' => trans('message.about_slug')]) }}"
                                    class="secondary-menu__link">{{ trans('message.about_page') }}</a></li>
                        <li class="secondary-menu__item"><a
                                    href="{{ route('blog_list' . $prefixLang) }}"
                                    class="secondary-menu__link">{{ trans('message.news') }}</a></li>
                        <li class="secondary-menu__item"><a
                                    href="{{ route('fr_job_list' . $prefixLang) }}"
                                    class="secondary-menu__link">{{ trans('message.job') }}</a></li>
                    </ul>
                </div>

            </div>

        </nav>

    </div>

</header><!-- .header -->