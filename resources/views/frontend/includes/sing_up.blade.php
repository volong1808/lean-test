<section class="signup-outer gradient-bg padding-lg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <ul class="clearfix">
                    <li> <span class="icon-men"></span>
                        <h4>Đăng ký <span>tài khoản</span></h4>
                    </li>
                    <li> <span class="icon-chat"></span>
                        <h4>Tìm hiểu <span>tải về</span></h4>
                    </li>
                    <li> <span class="icon-lap"></span>
                        <h4>Cài đặt <span>sử đụng</span></h4>
                    </li>
                </ul>
                <div class="signup-form">
                    <form action="{{ route('front_en_login_post') }}" method="post">
                        {{ csrf_field() }}
                        @if(session('successRegister'))
                            <div class="alert alert-success">
                                <strong>Thành công!</strong>  {{ session('loginSuccess') }}.
                            </div>
                        @endif
                        <div class="email">
                            <input name="username" type="text" placeholder="Nhập Tên Đăng Nhập">
                        </div>
                        @if ($errors->has('username'))
                            <label class="red-text">{{ $errors->first('username') }}</label>
                        @endif
                        <div class="password">
                            <input name="password" type="password" placeholder="Nhập Mật KHẩu">
                        </div>
                        @if ($errors->has('password'))
                            <label class="red-text">{{ $errors->first('password') }}</label>
                        @endif
                        <button class="signup-btn">Đăng Nhập</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>