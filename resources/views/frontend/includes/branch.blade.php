<div class="block block--padding block--grey-lightest">
    <div class="wrap">
        <div class="brand">
            <div class="brand__columns">
                <div class="brand__column">
                    <div class="brand__column-inner">
                        <div class="brand__header">
                            <h2 class="brand__title">{{ $premierBrands->name }}</h2>
                            <span class="brand__subtitle subtitle">{{ $premierBrands->description }}</span>
                        </div>
                        <div class="brand__text">
                            <p class="p1">{!! $premierBrands->content !!}</p>
                        </div>
                    </div>
                </div>

                <div class="brand__column brand__column--brands">
                    <div class="brand__column-inner">
                        @if ($topBranch->count() > 0)
                            <div class="brand__items">
                                @foreach ($topBranch as $branch)
                                    <div class="brand__item">
                                        <a href="{{ route('branch_detail', ['slug' => $branch->slug]) }}" target="" class="brand-item">
                                            <div class="brand__inner">
                                                <figure class="brand__image">
                                                    <img width="170" height="65" src="{{ !empty($branch->image->thumb) ? asset($branch->image->thumb) : '' }}" class="attachment-brand-logo size-brand-logo" alt="{{ $branch->name }}" loading="lazy">
                                                </figure>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                                <div class="brand__item brand__item--all">
                                    <a href="{{ route('branch_list') }}" class="brand-item" target="">
                                        <span class="brand__label">{{ trans('message.branch_view_all') }}</span>
                                        <span class="brand__icon icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>