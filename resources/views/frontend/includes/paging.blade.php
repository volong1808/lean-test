@if ($pagination->lastPage() > 1)
<div class="block__footer">
    <div class="wp-pagenavi" role="navigation">
        <?php
        /**
         * @var $pagination
         */
        $currentRoute = \Illuminate\Support\Facades\Route::currentRouteName();
        $params = \Illuminate\Support\Facades\Route::current()->parameters();
        if (\Illuminate\Support\Facades\Request::get('s')) {
            $params = array_merge($params, ['s' => \Illuminate\Support\Facades\Request::get('s')]);
        }
        $maxShowPage = config('constants.MAX_PAGE_SHOW');
        $pageStart = $pagination->currentPage();
        if ($pageStart > 1) {
            $pageStart = $pageStart - 1;
        }
        $maxShowPage = $maxShowPage + $pageStart;
        if ($maxShowPage > $pagination->lastPage()) {
            $maxShowPage = $pagination->lastPage();
        }
        $prevPage = $pagination->currentPage() - 1;
        if ($prevPage < 1) {
            $prevPage = 1;
        }
        $nextPage = $pagination->currentPage() + 1;
        if ($nextPage > $pagination->lastPage()) {
            $nextPage = $pagination->lastPage();
        }
        ?>
        @if($pagination->currentPage() != 1)
        <a class="prevpostslink" rel="next" href="{{ route($currentRoute, array_merge($params, ['page' => $prevPage])) }}">«</a>
        @endif
        @for ($i = $pageStart; $i <= $maxShowPage; $i++)
            @if($i == $pagination->currentPage())
                <span aria-current="page" class="current">{{ $i }}</span>
            @else
                <a class="page larger" title="{{ $i }}" href="{{ route($currentRoute, array_merge($params, ['page' => $i])) }}">{{ $i }}</a>
            @endif
        @endfor
        @if($pagination->currentPage() != $pagination->lastPage())
        <a class="nextpostslink" rel="next" href="{{ route($currentRoute, array_merge($params, ['page' => $nextPage])) }}">»</a>
        @endif
    </div>
</div>
@endif

