<section class="banner-outer">
    <div class="banner-slider">
        @foreach ($slides as $slide)
            <?php
            /* @var $slide */
            $image = $slide->image;
            $meta = $slide->slide;
            ?>
            @isset($image)
                <div class="banner mobile-img" style="background: url('{{ asset($image->url) }}') no-repeat; background-size: 100%;">
{{--                    <div class="banner-slider__content">--}}
{{--                        <div class="banner-slider__title">--}}
{{--                            <h2>{{ $slide->name }}</h2>--}}
{{--                        </div>--}}
{{--                        <div class="banner-slider__description">--}}
{{--                            <h3>{{ $slide->description }}</h3>--}}
{{--                        </div>--}}
{{--                        @if (!empty($meta))--}}
{{--                            <div class="banner-slider__button">--}}
{{--                                <a href="{{ $meta->link }}" class="btn get-started">{{ $meta->button }}</a>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                    </div>--}}
                </div>
            @endisset
        @endforeach
    </div>
</section>
<!-- end slider -->
