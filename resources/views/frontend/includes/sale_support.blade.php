<div class="wrap">
    <div class="call-to-action">
        <div class="call-to-action__image-container">
            <figure class="call-to-action__image">
                <img width="930" height="930" src="{{ asset('images/sale_support.jpg') }}" class="attachment-cta-image size-cta-image" alt="Lea-Tes"></figure>
        </div>
        <div class="call-to-action__content">
            <div class="call-to-action__header">
                <h2 class="call-to-action__title">{{ trans('message.sale_support_help') }}</h2>
                <p class="call-to-action__text intro">{{ trans('message.sale_support_help_description') }}</p>
            </div>
            <div class="call-to-action__items">
                <div class="call-to-action__item">
                    <div class="cto-item">
                        <figure class="cto-item__image">
                            <div class="cto-item__overlay"></div>
                            <img width="100" height="100" src="{{ asset('images/sale_support.jpg') }}" class="attachment-team-member-small-image size-team-member-small-image"></figure>
                        <div class="cto-item__content">
                            <div class="cto-item__title">
                                <h3>{{ $company['seller'] ?? '' }}</h3>
                                <p>{{ $company['seller_position'] ?? '' }}</p>
                            </div>
                            <ul class="cto-item__items">
                                <li class="cto-item__item">
                                    <a href="tel:{{ $company['hot_line'] }}">
                                                    <span class="link-icon icon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                                        {{ $company['hot_line'] }}</a>
                                </li>
                                <li class="cto-item__item">
                                    <a href="mailto:{{ $company['email_sale'] ?? '' }}">
                                                    <span class="link-icon icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                        {{ $company['email_sale'] }}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="call-to-action__item">
                    <div class="cto-item">
                        <figure class="cto-item__image">
                            <img width="100" height="100" src="{{ asset('images/catalog_cover.jpg') }}" class="attachment-team-member-small-image size-team-member-small-image">                                    </figure>
                        <div class="cto-item__content">
                            <h3 class="cto-item__title">{{ trans('message.product_catalog') }}</h3>
                            <ul class="cto-item__items">
                                <li class="cto-item__item">{{ trans('message.download_catalog_product') }}</li>
                                <li class="cto-item__item">
                                    <a target="_blank" href="#" class="cto-item__link">{{ trans('message.download') }}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>