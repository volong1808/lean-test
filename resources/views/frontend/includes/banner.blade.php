<div class="block">
    <div class="jumbotron jumbotron--large">
        <div class="jumbotron__visuals">
            <div class="jumbotron__aspect-ratio"></div>
            <figure class="jumbotron__image">
                <img width="1680" height="500" src="{{ url($bannerConfig['image']) }}"
                     class="attachment-jumbotron-image size-jumbotron-image" alt="{{ $bannerConfig['alt'] ?? '' }}" loading="lazy"
                     data-object-fit="cover">
            </figure>
            <div class="jumbotron__overlay overlay-visual"></div>
        </div>
        <div class="jumbotron__inner">
            <div class="wrap">
                <div class="jumbotron__label">
                    <div class="jumbotron__label-inner">

                        <h1 class="jumbotron__title">{{  $bannerConfig['title'] ?? '' }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>