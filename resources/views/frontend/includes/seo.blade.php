@isset($seoData)
<meta name="title" content="{{ $seoData->name }}">
<meta name="description" content="{{ $seoData->description }}">
<meta property="og:title" content="{{ $seoData->name }}">
<meta property="og:description" content="{{ $seoData->description }}">
@endisset
@if (!empty($seoData->image))
<meta property="og:image" content="{{ asset($seoData->image->origin) }}">
@elseif (!empty($company['logo']))
<meta property="og:image" content="{{ asset($company['logo']) }}">
@endif
<link rel="canonical" href="{{ \Illuminate\Support\Facades\URL::current() }}"/>
@isset($ogType)
<meta property="og:type" content="{{ $ogType }}">
@endisset
<meta property="og:url" content="{{ \Illuminate\Support\Facades\Request::url() }}">
