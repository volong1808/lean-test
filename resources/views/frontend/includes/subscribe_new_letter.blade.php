<div class="block block--padding block--padding-small block--grey-light">

    <div class="wrap">

        <div class="newsletter">

            <div class="newsletter__info">
                <h3 class="newsletter__title">{{ trans('message.subscribe_to_email') }}</h3>
                <p class="newsletter__text">{!! trans('message.subscribe_to_email_description') !!}<p>
            </div>
            <div class="newsletter__form">
                <div class="gf_browser_chrome gform_wrapper">
                    <div id="success-form-subscribe" class="message__form message__form--success"></div>
                    <form method="post" enctype="multipart/form-data"id="form-subscribe"
                          action="{{ route('contact_subscribe_' . $lang) }}">
                        @csrf
                        <div class="gform_body">
                            <ul class="gform_fields top_label form_sublabel_below description_below">
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--text">
                                    <div class="ginput_container">
                                        <input type="text" name="name" id="name" placeholder="{{ trans('message.name_customer_email_placeholder') }}" />
                                    </div>
                                    <div id="error-name" class="message__error"></div>
                                </li>
                                <li class="gfield gfield_contains_required field_sublabel_below field_description_below gfield_visibility_visible gfield--email">
                                    <div class="ginput_container">
                                        <input type="email" name="email" id="email" placeholder="{{ trans('message.your_email') }}" />
                                    </div>
                                    <div id="error-email" class="message__error"></div>
                                </li>
                            </ul>
                        </div>
                        <div class="gform_footer top_label">
                            <button class="button button--grey">
                                <span class="button__inner"><span class="button__label">{{ trans('message.subscribe') }}</span><span
                                            class="button__icon icon"><i class="fa fa-arrow-right" aria-hidden="true"></i></span></span>
                            </button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>