<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title_head', 'Trang Chủ')</title>

    <!-- favicon -->
    @include('include.favicon')
    @include('frontend.includes.seo')

    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Iconmoon -->
    <link href="{{ asset('assets/iconmoon/css/iconmoon.css') }}" rel="stylesheet">
    <!-- Style Page -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Archivo&family=Montserrat&family=Oswald:wght@300;400&display=swap" rel="stylesheet">
    <style type="text/css">
        .tk-myriad-pro-condensed {
            font-family: 'Oswald', sans-serif;
        }

        .tk-myriad-pro {
            font-family: 'Oswald', sans-serif;
        }
    </style>
    <style type="text/css">@font-face {
            font-family: myriad-pro-condensed;
            src: url(https://use.typekit.net/af/c63dde/0000000000000000000170a8/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff2"), url(https://use.typekit.net/af/c63dde/0000000000000000000170a8/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff"), url(https://use.typekit.net/af/c63dde/0000000000000000000170a8/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("opentype");
            font-weight: 700;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: myriad-pro-condensed;
            src: url(https://use.typekit.net/af/f72c04/0000000000000000000170a4/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"), url(https://use.typekit.net/af/f72c04/0000000000000000000170a4/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"), url(https://use.typekit.net/af/f72c04/0000000000000000000170a4/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");
            font-weight: 400;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: myriad-pro;
            src: url(https://use.typekit.net/af/1b1b1e/00000000000000000001709e/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff2"), url(https://use.typekit.net/af/1b1b1e/00000000000000000001709e/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("woff"), url(https://use.typekit.net/af/1b1b1e/00000000000000000001709e/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3) format("opentype");
            font-weight: 700;
            font-style: normal;
            font-display: auto;
        }

        @font-face {
            font-family: myriad-pro;
            src: url(https://use.typekit.net/af/cafa63/00000000000000000001709a/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff2"), url(https://use.typekit.net/af/cafa63/00000000000000000001709a/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("woff"), url(https://use.typekit.net/af/cafa63/00000000000000000001709a/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3) format("opentype");
            font-weight: 400;
            font-style: normal;
            font-display: auto;
        }</style>
    <style type="text/css">iframe#_hjRemoteVarsFrame {
            display: none !important;
            width: 1px !important;
            height: 1px !important;
            opacity: 0 !important;
            pointer-events: none !important;
        }
    </style>
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

    @yield('css_page')
    @if (!empty($plugin['google_analytics']))
        {!! $plugin['google_analytics'] !!}
    @endif

    <script type="text/javascript" src="{{ asset('js/manifest.js') }}" id="theme-manifest-js"></script>
    <script type="text/javascript" src="{{ asset('js/vendor.js') }}" id="theme-vendor-js"></script>
{{--    <script type="text/javascript" src="{{ asset('js/jquery.js') }}" id="jquery-core-js"></script>--}}
</head>
<body>

<div class="wrapper">

    <!-- header -->
    @include('frontend.includes.header')
    <!-- /header -->
    @yield('content')
    @include('frontend.includes.footer')

</div><!-- .wrapper -->
<div id="loading" style="display:none">
    <img src="{{ asset('images/loading.gif') }}" alt="Loading..."/>
</div>
<!-- jQuery library -->
<script src="{{ asset('js/vendor/jquery-3.5.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/scripts.js') }}" id="jquery-core-js"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<!-- Bootsrap JS -->
@yield('script_page')
</body>
</html>
