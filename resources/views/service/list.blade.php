@extends('frontend.layouts.master', ['ogType' => 'article'])
@section('title_head')
    {{ $title }}
@endsection
@section('content')
    @include('frontend.includes.banner', ['bannerConfig' => $banner])
    <!-- break crum -->
    <div class="block block--grey-light">
        <div class="breadcrumb">
            <div class="wrap">
                <div class="breadcrumb__inner"><p><span><span><a href="{{ route('home') }}">{{ trans('message.home_page') }}</a> » <span
                                        class="breadcrumb_last" aria-current="page">{{ trans('message.service') }}</span></span></span></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end break crum -->
    <div class="block block--padding block--grey-lightest">

        <div class="wrap">

            <div class="wysiwyg">

                <div class="box">

                    <h2 class="p1"><span class="s1"><b>{{ $page->name }}</b></span></h2>
                    <p class="p1"></p><p class="intro">{{ $page->description }}</p>
                    {!! $page->content !!}

                </div>

            </div>

        </div>

    </div>
    <div class="block block--padding block--pattern">
        <div class="wrap">
            <div class="block__inner">
                <div class="block__header block__header--center">
                    <h2 class="block__title">{{ trans('message.service') }}</h2>
                    <span class="block__subtitle subtitle">{{ trans('message.service_description') }}</span>
                </div>
                <div class="block__body">
                    @if ($services->count() > 0)
                        <div class="service">
                            @foreach ($services as $service)
                                <div class="service__item">
                                    <a href="{{ route('fr_service_detail', ['slug' => $service->slug]) }}" class="service-item">
                                        <div class="service-item__header">
                                            <div class="service-item__icon-outer">
                                                <div class="service-item__icon-inner">
                                                    <div class="service-item__icon icon">
                                                        <img src="{{ !empty($service->image->thumb) ? asset($service->image->thumb) : '' }}" alt="{{ $service->name }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <h3 class="service-item__title">{{ $service->name }}</h3>
                                            <span class="service-item__arrow icon"><svg id="ico-arrow-right" viewBox="0 0 16 15.84"><path d="M0 11V4.89h8.08V0L16 7.92l-7.92 7.92V11z" fill="currentColor"></path></svg></span>
                                        </div>
                                        <div class="service-item__body">
                                            <p class="service-item__excerpt">{{ $service->description }}</p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="block block--padding block--grey-lightest">
        @include('frontend.includes.sale_support')
    </div>

    @include('frontend.includes.subscribe_new_letter')

@endsection
