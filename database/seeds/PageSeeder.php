<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->truncate();
        $faker = Faker\Factory::create();
        $pages = [
            'Trang Chủ',
            'Sản Phẩm',
            'Giới Thiệu',
            'Hướng Dẫn',
            'Blog',
            'Q&A',
            'Liên Hệ'
        ];
        foreach ($pages as $page) {
            $data = [
                'name' => $page,
                'description' => $faker->sentence(10),
                'content' => $faker->sentence(20),
                'slug' => str_slug($page),
                'page_type' => config('constants.PAGE_TYPE.STATIC'),
                'state' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
            DB::table('pages')->insert($data);
        }
    }
}
