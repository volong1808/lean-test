<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->truncate();
        DB::table('images')->truncate();
        DB::table('metadata_plan')->truncate();
        DB::table('metadata_slide')->truncate();
        $faker = Faker\Factory::create();
        $postType = config('constants.POST_TYPE');
        foreach ($postType as $type) {
            $data = [
                'description' => $faker->sentence(10),
                'content' => $faker->sentence(20),
                'post_type' => $type,
                'state' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
            if ($type == config('constants.POST_TYPE.PLAN')) {
                $planItems = [
                    'Gói Sản Phẩm 1',
                    'Gói Sản Phẩm 2',
                    'Gói Sản Phẩm 3'
                ];
                $planTime = [
                    1,
                    2,
                    3
                ];
                $planPrice = [
                    1680000,
                    2590000,
                    3240000
                ];
                foreach ($planItems as $key => $planItem) {
                    $data['name'] = $planItem;
                    $data['slug'] = str_slug($planItem);
                    $postId = DB::table('posts')->insertGetId($data);
                    $metaData = [
                        'post_id' => $postId,
                        'time' => $planTime[$key],
                        'price' => $planPrice[$key],
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                        'is_recommend' => $key == 1 ? 1 : 0
                    ];
                    DB::table('metadata_plan')->insert($metaData);
                }
            } elseif ($type == config('constants.POST_TYPE.SLIDE')) {
                $images = [
                    'images/slides/slide-1.png',
                    'images/slides/slide-2.png',
                    'images/slides/slide-4.jpg',
                ];
                foreach ($images as $image) {
                    $absolutePath = getImagePath($image);
                    $file = pathinfo($absolutePath);
                    $imageData = [
                        'origin' => $image,
                        'url' => $image,
                        'thumb' => $image,
                        'file_name' => $file['filename'],
                        'file_path' => 'images/slides',
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                    $imageId = DB::table('images')->insertGetId($imageData);
                    $slideData = [
                        'name' => 'FbGlobal',
                        'description' => 'FbGlobal là phần mềm tự động hóa các thao tác người dùng Facebook',
                        'content' => '',
                        'slug' => 'fb-global',
                        'post_type' => $type,
                        'image_id' => $imageId,
                        'state' => 1,
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                    $postId = DB::table('posts')->insertGetId($slideData);
                    $slideMetaData = [
                        'post_id' => $postId,
                        'button' => 'Xem Thêm',
                        'link' => 'http://fb.test/san-pham/fb-global',
                        'created_at' => \Carbon\Carbon::now(),
                        'updated_at' => \Carbon\Carbon::now(),
                    ];
                    DB::table('metadata_slide')->insert($slideMetaData);
                }
            } else {
                for($i = 0; $i < 30; $i++) {
                    $name = $faker->sentence(3);
                    $data['name'] = $name;
                    $data['slug'] = str_slug($name);
                    DB::table('posts')->insert($data);
                }
            }
        }
    }
}
