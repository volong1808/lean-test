<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        $faker = Faker\Factory::create();
        DB::table('users')->insert([
            'username' => 'sadmin',
            'email' => 'sadmin@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 1,
            'status' => 1,
            'email_verified_at' => Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' =>  \Carbon\Carbon::now(),
        ]);

        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 2,
            'status' => 1,
            'email_verified_at' => Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' =>  \Carbon\Carbon::now(),
        ]);

        DB::table('users')->insert([
            'username' => 'mod',
            'email' => 'mod@gmail.com',
            'password' => bcrypt('123456'),
            'role' => 3,
            'status' => 1,
            'email_verified_at' => Carbon\Carbon::now(),
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' =>  \Carbon\Carbon::now(),
        ]);

        for ($i = 1; $i <= 30; $i++) {
            $data = [
                'username' => "user{$i}",
                'email' => "user{$i}@gmail.com",
                'password' => bcrypt('123456'),
                'name' => $faker->name,
                'role' => 9,
                'status' => 1,
                'email_verified_at' => Carbon\Carbon::now(),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' =>  \Carbon\Carbon::now(),
            ];
            DB::table('users')->insert($data);
        }
    }
}
