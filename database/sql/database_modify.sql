ALTER TABLE metadata_product
ADD COLUMN model VARCHAR(50) NULL,
ADD COLUMN producer VARCHAR(100) NULL;

ALTER TABLE metadata_product
ADD COLUMN price INT NULL;

ALTER TABLE orders DROP plan_id;

ALTER TABLE orders DROP username;

ALTER TABLE `order_item` ADD `order_id` INT NOT NULL AFTER `product_id`;

ALTER TABLE `metadata_product` ADD `discount_price` INT NULL AFTER `change_log`;

ALTER TABLE `metadata_product` CHANGE `change_log` `change_log` INT NULL;

ALTER TABLE `posts` ADD `parrent_post_id` INT NULL AFTER `id`;

ALTER TABLE `posts` ADD `lang` VARCHAR(50) NULL DEFAULT 'vi' AFTER `updated_at`;

ALTER TABLE `metadata_product` CHANGE `change_log` `change_log` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE `pages` ADD `parrent_post_id` INT NULL AFTER `id`;

ALTER TABLE `pages` ADD `lang` VARCHAR(50) NULL DEFAULT 'vi' AFTER `updated_at`;

ALTER TABLE `categories` ADD `parrent_post_id` INT NULL AFTER `id`;

ALTER TABLE `categories` ADD VARCHAR(50) NULL DEFAULT 'vi' AFTER `updated_at`;

ALTER TABLE `posts` ADD `order` INT NULL AFTER `updated_at`;
UPDATE `posts` SET `order`= 99 WHERE 1

ALTER TABLE `categories` ADD `order` INT NULL DEFAULT '99' AFTER `updated_at`;