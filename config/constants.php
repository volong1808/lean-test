<?php

return [
    'role' => [
        'SADMIN' => 1,
        'ADMIN' => 2,
        'MOD' => 3,
        'USER' => 9
    ],
    'USER_STATUS' => [
        'ACTIVE' => 1,
        'INACTIVE' => 0
    ],
    'PER_PAGE' => 20,
    'POST_TYPE' => [
        'PRODUCT' => 'product',
        'SLIDE' => 'slide',
        'POST' => 'post',
        'PLAN' => 'plan',
        'VIDEO' => 'video',
        'FAQ' => 'faq',
        'FEEDBACK' => 'feedback',
        'JOB' => 'job',
        'FIELD' => 'field',
        'SERVICE' => 'service',
        'PARTNER' => 'partner',
        'CUSTOMER' => 'customer'
    ],
    'PAGE_TYPE' => [
        'STATIC' => 'page_static',
    ],
    'POST_STATE' => [
        'PUBLISHED' => 1,
        'UNPUBLISHED' => 0
    ],
    'message' => [
        'errorMessage' => 'Lỗi! Thực hiện thao tác không thành công. Xin vui lòng thử lại.'
    ],
    'BULK_ACTION' => [
        'none' => '1',
        'delete' => '2',
    ],
    'product_image' => [
        'WIDTH' => 600,
        'HEIGHT' => 600,
        'THUMB_WIDTH' => 300,
        'THUMB_HEIGHT' => 300,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],
    'slide_image' => [
        'WIDTH' => 1600,
        'HEIGHT' => 550,
        'THUMB_WIDTH' => 250,
        'THUMB_HEIGHT' => 100,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],
    'post_image' => [
        'WIDTH' => 590,
        'HEIGHT' => 350,
        'THUMB_WIDTH' => 260,
        'THUMB_HEIGHT' => 195,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'faq_image' => [
        'WIDTH' => 100,
        'HEIGHT' => 100,
        'THUMB_WIDTH' => 50,
        'THUMB_HEIGHT' => 50,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'page_static_image' => [
        'WIDTH' => 1920,
        'HEIGHT' => 600,
        'THUMB_WIDTH' => 300,
        'THUMB_HEIGHT' => 100,
        'PLACE_HOLDER' => 'backend/images/page_place_holder.jpg',
    ],

    'feedback_image' => [
        'WIDTH' => 200,
        'HEIGHT' => 200,
        'THUMB_WIDTH' => 150,
        'THUMB_HEIGHT' => 150,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'job_image' => [
        'WIDTH' => 263,
        'HEIGHT' => 187,
        'THUMB_WIDTH' => 263,
        'THUMB_HEIGHT' => 187,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'video_image' => [
        'WIDTH' => 1366,
        'HEIGHT' => 563,
        'THUMB_WIDTH' => 400,
        'THUMB_HEIGHT' => 600,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'service_image' => [
        'WIDTH' => 590,
        'HEIGHT' => 350,
        'THUMB_WIDTH' => 270,
        'THUMB_HEIGHT' => 350,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'partner_image' => [
        'WIDTH' => 590,
        'HEIGHT' => 350,
        'THUMB_WIDTH' => 150,
        'THUMB_HEIGHT' => 50,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'customer_image' => [
        'WIDTH' => 300,
        'HEIGHT' => 300,
        'THUMB_WIDTH' => 100,
        'THUMB_HEIGHT' => 100,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    'field_image' => [
        'WIDTH' => 640,
        'HEIGHT' => 640,
        'THUMB_WIDTH' => 300,
        'THUMB_HEIGHT' => 300,
        'PLACE_HOLDER' => 'images/product_place_holder_image.jpg',
    ],

    /* Dir path */
    'UPLOAD_PATH' => './upload/',

    'IMAGE_SIZE' => [
        'SLIDE' => [
            'width' => 1900,
            'height' => 705
        ],
    ],

    'avatar' => [
        'WIDTH' => 640,
        'HEIGHT' => 515,
        'THUMB_WIDTH' => 100,
        'THUMB_HEIGHT' => 100,
    ],

    'FORM_ERROR_MESSAGE' => 'Lỗi ! Có lỗi với dữ liệu bạn nhập. Xin vui lòng kiểm tra và thử lại.',
    'PLAN_STATE' => [
        'NEW' => 1,
        'PAYMENT' => 2,
        'CANCEL' => 3,
    ],

    'NEWS_HOME_PAGE_ITEM' => 10,
    'NEWS_PAGE_ITEM' => 12,
    'MAX_PAGE_SHOW' => 6,

    'company_config' => [
        'key' => 'company',
        'title' => 'Trang Quản Lý Thông Tin Công Ty'
    ],
    'company_config_en' => [
        'key' => 'company_en',
        'title' => 'Trang Quản Lý Thông Tin Công Ty Tiếng Anh'
    ],
    'banking_config' => [
        'key' => 'banking',
        'title' => 'Trang Quản Lý Hình Thức Thanh Toán'
    ],

    'plugin_config' => [
        'key' => 'plugin',
        'title' => 'Trang Quản Lý Script Plugin'
    ],

    'CONTACT_STATUS' => [
        'NEW' => 1,
        'FEEDBACKED' => 2,
        'CANCELED' => 3,
    ],
    'CONTACT_EMAIL_TO' => 'tudongsohar@gmail.com',

    'IS_API_LOGIN' => 1,
    'Is_API_NO_LOGIN' => 0,
    'ERROR_CODE' => [
        'NAME' => 21,
        'USERNAME' => 22,
        'EMAIL' => 23,
        'PHONE' => 24,
        'PASSWORD' => 25,
        'OTHER' => 2
    ],

    'TOOL_DOWNLOAD_DIR' => 'tools',

    'IS_REMEMBER_LOGIN' => 1,

    'PAYMENT_DEFAULT' => 'in_company',

    'ORDER_STATUS' => [
        'NEW' => 1,
        'PAID' => 2,
        'EXPIRED ' => 3,
        'VN_PAY' => 4,
        'CANCELED ' => 5,
    ],

    'ORDER_STATUS_LABEL' => [
        1 => 'Mới',
        2 => 'Đã thanh toán',
        3 => 'Hết Hạn',
        4 => 'Thanh Toán Tực Tuyến',
        5 => 'Đã Hủy',
    ],

    'ORDER_STATUS_DEFAULT' => 1,
    'ORDER_CODE_LENGTH' => 12,
    'PAYMENT_ONLINE_KEY' => 'online',
    'PAYMENT_SUCCESS_CODE' => '00',

    'PUBLIC_STATE_WEBSITE' => 1,

    'MIN_PAYMENT' => 5000,
    'LANG' => [
        'vi' => 'Tiếng Việt',
        'en' => 'Tiếng Anh'
    ],
    'LANGUAGE_DEFAULT' => 'vi',
    'COMPANY_CONFIG_KEY' => [
        'vi' => 'company',
        'en' => 'company_en'
    ],
];
