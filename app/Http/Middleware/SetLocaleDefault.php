<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Support\Facades\Session;

class SetLocaleDefault
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = Session::get('lang');
        $langConfig = config('constants.LANG.' . $lang);
        $slug = 'home';
        if (empty($langConfig)) {
            $lang = config('constants.LANGUAGE_DEFAULT');
            $slug = 'home';
        } else {
            $slug = $slug . '_' . $lang;
        }
        App::setLocale($lang);
        return redirect()->route($slug);
    }
}
