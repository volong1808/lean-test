<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Support\Facades\Session;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = Session::get('lang');
        $langConfig = config('constants.LANG.' . $lang);
        if (empty($langConfig)) {
            $lang = config('constants.LANGUAGE_DEFAULT');
        }
        App::setLocale($lang);
        return $next($request);
    }
}
