<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'address' => 'required|max:500',
            'phone_number' => 'required|regex:/^[0-9]{10,11}$/',
            'message' => 'required|max:500',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Tên',
            'email' => 'Email',
            'address' => 'Địa chỉ',
            'phone_number' => 'Điện Thoại',
            'message' => 'Nội Dung',
        ];
    }

    /**
     * @return array
     */
    public function  messages()
    {
        return [
            'regex' => ':attribute phải là số và có độ dài từ 10 đến 11 ký tự số.',
        ];
    }
}
