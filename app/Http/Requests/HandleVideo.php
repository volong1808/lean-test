<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleVideo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'max:500|nullable',
            'image' => 'image|mimes:jpeg,png,jpg|max:2000',
            'url' => 'url|max:255|nullable',
            'slug' => 'required|max:255|regex:/^[a-zA-Z0-9-_]+$/i',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tiêu đề Video là bắt buộc, tối đa 255 kí tự',
            'name.max' => 'Tối đa 255 kí tự',
            'description.max' => 'Tối đa 500 kí tự',
            'slug.required' => 'Định danh là bắt buộc',
            'slug.max' => 'Đinh danh (VI) ít hơn :max ký tự',
            'slug.regex' => 'Đinh danh (VI) chỉ được nhâp các ký tự a-z, A-Z, 0-9, ký tự -, _',
            'url.url' => 'Link không đúng định dạng',
            'url.max' => 'Link quá dài (tối đa 255 ký tự)',
        ];
    }
}
