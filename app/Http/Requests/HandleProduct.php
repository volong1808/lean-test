<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'max:500',
            'image' => 'image|mimes:jpeg,png,jpg|max:2000',
            'slug' => 'required|max:255|regex:/^[a-zA-Z0-9-_]+$/i',
            'url' => 'max:255|nullable',
            'price' => 'integer|nullable',
            'discount_price' => 'integer|nullable',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tên sản phẩm là bắt buộc tối đa 255 kí tự',
            'name.max' => 'Tối đa 255 kí tự',
            'description.max' => 'Tối đa 500 kí tự',
            'image.image' => 'Vui lòng chỉ upload hình ảnh định dạng jpeg, png, jpg',
            'image.mimes' => 'Vui lòng chỉ upload hình ảnh định dạng jpeg, png, jpg',
            'image.max' => 'Vui lòng upload hình ảnh nhỏ hơn 2MB',
            'slug.required' => 'Định danh là bắt buộc',
            'slug.max' => 'Đinh danh (VI) ít hơn :max ký tự',
            'slug.regex' => 'Đinh danh (VI) chỉ được nhâp các ký tự a-z, A-Z, 0-9, ký tự -, _',
            'url.url' => 'Link download không đúng định dạng',
            'url.max' => 'Link download quá dài(tối đa 255 ký tự)',
            'price.integer' => 'Giá Sản Phẩm Phải Là Số',
            'discount_price.integer' => 'Giảm Gía Phải Là Số',
        ];
    }
}
