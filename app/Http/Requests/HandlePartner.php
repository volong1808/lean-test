<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class HandlePartner extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $currentRouteName = Route::currentRouteName();
        $imageRule = 'nullable|image|mimes:jpeg,png,jpg|max:2000';
        if ($currentRouteName == 'slide_do_edit' ||
            $currentRouteName == 'partner_do_edit' ||
            $currentRouteName == 'customer_do_edit') {
            $imageRule = 'nullable|image|mimes:jpeg,png,jpg|max:2000';
        }
        return [
            'name' => 'nullable|max:255',
            'description' => 'max:500',
            'image' => $imageRule,
            'url' => 'url|max:255|nullable',
            'youtube_link' => 'url|max:255|nullable',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tiêu Đề Slide là bắt buộc, tối đa 255 kí tự',
            'name.max' => 'Tối đa 255 kí tự',
            'description.max' => 'Tối đa 500 kí tự',
            'image.image' => 'Vui lòng chỉ upload hình ảnh định dạng jpeg, png, jpg',
            'image.mimes' => 'Vui lòng chỉ upload hình ảnh định dạng jpeg, png, jpg',
            'image.max' => 'Vui lòng upload hình ảnh nhỏ hơn 2MB',
            'url.url' => 'Link download không đúng định dạng',
            'url.max' => 'Link download quá dài(tối đa 255 ký tự)',
        ];
    }
}
