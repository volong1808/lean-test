<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class HandleUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . Auth::user()->id . '|max:255',
            'phone' => 'numeric|nullable',
            'avatar' => 'image|mimes:jpeg,png,jpg|max:2000',
        ];
        if(! empty($this->input('new_password') . $this->input('new_password_confirm'))) {
            $rules['new_password'] = 'required|min:6';
            $rules['new_password_confirm'] = 'required|same:new_password';
            $rules['password'] = ['required', function($attribute, $value, $fail) {
                if (! password_verify($value, Auth::user()->password)) {
                    return $fail('Mật khẩu cũ không đúng');
                }
            }];
        }
        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Họ tên là bắt buộc tối đa 255 kí tự',
            'name.max' => 'Tối đa 255 kí tự',
            'email.required' => 'Email là bắt buộc',
            'email.email' => 'Email không hợp lệ',
            'email.max' => 'Tối đa 255 kí tự',
            'email.unique' => 'Email đã được sử dụng',
            'phone.numeric' => 'Số diện thoại khống đúng',
            'avatar.image' => 'Vui lòng chỉ upload hình ảnh định dạng jpeg, png, jpg',
            'avatar.mimes' => 'Vui lòng chỉ upload hình ảnh định dạng jpeg, png, jpg',
            'avatar.max' => 'Vui lòng upload hình ảnh nhỏ hơn 2MB',
            'password.required' => 'Mật khảu cũ không được để trống',
            'new_password.required' => 'Mật khảu không được để trống',
            'new_password.min' => 'Mật khẩu phải dài hơn 6 ký tự',
            'new_password_confirm.required' => 'Chưa xác nhận lại mật khẩu',
            'new_password_confirm.same' => 'Mật khẩu không trùng khớp',
        ];
    }
}
