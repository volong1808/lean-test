<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleOption extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $configForm = $this->getConfigOption();
        if (empty($configForm)) {
            abort(404);
        }
        foreach ($configForm as $key => $inputData)
        {
            if (!empty($inputData['valid'])) {
                $rules[$inputData['inputName']] = $inputData['valid'];
            }
        }
        return $rules;
    }

    public function attributes()
    {
        $attributes = [];
        $configForm = $this->getConfigOption();
        foreach ($configForm as $key => $inputData)
        {
            if (!empty($inputData['valid'])) {
                $attributes[$inputData['inputName']] = $inputData['label'];
            }
        }

        return $attributes;
    }

    public function messages()
    {
        return [
            'regex' => ':attribute không hợp lệ.',
        ];
    }

    protected function getConfigOption()
    {
        $optionKey = $this->route('key');
        $configForm = config("option.{$optionKey}");
        return $configForm;
    }
}
