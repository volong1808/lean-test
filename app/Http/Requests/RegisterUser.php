<?php

namespace App\Http\Requests;

use Dotenv\Exception\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class RegisterUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users', 'email'),
            ],
            'phone' => [
                'required',
                'regex:/^[0-9]{10,11}$/',
                Rule::unique('users', 'phone'),
            ] ,
            'username' => [
                'required',
                'regex:/^[A-Za-z][A-Za-z0-9]{5,31}$/',
                Rule::unique('users', 'username'),
            ] ,
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'name' => 'Tên',
            'username' => 'Tên Đăng Nhập',
            'email' => 'Email',
            'phone_number' => 'Số Điện Thoại',
            'password' => 'Mật Khẩu',
            'password_confirmation' => 'Mật Khẩu Nhập Lại'
        ];
    }

    /**
     * @return array
     */
    public function  messages()
    {
        return [
            'regex' => ':attribute không hợp lệ',
            'confirmed' => 'Mật Khẩu Không Khớp.'
        ];
    }
}
