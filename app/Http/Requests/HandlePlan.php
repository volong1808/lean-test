<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandlePlan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'product_id' => 'required',
            'description' => 'max:500',
            'price' => 'required|numeric',
            'time' => 'required|numeric',
            'position' => 'numeric|nullable'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tên gói sản phẩm là bắt buộc tối đa 255 kí tự',
            'name.max' => 'Tối đa 255 kí tự',
            'product.required' => 'Chọn một sản phẩm',
            'description.max' => 'Tối đa 500 kí tự',
            'price.required' => 'Giá và Thời Gian Sử Dụng là bắt buộc và là số',
            'price.numeric' => 'Giá và Thời Gian Sử Dụng là bắt buộc và là số',
            'time.required' => 'Giá và Thời Gian Sử Dụng là bắt buộc và là số',
            'time.numeric' => 'Giá và Thời Gian Sử Dụng là bắt buộc và là số',
            'position.numeric' => 'Vị Trí Sắp Xếp phải là số',
        ];
    }
}
