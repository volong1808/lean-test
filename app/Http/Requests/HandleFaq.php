<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleFaq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required|max:500',
            'position' => 'numeric|nullable'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Câu hỏi là bắt buộc tối đa 255 kí tự',
            'name.max' => 'Tối đa 255 kí tự',
            'description.required' => 'Trả lời là bắt buộc',
            'description.max' => 'Tối đa 500 kí tự',
            'position.numeric' => 'Vị Trí Sắp Xếp phải là số',
        ];
    }
}
