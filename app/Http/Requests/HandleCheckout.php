<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleCheckout extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $bankingConfig = config('option.banking');
        $allBankingType = array_keys($bankingConfig);
        $allBankingType = implode(',', $allBankingType);
        $rules = [
            'name' => 'required|max:255',
            'phone' => 'required|regex:/^[0-9]{10,11}$/',
            'email' => 'nullable|email',
            'message' => 'nullable|max:1000',
            'banking_type' => 'required|in:' . $allBankingType,
        ];

        $skipRequideAddrress = [
            'cod', 'cash', 'in_company'
        ];

        if (in_array($this->banking_type, $skipRequideAddrress)) {
            $rules['address'] = 'required|max:255';
        } else {
            $rules['address'] = 'nullable|max:255';
        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => 'Tên',
            'email' => 'Email',
            'phone' => 'Điện Thoại',
            'message' => 'Nội Dung',
            'address' => 'Địa Chỉ',
            'banking_type' => 'Hình Thức Thanh Toán'
        ];
    }
}
