<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Mail\ResetPassworUserMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\RegisterUser;
use Illuminate\Support\Facades\Validator;

class APIUserController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->user = new UserService();
    }

    public function doRegister(Request $request)
    {
        $registerUserValid = new RegisterUser();
        $rules = $registerUserValid->rules();
        $validStage = Validator::make($request->all(), $rules);
        if ($validStage->fails()) {
            $errors = $validStage->errors();
            $codeErrors = $this->GetCodeErrorValidation($errors);
            echo $codeErrors;
            return;
        }
        $dataUser = $request->all();
        $registerStage = $this->user->add($dataUser);
        if ($registerStage) {
            echo 1;
            return;
        }
        echo 0;
        return;

    }

    public function doLogin(Request $request)
    {
        if ($this->user->isLogged($request->username)) {
            echo 3;
            return;
        }
        $roleUser = config('constants.role.USER');
        $loginStage = $this->user->verifyUserApiLogin($request->username, $request->password, $roleUser);
        if ($loginStage) {
            $codeReturn = 1;
            $this->user->setStatusLogged($request->username, config('constants.IS_API_LOGIN'));
            $time = time();
            $this->user->setTimeLogin($request->username, $time);
            $this->user->setUserApiLogin($request->username);
            echo $codeReturn;
            return;
        }
        echo 0;
        return;
    }

    public function pingLogin()
    {
        $timePing = time();
        $userLogin = $this->user->getUserApiLogin();
        $user = $this->user->getByUserName($userLogin['username']);
        if (empty($user)) {
            echo 0;
            return;
        }
        $timeWait = $timePing - $user->time_login;
        if ($timeWait > env('MAX_TIME_WAIT_LOGIN')) {
            $this->user->setStatusLogged($userLogin['username'], 0);
            $this->user->apiLogout();
            echo 0;
            return;
        }
        $this->user->setTimeLogin($userLogin['username'], $timePing);
        echo 1;
        return;
    }

    public function doReset(Request $request)
    {
        $newPassword = generateRandomString();
        $user = $this->user->updatePassword($request->username, $newPassword);
        if ($user == false) {
            echo 0;
            return;
        }
        $data = [
            'email' => $user->email,
            'name' => $user->name,
            'newPassword' => $newPassword,
        ];
        $emailReset = new ResetPassworUserMail($data);
        Mail::to($request->email)->send($emailReset);
        echo 1;
        return;
    }

    public function logout()
    {
        $logoutStage = $this->user->apiLogout();
        if ($logoutStage) {
            echo 1;
            return;
        }
        echo 0;
        return;
    }

    protected function GetCodeErrorValidation($errors)
    {
        if ($errors->has('name')) {
            return config('constants.ERROR_CODE.NAME');
        }
        if ($errors->has('username')) {
            return config('constants.ERROR_CODE.USERNAME');
        }
        if ($errors->has('email')) {
            return config('constants.ERROR_CODE.EMAIL');
        }
        if ($errors->has('phone')) {
            return config('constants.ERROR_CODE.PHONE');
        }
        if ($errors->has('password') || $errors->has('password_confirmation')) {
            return config('constants.ERROR_CODE.PASSWORD');
        }
        return config('constants.ERROR_CODE.OTHER');
    }
}
