<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Post as Post;
use App\Image as Image;

class AdminController extends Controller
{
    public $metadataFields = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Auth::check()) {
            Redirect::route('login');
        }
        return view('admin.index');
    }

    /**
     * @param $postId
     * @param $postType
     * @return bool
     */
    public function canDeletePost($postId, $postType = '')
    {
        if (is_array($postId)) {
            $posts = Post::whereIn('id', $postId)->where('post_type', $postType)->get();
            if (count($posts) == count($postId)) {
                return true;
            }
            return false;
        }
        $post = Post::where('id', $postId)->where('post_type', $postType)->get();
        if (!empty($post)) {
            return true;
        }
        return false;
    }

    /**
     * @param $image
     */
    public function deleteImage($image)
    {
        if (isset($image)) {
            $imagePath = base_path() . DIRECTORY_SEPARATOR . $image->url;
            $originPath = base_path() . DIRECTORY_SEPARATOR . $image->origin;
            $thumbPath = base_path() . DIRECTORY_SEPARATOR . $image->thumb;
            if (file_exists($imagePath) AND !is_dir($imagePath)) {
                unlink($imagePath);
            }
            if (file_exists($originPath) AND !is_dir($originPath)) {
                unlink($originPath);
            }
            if (file_exists($thumbPath) AND !is_dir($thumbPath)) {
                unlink($thumbPath);
            }
            Image::destroy($image->id);
        }
        return;
    }
}
