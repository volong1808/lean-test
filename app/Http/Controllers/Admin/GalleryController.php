<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Helpers\PostHelper;
use App\Post as Post;
use Illuminate\Http\Request;
use App\Image;
use Illuminate\Support\Facades\DB;

class GalleryController extends AdminController
{
    use PostHelper;

    /**
     * GalleryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function add(Request $request)
    {
        if ($this->validateAjax($request)) {
            return response("Không thể tải ảnh lên", 403);
        }
        $postType = $request->postType;
        $postId = $request->postId;
        $imageId = $this->savePostImage($postType, $request);

        $galery = new Gallery();
        $galery->post_id = $postId;
        $galery->image_id = $imageId;
        $galery->save();
        return response()->json(['image_id' => $imageId], 200);
    }

    public function delete(Request $request)
    {
        $postId = $request->postId;
        $imageId = $request->imageId;
        $gallery = Gallery::where('post_id', $postId)->where('image_id', $imageId);

        $this->handleDelete($imageId);
        $gallery->delete();
        return response()->json(["deleted"], 200);
    }

    public function handleDelete($imageId)
    {
        $image = Image::find($imageId);
        if (isset($image)) {
            $imagePath = base_path() . DIRECTORY_SEPARATOR . $image->url;
            $thumbPath = base_path() . DIRECTORY_SEPARATOR . $image->thumb;
            $originPath = base_path() . DIRECTORY_SEPARATOR . $image->origin;
            if (file_exists($imagePath) AND !is_dir($imagePath)) {
                unlink($imagePath);
            }
            if (file_exists($thumbPath) AND !is_dir($thumbPath)) {
                unlink($thumbPath);
            }
            if (file_exists($originPath) AND !is_dir($originPath)) {
                unlink($originPath);
            }
            $image->delete();
        }
    }

    public function clear(Request $request)
    {
        $imageIds = $request->imageIds;
        if (empty($imageIds)) {
            return response()->json([], 200);
        }
        foreach ($imageIds as $imageId) {
            $gallery = Gallery::where('post_id', 0)->where('image_id', $imageId)->first();
            if(!empty($gallery)){
                $this->handleDelete($gallery->image_id);
                $gallery->delete();
            }
        }
        return response()->json([], 200);
    }

    public function validateAjax($request)
    {
        if (!isset($request->postId) || !isset($request->postType) || !isset($request->image)) {
            return true;
        }
        if ($request->postId != 0 && empty(Post::find($request->postId))) {
            return true;
        }
        return false;
    }
}
