<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\HandleFaq;
use App\Http\Requests\HandleVideo;
use App\Post as Post;
use Illuminate\Http\Request;
use App\Helpers\PostHelper;
use App\Http\Requests\HandleSlide;
use Illuminate\Support\Facades\DB;

/**
 * Class FaqController
 * @package App\Http\Controllers\Admin
 */
class FaqController extends AdminController
{
    protected $postType;

    protected $deleteMessage;

    public $metadataFields = [
        'position' => null,
    ];

    use PostHelper;

    /**
     * SlideController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postType = config('constants.POST_TYPE.FAQ');
        $this->deleteMessage = 'Bạn vừa thực hiện xóa thành công faqs.';
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::where('post_type', $this->postType)
            ->orderBy('updated_at', 'DESC')->paginate(config('constants.PER_PAGE'));
        $data = [
            'posts' => $posts,
            'title' => 'Trang Quản Lý FAQs (Câu Hỏi Thường Gặp)',
        ];
        return view('admin.faq.list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        if ($this->canDeletePost($itemId, $this->postType)) {
            $this->handleDelete($itemId);
            return redirect()->back()->withSuccess($this->deleteMessage);
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('postID');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (!empty($itemIds) AND $this->canDeletePost($itemIds, $this->postType)) {
                foreach ($itemIds as $itemId) {
                    $this->handleDelete($itemId);
                }
                return redirect()->back()->withSuccess($this->deleteMessage);
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * @param $postId
     */
    public function handleDelete($postId)
    {
        $post = Post::find($postId);
        $image = $post->image;
        if (!empty($image)) {
            $this->deleteImage($image);
        }
        if (!empty($this->metadataFields)) {
            DB::table('metadata_' . $this->postType)->where('post_id', $post->id)->delete();
        }
        Post::destroy($post->id);
        return;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $data['title'] = 'Trang Thêm Mới FAQs (Câu Hỏi Thường Gặp)';
        $data['initValue'] = $this->getPostInputData($this->postType);
        $data['categories'] = Category::whereNull('parent_id')->where('post_type',$this->postType)->get();
        return view('admin.faq.add', $data);
    }

    /**
     * @param HandleFaq $request
     * @return mixed
     */
    public function doAdd(HandleFaq $request)
    {
        $postId = $this->savePost($this->postType, $request);
        return redirect(route('faq_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới faq.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật FAQs (Câu Hỏi Hướng Dẫn)';
        $data['initValue'] = $this->getPostInputData($this->postType, $post);
        $data['post'] = $post;
        $data['categories'] = Category::whereNull('parent_id')->where('post_type',$this->postType)->get();
        return view('admin.faq.edit', $data);
    }

    /**
     * @param HandleFaq $request
     * @param $id
     * @return mixed
     */
    public function doEdit(HandleFaq $request, $id)
    {
        $post = Post::findOrFail($id);
        $postId = $this->savePost($this->postType, $request, $post);
        return redirect(route('faq_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa cập nhật thành công faq.');
    }
}
