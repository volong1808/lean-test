<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EditorController extends Controller
{
    public function upload(Request $request)
    {
        if ($request->ajax()) {
            $image_url = '';
            $imagedetails = getimagesize($_FILES['file']['tmp_name']);
            $imageOrigin = getUploadImageUrl(
                $request->file,
                $imagedetails[0],
                $imagedetails[1],
                'origin'
            );
            if (!$imageOrigin['url']) {
                $success = false;
                $message = 'Has error when upload file.';
            } else {
                $success = true;
                $message = 'You have been upload success image.';
                $image_url = asset($imageOrigin['url']);
            }
        } else {
            $success = false;
            $message = 'Have problem with your request.';
        }

        $data = [
            'success' => $success,
            'message' => $message,
            'image_url' => $image_url,
            'csrf_token' => csrf_token(),
        ];
        echo json_encode($data);
        return;

    }
}
