<?php

namespace App\Http\Controllers\Admin;

use App\Category as Category;
use App\CategoryPost;
use App\Helpers\CategoryHelper;
use App\Http\Requests\HandleCategory;
use Illuminate\Http\Request;

/**
 * Class ProductCategoryController
 * @package App\Http\Controllers\Admin
 */
class ProductCategoryController extends AdminController
{
    protected $postType;

    use CategoryHelper;

    /**
     * ProductCategoryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postType = config('constants.POST_TYPE.PRODUCT');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $categories = Category::where('post_type', $this->postType)
            ->with('langPost')
            ->orderBy('updated_at', 'DESC');
        if ($request->has('lang')) {
            $posts = $categories->where('lang', $request->lang);
        }
        $categories = $categories->paginate(config('constants.PER_PAGE'));
        $data = [
            'categories' => $categories,
            'title' => 'Trang Quản Lý Danh Mục Sản Phẩm',
        ];
        return view('admin.product_category.list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $category = Category::find($itemId);
        if (isset($category)) {
            $categoryLang = Category::where('parrent_post_id', $itemId)->first();
            if (!empty($categoryLang)) {
                $this->deleteCategory($categoryLang);
            }
            $this->deleteCategory($category);
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công danh mục.');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $category = Category::find($itemId);
                    if (isset($category)) {
                        $categoryLang = Category::where('parrent_post_id', $itemId)->first();
                        if (!empty($categoryLang)) {
                            $this->deleteCategory($categoryLang);
                        }
                        $this->deleteCategory($category);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công danh mục.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * Function help to add new category
     *
     */
    public function add(Request $request)
    {
        $lang = $request->lang;
        $parentId = $request->parrent_id;
        $parentPost = Category::find($parentId);
        $data['title'] = 'Trang Thêm Mới Danh Mục Sản Phẩm';
        $data['initValue'] = $this->getCategoryInputData($this->postType, null, $parentPost);
        $data['parentCategories'] = $this->getCategoryOption($this->postType, null, $lang);
        $data['initValue']['lang'] = $lang;
        $data['initValue']['parrent_post_id'] = $parentId;
        return view('admin.product_category.add', $data);
    }

    /**
     * Function help to handle add new post
     *
     */
    public function handleAdd(HandleCategory $request)
    {
        $postId = $this->saveCategory($this->postType, $request);
        return redirect(route('product_category_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới danh mục.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật Thông Tin Danh Mục';
        $data['initValue'] = $this->getCategoryInputData($this->postType, $category, null);
        $data['category'] = $category;
        $data['parentCategories'] = $this->getCategoryOption($this->postType);
        return view('admin.product_category.edit', $data);
    }

    /**
     * @param HandleCategory $request
     * @param $id
     * @return mixed
     */
    public function handleEdit(HandleCategory $request, $id)
    {
        $category = Category::findOrFail($id);
        $categoryId = $this->saveCategory($this->postType, $request, $category);
        return redirect(route('product_category_edit', ['id' => $categoryId]))
            ->withSuccess('Bạn vừa cập nhật thành công danh mục');
    }

    public function order($id, Request $request)
    {
        $order = $request->order;
        $post = Category::findOrFail($id);
        $post->order = $order;
        $dataResponsive = [];
        if ($post->save()) {
            $dataResponsive = [
                'status' => true,
            ];
            return response()->json($dataResponsive);
        }
        $dataResponsive = [
            'status' => false,
        ];
        return response()->json($dataResponsive);

    }
}
