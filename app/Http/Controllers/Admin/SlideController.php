<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\Post as Post;
use Illuminate\Http\Request;
use App\Helpers\PostHelper;
use App\Http\Requests\HandleSlide;
use Illuminate\Support\Facades\DB;

/**
 * Class SlideController
 * @package App\Http\Controllers\Admin
 */
class SlideController extends AdminController
{
    protected $postType;

    protected $deleteMessage;

    public $metadataFields = [
        'button' => '',
        'link' => '',
        'youtube_url' => '',
    ];

    use PostHelper;

    /**
     * SlideController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postType = config('constants.POST_TYPE.SLIDE');
        $this->deleteMessage = 'Bạn vừa thực hiện xóa thành công slide.';
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $posts = Post::where('post_type', $this->postType)
            ->with('langPost')
            ->orderBy('updated_at', 'DESC');
        if ($request->has('lang')) {
            $posts = $posts->where('lang', $request->lang);
        }
        $posts = $posts->paginate(config('constants.PER_PAGE'));
        $data = [
            'posts' => $posts,
            'title' => 'Trang Quản Lý Slide Hình Ảnh',
        ];
        return view('admin.slide.list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        if ($this->canDeletePost($itemId, $this->postType)) {
            $postLang = Post::where('parrent_post_id', $itemId)->first();
            if (!empty($postLang)) {
                $this->handleDelete($postLang->id);
            }
            $this->handleDelete($itemId);
            return redirect()->back()->withSuccess($this->deleteMessage);
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('postID');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (!empty($itemIds) AND $this->canDeletePost($itemIds, $this->postType)) {
                foreach ($itemIds as $itemId) {
                    $postLang = Post::where('parrent_post_id', $itemId)->first();
                    if (!empty($postLang)) {
                        $this->handleDelete($postLang->id);
                    }
                    $this->handleDelete($itemId);
                }
                return redirect()->back()->withSuccess($this->deleteMessage);
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * @param $postId
     */
    public function handleDelete($postId)
    {
        $post = Post::find($postId);
        if (!empty($post->image)) {
            $image = $post->image;
            $this->deleteImage($image);
        }
        if (!empty($this->metadataFields)) {
            DB::table('metadata_' . $this->postType)->where('post_id', $post->id)->delete();
        }

        Post::destroy($post->id);
        return;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $lang = $request->lang;
        $parentId = $request->parrent_id;
        $parentPost = Post::find($parentId);
        $data['title'] = 'Trang Thêm Mới Slide';
        $data['initValue'] = $this->getPostInputData($this->postType, null, $parentPost);
        if (!empty($lang) && !empty($parentPost)) {
            $data['initValue']['lang'] = $lang;
            $data['initValue']['parent_id'] = $parentId;
            $data['initValue']['image'] =  Image::find($parentPost->image_id);
            $data['gallery'] = $this->getGalleryImages($parentId)->toArray();
        }
        return view('admin.slide.add', $data);
    }

    /**
     * @param HandleProduct $request
     * @return mixed
     */
    public function doAdd(HandleSlide $request)
    {
        $postId = $this->savePost($this->postType, $request);
        return redirect(route('slide_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới slide.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật Slide';
        $data['initValue'] = $this->getPostInputData($this->postType, $post);
        $data['post'] = $post;
        return view('admin.slide.edit', $data);
    }

    /**
     * @param HandleSlide $request
     * @param $id
     * @return mixed
     */
    public function doEdit(HandleSlide $request, $id)
    {
        $post = Post::findOrFail($id);
        $postId = $this->savePost($this->postType, $request, $post);
        return redirect(route('slide_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa cập nhật thành công slide');
    }
}
