<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HandleUser;
use App\User;
use App\Helpers\UserHelper;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
class UserController extends AdminController
{
    use UserHelper;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::with('plan')->where('role', config('constants.role.USER'))->orderBy('updated_at', 'DESC')
            ->paginate(config('constants.PER_PAGE'));
        $data = [
            'title' => 'Trang Quản Lý Người Dùng',
            'users' => $users
        ];
        return view('admin.user.list', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myProfile()
    {
        $data['title'] = 'My Profile';
        $data['initValue'] = $this->getInitUserData(Auth::user());
        return view('admin.user.profile', $data);
    }

    public function updateProfile(HandleUser $request)
    {
        $this->saveUserData($request, Auth::user());
        return redirect(route('my_profile'))->withSuccess('Bạn vừa cập nhật thành công tài khoản');
    }
}
