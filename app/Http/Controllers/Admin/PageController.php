<?php

namespace App\Http\Controllers\Admin;

use App\Page as Page;
use Illuminate\Http\Request;
use App\Helpers\PageHelper;
use App\Http\Requests\HandlePage;

/**
 * Class PageController
 * @package App\Http\Controllers\Admin
 */
class PageController extends AdminController
{
    protected $pageType;

    public $seoable = true;

    use PageHelper;

    /**
     * PageController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->pageType = config('constants.PAGE_TYPE.STATIC');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $pages = Page::where('page_type', $this->pageType)
            ->with('langPost')
            ->orderBy('updated_at');
        if ($request->has('lang')) {
            $pages = $pages->where('lang', $request->lang);
        }
        $pages = $pages->paginate(config('constants.PER_PAGE'));
        $data = [
            'pages' => $pages,
            'title' => 'Trang Quản Lý Trang Tĩnh',
        ];
        return view('admin.page.list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $page = page::find($itemId);
        if (isset($page)) {
            $pageLang = page::where('parrent_post_id', $itemId)->first();
            if (!empty($pageLang)) {
                $this->handleDelete($pageLang);
            }
            $this->handleDelete($page);
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công sản phẩm.');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param $page
     */
    public function handleDelete($page)
    {
        $imagePath = base_path() . DIRECTORY_SEPARATOR . $page->image;
        if (file_exists($imagePath) AND !is_dir($imagePath)) {
            unlink($imagePath);
        }
        $page->delete();
        return;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedpage');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $page = page::find($itemId);
                    if (isset($page)) {
                        $pageLang = page::where('parrent_post_id', $itemId)->first();
                        if (!empty($pageLang)) {
                            $this->handleDelete($pageLang);
                        }
                        $this->handleDelete($page);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công sản phẩm.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $lang = $request->lang;
        $parentId = $request->parrent_id;
        $parentPage = Page::find($parentId);
        $data['title'] = 'Trang Thêm Mới Gói Sản Phẩm';
        $data['initValue'] = $this->getPageInputData($this->pageType, null, $parentPage);
        $data['products'] = page::where('page_type', config('constants.page_TYPE.PRODUCT'))
            ->orderBy('updated_at')->get();
        $data['initValue']['lang'] = $lang;
        $data['initValue']['parent_id'] = $parentId;
        return view('admin.page.add', $data);
    }

    /**
     * @param HandlePage $request
     * @return mixed
     */
    public function doAdd(HandlePage $request)
    {
        $pageId = $this->savepage($this->pageType, $request);
        return redirect(route('page_edit', ['id' => $pageId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới gói sản phẩm.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $page = page::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật Gói Sản Phẩm';
        $data['initValue'] = $this->getPageInputData($this->pageType, $page);
        $data['products'] = page::where('page_type', config('constants.page_TYPE.PRODUCT'))
            ->orderBy('updated_at')->get();
        $data['page'] = $page;
        return view('admin.page.edit', $data);
    }

    /**
     * @param HandlePage $request
     * @param $id
     * @return mixed
     */
    public function doEdit(HandlePage $request, $id)
    {
        $page = page::findOrFail($id);
        $pageId = $this->savePage($this->pageType, $request, $page);
        return redirect(route('page_edit', ['id' => $pageId]))
            ->withSuccess('Bạn vừa cập nhật thành công gói sản phẩm');
    }
}
