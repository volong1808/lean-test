<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post as Post;
use App\Services\ProductService;
use Illuminate\Http\Request;
use App\Helpers\PostHelper;
use App\Http\Requests\HandlePlan;
use Illuminate\Support\Facades\DB;

/**
 * Class PlanController
 * @package App\Http\Controllers\Admin
 */
class PlanController extends AdminController
{
    protected $postType;

    protected $deleteMessage;

    public $metadataFields = [
        'product_id' => '',
        'price' => '0',
        'time' => '1',
        'is_recommend' => '0',
        'position' => null,
    ];

    use PostHelper;

    /**
     * PlanController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postType = config('constants.POST_TYPE.PLAN');
        $this->deleteMessage = 'Bạn vừa thực hiện xóa thành công gói sản phẩm.';
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::where('post_type', config('constants.POST_TYPE.PLAN'))
            ->orderBy('updated_at')->paginate(config('constants.PER_PAGE'));
        $data = [
            'posts' => $posts,
            'title' => 'Trang Quản Lý Gói Sản Phẩm',
        ];
        return view('admin.plan.list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        if ($this->canDeletePost($itemId, $this->postType)) {
            $this->handleDelete($itemId);
            return redirect()->back()->withSuccess($this->deleteMessage);
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param $post
     */
    public function handleDelete($postId)
    {
        $post = Post::find($postId);
        $image = $post->image;
        if (!empty($image)) {
            $this->deleteImage($image);
        }
        if (!empty($this->metadataFields)) {
            DB::table('metadata_' . $this->postType)->where('post_id', $post->id)->delete();
        }

        Post::destroy($post->id);
        return;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $post = Post::find($itemId);
                    if (isset($post)) {
                        $this->handleDelete($post);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công sản phẩm.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $productService = new  ProductService();
        $data['title'] = 'Trang Thêm Mới Gói Sản Phẩm';
        $data['initValue'] = $this->getPostInputData($this->postType);
        $data['products'] = $productService->getAll();
        $data['categories'] = Category::whereNull('parent_id')
            ->where('post_type',$this->postType)->get();
        return view('admin.plan.add', $data);
    }

    /**
     * @param HandlePlan $request
     * @return mixed
     */
    public function doAdd(HandlePlan $request)
    {
        $postId = $this->savePost($this->postType, $request);
        return redirect(route('plan_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới gói sản phẩm.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật Gói Sản Phẩm';
        $data['initValue'] = $this->getPostInputData($this->postType, $post);
        $productService = new  ProductService();
        $data['products'] = $productService->getAll();
        $data['post'] = $post;
        $data['categories'] = Category::whereNull('parent_id')
            ->where('post_type',$this->postType)->get();
        return view('admin.plan.edit', $data);
    }

    /**
     * @param HandlePlan $request
     * @param $id
     * @return mixed
     */
    public function doEdit(HandlePlan $request, $id)
    {
        $post = Post::findOrFail($id);
        $postId = $this->savePost($this->postType, $request, $post);
        return redirect(route('plan_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa cập nhật thành công gói sản phẩm');
    }
}
