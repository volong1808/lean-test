<?php

namespace App\Http\Controllers\Admin;

use App\Category as Category;
use App\CategoryPost;
use App\Helpers\CategoryHelper;
use App\Http\Requests\HandleCategory;
use Illuminate\Http\Request;

/**
 * Class AttributeController
 * @package App\Http\Controllers\Admin
 */
class AttributeController extends AdminController
{
    protected $postType;

    use CategoryHelper;

    /**
     * PostCategoryController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postType = config('constants.POST_TYPE.PLAN');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::where('post_type', $this->postType)
            ->orderBy('updated_at', 'DESC')->paginate(config('constants.PER_PAGE'));
        $data = [
            'categories' => $categories,
            'title' => 'Trang Quản Lý Thuộc Tính Gói Sản Phẩm',
        ];
        return view('admin.attribute.list', $data);
    }

    /**
     * Function help to add new category
     *
     */
    public function add()
    {
        $data['title'] = 'Trang Thêm Mới Thuộc Tính Gói Sản Phẩm';
        $data['initValue'] = $this->getCategoryInputData($this->postType);
        $data['parentCategories'] = $this->getCategoryOption($this->postType);
        return view('admin.attribute.add', $data);
    }

    /**
     * Function help to handle add new post
     *
     */
    public function handleAdd(HandleCategory $request)
    {
        $postId = $this->saveCategory($this->postType, $request);
        return redirect(route('attribute_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới thuộc tính.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật Thông Tin Thuộc Tính';
        $data['initValue'] = $this->getCategoryInputData($this->postType, $category);
        $data['category'] = $category;
        $data['parentCategories'] = $this->getCategoryOption($this->postType);
        return view('admin.attribute.edit', $data);
    }

    /**
     * @param HandleCategory $request
     * @param $id
     * @return mixed
     */
    public function handleEdit(HandleCategory $request, $id)
    {
        $category = Category::findOrFail($id);
        $categoryId = $this->saveCategory($this->postType, $request, $category);
        return redirect(route('attribute_edit', ['id' => $categoryId]))
            ->withSuccess('Bạn vừa cập nhật thành công thuộc tính');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $category = Category::find($itemId);
        if (isset($category)) {
            $this->deleteCategory($category);
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công thuộc tính.');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $category = Category::find($itemId);
                    if (isset($category)) {
                        $this->deleteCategory($category);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công thuộc tính.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }
}
