<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Helpers\PostHelper;
use App\Http\Requests\HandlePost;
use App\Image;
use App\Post as Post;
use Illuminate\Http\Request;

/**
 * Class PostController
 * @package App\Http\Controllers\Admin
 */
class PostController extends AdminController
{
    use PostHelper;

    protected $postType;

    public $seoable = true;

    /**
     * PostController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->postType = config('constants.POST_TYPE.POST');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $posts = Post::where('post_type', $this->postType)
            ->with('langPost')
            ->orderBy('updated_at', 'DESC');
        if ($request->has('lang')) {
            $posts = $posts->where('lang', $request->lang);
        }
        $posts = $posts->paginate(config('constants.PER_PAGE'));
        $data = [
            'posts' => $posts,
            'title' => 'Trang Quản Lý Bài Viết',
        ];
        return view('admin.post.list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $post = Post::find($itemId);
        if (isset($post)) {
            $postLang = Post::where('parrent_post_id', $itemId)->first();
            if (!empty($postLang)) {
                $this->handleDelete($postLang);
            }
            $this->handleDelete($post);
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công bài viết.');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $post = Post::find($itemId);
                    if (isset($post)) {
                        $postLang = Post::where('parrent_post_id', $itemId)->first();
                        if (!empty($postLang)) {
                            $this->handleDelete($postLang);
                        }
                        $this->handleDelete($post);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công bài viết.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * Function help to add new post
     *
     */
    public function add(Request $request)
    {
        $lang = $request->lang;
        $parentId = $request->parrent_id;
        $parentPost = Post::find($parentId);
        $data['title'] = 'Trang Thêm Mới Bài Viết';
        $data['categories'] = Category::whereNull('parent_id')
            ->where('post_type',$this->postType)->get();
        $data['initValue'] = $this->getPostInputData($this->postType, null, $parentPost);
        if (!empty($lang) && !empty($parentPost)) {
            $data['initValue']['lang'] = $lang;
            $data['initValue']['parent_id'] = $parentId;
            $data['initValue']['image'] =  Image::find($parentPost->image_id);
            $data['gallery'] = $this->getGalleryImages($parentId)->toArray();
        }
        return view('admin.post.add', $data);
    }

    /**
     * Function help to handle add new post
     *
     */
    public function handleAdd(HandlePost $request)
    {
        $postId = $this->savePost($this->postType, $request);
        return redirect(route('post_edit', ['id' => $postId]))->withSuccess('Bạn vừa thực hiện thành công thêm mới bài viết.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật Bài Viết';
        $data['categories'] = Category::whereNull('parent_id')->where('post_type', $this->postType)->get();
        $data['initValue'] = $this->getPostInputData($this->postType, $post);
        $data['post'] = $post;
        return view('admin.post.edit', $data);
    }

    /**
     * @param HandlePost $request
     * @param $id
     * @return mixed
     */
    public function handleEdit(HandlePost $request, $id)
    {
        $post = Post::findOrFail($id);
        $postId = $this->savePost($this->postType, $request, $post);
        return redirect(route('post_edit', ['id' => $postId]))->withSuccess('Bạn vừa cập nhật thành công bài viết');
    }
}
