<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryPost;
use App\Gallery;
use App\Http\Requests\HandleProduct;
use App\Image;
use App\Post as Post;
use Illuminate\Http\Request;
use App\Helpers\PostHelper;
use Illuminate\Support\Facades\DB;

/**
 * Class ProductController
 * @package App\Http\Controllers\Admin
 */
class ProductController extends AdminController
{
    protected $postType;

    public $metadataFields = [
        'change_log' => null,
        'model' => '',
        'producer' => '',
        'price' => null,
        'url' => '',
        'discount_price' => null,
        'field_id' => null,
        'properties_description' => null,
        'properties' => null
    ];

    public $seoable = true;

    use PostHelper;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->postType = config('constants.POST_TYPE.PRODUCT');
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $posts = Post::where('post_type', $this->postType)
            ->with('langPost')
            ->orderBy('updated_at', 'desc');

        if ($request->has('lang')) {
            $posts = $posts->where('lang', $request->lang);
        }
        if ($request->has('k')) {
            $posts = $posts->where('name', 'like', '%' . $request->k . '%');
        }
        $posts = $posts->paginate(config('constants.PER_PAGE'));
        $data = [
            'k' => $request->k,
            'lang' => $request->lang,
            'posts' => $posts,
            'title' => 'Trang Quản Lý Sản Phẩm',
        ];
        return view('admin.product.list', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        $itemId = $request->input('itemId');
        $post = Post::find($itemId);
        if (isset($post)) {
            $postLang = Post::where('parrent_post_id', $itemId)->first();
            if (!empty($postLang)) {
                $this->handleDelete($postLang);
            }
            $this->handleDelete($post);
            return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công sản phẩm.');
        }
        return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
    }

    /**
     * @param $post
     */
    public function handleDelete($post)
    {
        if (!empty($post->image)) {{
            $this->deleteImage($post->image);
        }}
        $gallery = $this->getGalleryImages($post->id);
        if (!empty($gallery)) {
            foreach ($gallery as $item) {
                $image = Image::find($item->id);
                if (isset($image)) {
                    $this->deleteImage($image);
                    Gallery::where('post_id', $post->id)->where('image_id', $item->id)->delete();
                }
            }
        }
        if (!empty($this->metadataFields)) {
            DB::table('metadata_' . $this->postType)->where('post_id', $post->id)->delete();
        }

        $post->delete();
        return;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkAction(Request $request)
    {
        $itemIds = $request->input('checkedPost');
        $action = $request->input('action');
        if ($action == config('constants.BULK_ACTION.delete')) {
            if (! empty($itemIds)) {
                foreach ($itemIds as $itemId) {
                    $post = Post::find($itemId);
                    if (isset($post)) {
                        $postLang = Post::where('parrent_post_id', $itemId)->first();
                        if (!empty($postLang)) {
                            $this->handleDelete($postLang);
                        }
                        $this->handleDelete($post);
                    }
                }
                return redirect()->back()->withSuccess('Bạn vừa thực hiện xóa thành công sản phẩm.');
            }
            return redirect()->back()->withErrors([config('constants.message.errorMessage')]);
        }
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $lang = $request->lang;
        $parentId = $request->parrent_id;
        $parentPost = Post::find($parentId);
        $data['title'] = 'Trang Thêm Mới Sản Phẩm';
        $data['categories'] = Category::whereNull('parent_id')
            ->where('post_type', $this->postType)->get();
        $data['initValue'] = $this->getPostInputData($this->postType, null, $parentPost);
        $data['partner'] = Post::where('post_type', config('constants.POST_TYPE.PARTNER'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))->get();
        $data['fields'] = Post::where('post_type', config('constants.POST_TYPE.FIELD'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))->get();
        if (!empty($lang) && !empty($parentPost)) {
            $data['initValue']['lang'] = $lang;
            $data['initValue']['parent_id'] = $parentId;
            $data['initValue']['image'] =  Image::find($parentPost->image_id);
            $data['gallery'] = $this->getGalleryImages($parentId)->toArray();
        }
        return view('admin.product.add', $data);
    }

    /**
     * @param HandleProduct $request
     * @return mixed
     */
    public function doAdd(HandleProduct $request)
    {
        $postId = $this->savePost('product', $request);
        if (isset($request->imageList)) {
            foreach ($request->imageList as $item) {
                $gallery = Gallery::where('post_id', 0)->where('image_id', $item)->first();
                if (isset($gallery)) {
                    $gallery->post_id = $postId;
                    $gallery->save();
                }
            }
        } elseif (!empty($request->parrent_id)) {
            $galleries = $this->getGalleryImages($request->parrent_id)->toArray();
            if (!empty($galleries)) {
                foreach ($galleries as $item) {
                    $gallery = new Gallery();
                    $gallery->post_id = $postId;
                    $gallery->image_id = $item->id;
                    $gallery->save();
                }
            }

        }
        return redirect(route('product_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa thực hiện thành công thêm mới sản phẩm.');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $data['title'] = 'Trang Cập Nhật Sản Phẩm';
        $data['categories'] = Category::whereNull('parent_id')
            ->where('post_type', $this->postType)->get();
        $data['initValue'] = $this->getPostInputData($this->postType, $post);
        $data['gallery'] = $this->getGalleryImages($id)->toArray();
        $data['post'] = $post;
        $data['partner'] = Post::where('post_type', config('constants.POST_TYPE.PARTNER'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))->get();
        $data['fields'] = Post::where('post_type', config('constants.POST_TYPE.FIELD'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))->get();
        return view('admin.product.edit', $data);
    }

    /**
     * @param HandleProduct $request
     * @param $id
     * @return mixed
     */
    public function doEdit(HandleProduct $request, $id)
    {
        $post = Post::findOrFail($id);
        $postId = $this->savePost($this->postType, $request, $post);
        return redirect(route('product_edit', ['id' => $postId]))
            ->withSuccess('Bạn vừa cập nhật thành công sản phẩm');
    }

    public function order($id, Request $request)
    {
        $order = $request->order;
        $post = Post::findOrFail($id);
        $post->order = $order;
        $dataResponsive = [];
        if ($post->save()) {
            $dataResponsive = [
                'status' => true,
            ];
            return response()->json($dataResponsive);
        }
        $dataResponsive = [
            'status' => false,
        ];
        return response()->json($dataResponsive);

    }
}
