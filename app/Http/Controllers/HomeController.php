<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\SeoHelper;
use App\MetaDataVideo;
use App\Page;
use App\Services\PageService;
use App\Services\PostService;
use Illuminate\Http\Request;
use App\MetaDataPlan;
use App\MetaDataProduct;
use App\Post;
use Illuminate\Support\Facades\Session;
use App;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    use SeoHelper;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lang = App::getLocale();
        $postService = new PostService();
        $page = new PageService();
        $feedbackType = config('constants.POST_TYPE.FEEDBACK');
        $topNews = Post::where('post_type', config('constants.POST_TYPE.POST'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('lang', $lang)
            ->with('image')
            ->orderBy('id', 'DESC')
            ->skip(0)->take(4)
            ->get();

        $topService = Post::where('post_type', config('constants.POST_TYPE.SERVICE'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->orderBy('id', 'DESC')
            ->where('lang', $lang)
            ->skip(0)->take(6)
            ->get();
        $topField = Post::where('post_type', config('constants.POST_TYPE.FIELD'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->orderBy('id', 'DESC')
            ->where('lang', $lang)
            ->skip(0)->take(6)
            ->get();

        $categories = Category::where('parent_id', null)
            ->where('post_type', config('constants.POST_TYPE.PRODUCT'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->skip(0)->take(5)
            ->get();

        $topProduct = Post::select('posts.*')
            ->where('posts.post_type', config('constants.POST_TYPE.PRODUCT'))
            ->with('image')
            ->where('lang', $lang)
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }])
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->skip(0)->take(8)
            ->get();
        $feedback = $postService->getByType($feedbackType);
        $about = $page->getBySlug(trans('message.about_slug'));
        $customer = $postService->getByType(config('constants.POST_TYPE.CUSTOMER'));
        $branchs = $postService->getByTypeLimit(config('constants.POST_TYPE.PARTNER'), 0, 5);
        $pageDetail = $page->getBySlug(trans('message.home_slug'));
        $seoData = null;
        if (!empty($pageDetail)) {
            $seoData = $this->getPageSeoData($pageDetail->id);
        }

        $premierBrands = $page->getBySlug(trans('message.premier_brands_slug'));
        $data['topProduct'] = $topProduct;
        $data['topNews'] = $topNews;
        $data['feedback'] = $feedback;
        $data['about'] = $about;
        $data['feedback'] = $feedback;
        $data['customer'] = $customer;
        $data['categories'] = $categories;
        $data['topService'] = $topService;
        $data['seoData'] = $seoData;
        $data['page'] = $pageDetail;
        $data['topBranch'] = $branchs;
        $data['premierBrands'] = $premierBrands;
        $data['topField'] = $topField;
        return view('home', $data);
    }

    public function changeLang($lang)
    {
        $langConfig = config('constants.LANG.' . $lang);
        if (empty($langConfig) || $lang === config('constants.LANGUAGE_DEFAULT')) {
            $lang = config('constants.LANGUAGE_DEFAULT');
            $homeSlug = 'home';
        } else {
            $homeSlug = 'home_' . $lang;
        }
        App::setLocale($lang);
        Session::put('lang', $lang);
        return redirect()->route($homeSlug);
    }
}
