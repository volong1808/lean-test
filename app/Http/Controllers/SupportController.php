<?php

namespace App\Http\Controllers;

use App\Services\VideoService;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public function index($slug = null)
    {
        $banner = [
            'title' => 'Hướng Dẫn',
            'image' => '/images/banner_default.jpg',
        ];
        $data = [
            'banner' => $banner,
        ];
        $videoService = new VideoService();
        if (empty($slug)) {
            $currentVideo = $videoService->getNewVideo();
            $slug = $currentVideo->slug;
        } else {
            $currentVideo = $videoService->getBySlug($slug);
        }
        if (empty($currentVideo)) {
            return view('guides.index', $data);
        }
        $videos = $videoService->getOther($slug);
        $data['currentVideo'] = $currentVideo;
        $data['posts'] = $videos;
        return view('guides.index', $data);
    }
}
