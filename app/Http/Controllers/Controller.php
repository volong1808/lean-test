<?php

namespace App\Http\Controllers;

use App\Services\PageService;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function checkPage($segment = 1)
    {
        $slugPage = \Illuminate\Support\Facades\Request::segment($segment);
        $pageService = new  PageService();
        $page = $pageService->getBySlug($slugPage);
        if (empty($page)) {
            abort(404);
        }
        return $page;
    }
}
