<?php

namespace App\Http\Controllers;

use App\Mail\OrderMail;
use App\Mail\InvoiceMail;
use App\Services\BankingService;
use App\Services\OrderService;
use App\Services\ProductService;
use Illuminate\Support\Facades\Mail;
use App\Services\PlanService;
use App\Services\OptionService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\HandleCheckout;
use Illuminate\Http\Request;
use App\Services\FbaccService;

class CheckoutController extends Controller
{

    public function doCheckout(HandleCheckout $request)
    {
        $productService = new ProductService();
        $orderService = new OrderService();
        $dataOrder = $request->except('_token');

        $cart = $productService->getCart();
        $dataOrder['status'] = config('constants.ORDER_STATUS_DEFAULT');
        $dataOrder['code'] = $orderService->getCode();
        $dataOrder['total'] = getTotalPriceCart($cart);
        $optionService = new OptionService();
        $bankingOptionKey = config('constants.banking_config.key');
        $bankingConfig = $optionService->getConfigOptionByKey($bankingOptionKey);
        $order = $orderService->add($dataOrder);
        $orderService->addCartItem($cart, $order->id);
        $data = [
            'cart' => $cart,
            'bankingConfig' => $bankingConfig,
            'order' => $order,
        ];
        $companyKeyOption = config('constants.company_config.key');
        $emailTo[] = $optionService->getItemByKey($companyKeyOption, 'email');
        if (!empty($dataOrder['email'])) {
            $emailTo[] = $dataOrder['email'];
        }
        $mailOrder = new OrderMail($data);
        Mail::to($emailTo)->send($mailOrder);
        if ($order->payment_type == config('constants.PAYMENT_ONLINE_KEY') && $order->total > config('constants.MIN_PAYMENT')) {
            $bankingService  = new BankingService();
            $urlReturn = route('banking_online_return', $dataOrder['code']);
            $urlVnp = $bankingService->getUrlBanking($order, $urlReturn);
            return redirect($urlVnp);
        }
        return redirect()->route('banking_online', $order->code)->with('checkoutSuccess', 'Mua gói sự dụng thành công - Mã Đơn Hàng: [' . $dataOrder['code'] . ']');
    }

    public function bankingOnline($orderCode)
    {
        $orderService = new OrderService();
        $productService = new ProductService();
        $option = new OptionService();

        $page = $this->checkPage();
        $orderDetail = $orderService->getByCode($orderCode);
        if (empty($orderDetail)) {
            abort(404);
        }

        $cart = $productService->getCart();

        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $banner = [
            'title' => 'Giỏ Hàng',
            'description' => 'Công cụ atuo login facebook',
            'image' => $bannerImg,
        ];

        $productService->removeCart();
        $paymentTypeContent = $option->getContentOptionByKey('banking');
        $paymentType = config('option.banking');

        $data = [
            'banner' => $banner,
            'name' => 'Đặt Hàng Thành Công',
            'title' => 'Đặt Hàng Thành Công',
            'cart' => $cart,
            'orderDetail' => $orderDetail,
            'page' => $page,
            'paymentType' => $paymentType,
            'paymentTypeContent' => $paymentTypeContent
        ];
        return view('product.result_oder', $data);
    }

    public function doBankingOnline($orderCode)
    {
        $orderService = new OrderService();
        $orderDetail = $orderService->getByCode($orderCode);
        if (empty($orderDetail)) {
            abort(404);
        }
        $bankingService  = new BankingService();
        $urlReturn = route('banking_online_return', $orderCode);
        $urlVnp = $bankingService->getUrlBanking($orderDetail, $urlReturn);
        return redirect($urlVnp);
    }

    public function paymentReturn($orderCode, Request $request)
    {
        $orderService = new OrderService();
        $bankingService  = new BankingService();
        $planService = new PlanService();
        $productService = new ProductService();
        $optionService = new OptionService();

        $orderDetail = $orderService->getByCode($orderCode);
        if (empty($orderDetail)) {
            return redirect()->route('banking_online', $orderCode)
                ->with('paymentSuccess', 'Đơn hàng không tồn tại vui lòng kiểm tra lại.');
        }

        if ($orderDetail->transaction_no == $request->vnp_TransactionNo) {
            return redirect()->route('banking_online', $orderCode)
                ->with('paymentError', 'Đơn hàng đã thanh toán.');
        }

        $stateReturn = $bankingService->getStageReturn($orderDetail, $request->all());
        if ($stateReturn['RspCode'] == config('constants.PAYMENT_SUCCESS_CODE')) {

            $dataUpdate = [
                'status' => $stateReturn['status'],
                'transaction_no' => $request->vnp_TransactionNo,

            ];
            $orderService->updateStatusPaidByCode($orderCode, $dataUpdate);
            $bankingOptionKey = config('constants.banking_config.key');
            $bankingConfig = $optionService->getConfigOptionByKey($bankingOptionKey);
            $cart = $productService->getCart();
            $data = [
                'cart' => $cart,
                'bankingConfig' => $bankingConfig,
                'order' => $orderDetail,
            ];
            $companyKeyOption = config('constants.company_config.key');
            $emailTo[] = $optionService->getItemByKey($companyKeyOption, 'email');
            if (!empty($orderDetail['email'])) {
                $emailTo[] = $orderDetail['email'];
            }
            $mailVoid = new InvoiceMail($data);
            Mail::to($emailTo)->send($mailVoid);
            return redirect()->route('banking_online', $orderCode)
                ->with('paymentSuccess', 'Bạn đã thành toán thành công.');
        }
        return redirect()->route('banking_online', $orderCode)
            ->with('paymentError', 'Thanh toán không thành công! Vui lòng thử lại.');
    }
}
