<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Helpers\SeoHelper;
use App\Mail\ContactMail;
use App\Mail\OrderMail;
use App\Mail\SubscribeMail;
use App\Page;
use App\Services\OptionService;
use App\Services\OrderService;
use App\Services\PageService;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class ContactController extends Controller
{
    use SeoHelper;

    public function index(Request $request)
    {
        $pageService = new PageService();
        Session::forget('errors');
        $slug = $request->segment(2);
        $page = $pageService->getBySlug($slug);
        if (empty($page)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $banner = [
            'title' => $page->name,
            'description' => $page->description,
            'image' => $bannerImg,
        ];
        $seoData = $this->getPageSeoData(Page::where('slug', 'lien-he')->first()->id);
        $data = [
            'page' => $page,
            'name' => $page->name,
            'banner' => $banner,
            'seoData' => $seoData,
        ];
        return view('contact', $data);
    }

    public function sentContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'address' => 'required|max:500',
            'phone_number' => 'required|regex:/^[0-9]{10,11}$/',
            'message' => 'required|max:500',
        ], [
            'required' => ':attribute ' . trans('message.required'),
            'max' => ':attribute ' . trans('message.max') . ' :max ' . trans('message.character'),
            'regex' => ':attribute '. trans('message.invalid_format'),
        ], [
            'name' => trans('message.full_name'),
            'email' => 'Email',
            'address' => trans('message.address'),
            'phone_number' => trans('message.phone'),
            'message' => trans('message.content')
        ]);


        if ($validator->passes()) {
            $lang = Session::has('lang') ? Session::get('lang') : config('constants.LANGUAGE_DEFAULT');

            $data = $request->all();
            $contact = new Contact();
            $contact->name = $data['name'];
            $contact->message = $data['message'];
            $contact->email = $data['email'];
            $contact->address = $data['address'];
            $contact->phone_number = $data['phone_number'];
            $contact->status = config('constants.CONTACT_STATUS.NEW');
            $contact->created_at = date('Y-m-d H:i:s', time());
            $contact->save();
            $emailContact = new ContactMail($data);
            $option = new OptionService();
            $companyKeyOption = config('constants.COMPANY_CONFIG_KEY.'. $lang);
            $emailTo[] = $option->getItemByKey($companyKeyOption, 'email');
            Mail::to($emailTo)->send($emailContact);
            return response()->json(['redirect' => true, 'success' => trans('message.contact_send_success')], 200);
        } else {
            return response()->json($validator->errors()->messages(), 422);
        }

    }

    public function sendOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'address' => 'required|max:500',
            'phone_number' => 'required|regex:/^[0-9]{10,11}$/',
            'message' => 'nullable|max:500',
        ], [
            'required' => ':attribute ' . trans('message.required'),
            'max' => ':attribute ' . trans('message.max') . ' :max ' . trans('message.character'),
            'regex' => ':attribute '. trans('message.invalid_format'),
        ], [
            'name' => trans('message.full_name'),
            'email' => 'Email',
            'address' => trans('message.address'),
            'phone_number' => trans('message.phone'),
            'message' => trans('message.content')
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $lang = Session::has('lang') ? Session::get('lang') : config('constants.LANGUAGE_DEFAULT');
            $orderService = new OrderService();
            $order = $orderService->add($data);
            $orderService->addCartItem($data, $order->id);

            $optionService = new OptionService();
            $companyKeyOption = config('constants.COMPANY_CONFIG_KEY.'. $lang);
            $emailTo[] = $optionService->getItemByKey($companyKeyOption, 'email');
            $emailTo[] = $data['email'];
            $mailOrder = new OrderMail($data);
            Mail::to($emailTo)->send($mailOrder);

            return response()->json(['redirect' => true, 'success' => trans('message.order_send_success')], 200);
        } else {
            return response()->json($validator->errors()->messages(), 422);
        }

    }

    public function sendSubscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email'
        ], [
            'required' => ':attribute ' . trans('message.required'),
            'max' => ':attribute ' . trans('message.max') . ' :max ' . trans('message.character')
        ], [
            'name' => trans('message.full_name'),
            'email' => 'Email'
        ]);

        if ($validator->passes()) {
            $data = $request->all();
            $lang = Session::has('lang') ? Session::get('lang') : config('constants.LANGUAGE_DEFAULT');
            $emailContact = new SubscribeMail($data);
            $option = new OptionService();
            $companyKeyOption = config('constants.COMPANY_CONFIG_KEY.'. $lang);
            $emailTo[] = $option->getItemByKey($companyKeyOption, 'email');
            $emailTo[] = $data['email'];
            Mail::to($emailTo)->send($emailContact);
            return response()->json(['redirect' => true, 'success' => trans('message.subscribe_send_success')], 200);
        } else {
            return response()->json($validator->errors()->messages(), 422);
        }

    }
}
