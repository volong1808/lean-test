<?php

namespace App\Http\Controllers;

use App;
use App\Helpers\SeoHelper;
use App\MetaDataPlan;
use App\Page;
use App\Post;
use App\SeoData;
use App\Services\OptionService;
use App\Services\PageService;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Services\CategoryService;

class ProductController extends Controller
{
    public $product;

    use SeoHelper;

    public function __construct()
    {
        $this->product = new ProductService();
    }

    public function index(Request $request)
    {
        $lang = App::getLocale();
        $slugPage = \Illuminate\Support\Facades\Request::segment(2);
        $pageService = new  PageService();
        $page = $pageService->getBySlug($slugPage);
        if (empty($page)) {
            abort(404);
        }

        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $banner = [
            'title' => $page->name,
            'description' => $page->description,
            'image' => $bannerImg,
        ];

        $products = Post::where('posts.post_type', config('constants.POST_TYPE.PRODUCT'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }]);
        if ($request->has('lang')) {
            $products = $products->where('lang', $request->lang);
        }
        $products = $products->paginate($itemInPage);

        $seoData = $this->getPageSeoData(Page::where('slug', $page->slug)->first()->id);

        $data = [
            'banner' => $banner,
            'name' => $page->name,
            'title' => $page->name,
            'products' => $products,
            'seoData' => $seoData,
        ];
        return view('product.list', $data);
    }

    public function category()
    {
        $lang = App::getLocale();
        $page = $this->checkPage(2);
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $categoryService = new CategoryService();
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $banner = [
            'title' => $page->name,
            'image' => $bannerImg,
        ];
        $categorise = $categoryService->getParent(config('constants.POST_TYPE.PRODUCT'), $lang);
        $seoData = $this->getPageSeoData(Page::where('slug', $page->slug)->first()->id);
        $data = [
            'banner' => $banner,
            'name' => $page->name,
            'title' => $page->name,
            'categories' => $categorise,
            'seoData' => $seoData,
        ];
        return view('product.category', $data);
    }

    public function categoryItem($slug)
    {
        $lang = App::getLocale();
        $page = $this->checkPage(2);
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $categoryService = new CategoryService();
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $banner = [
            'image' => $bannerImg,
        ];
        $category = $categoryService->getBySlug($slug);
        if (empty($category)) {
            abort(404);
        }
        $banner['title'] = $category->name;
        $seoData = $this->getPageSeoData(Page::where('slug', $page->slug)->first()->id);
        $subCategories = $category->children;
        if ($subCategories->count() > 0) {
            $data = [
                'categoryDetail' => $category,
                'banner' => $banner,
                'name' => $category->name,
                'title' => $category->name,
                'categories' => $subCategories,
                'seoData' => $seoData,
            ];
            return view('product.category', $data);
        }

        $products = $this->product->getByCateId($category->id);
        $data = [
            'banner' => $banner,
            'name' => $category->name,
            'title' => $category->name,
            'products' => $products,
            'seoData' => $seoData,
            'category' => $category
        ];
        return view('product.list', $data);

    }

    public function search(Request $request)
    {
        $lang = App::getLocale();
        $keyword = $request->s;
        $bannerImg = '/images/banner_default.jpg';
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $banner = [
            'title' => trans('message.search'),
            'description' => trans('message.search'),
            'image' => $bannerImg,
        ];
        $products = $this->product->search($keyword, $lang, $itemInPage);
        $seoData = $this->getPageSeoData(Page::where('slug', trans('message.product_slug'))->first()->id);
        $data = [
            'banner' => $banner,
            'name' =>  trans('message.search'),
            'title' =>  trans('message.search'),
            'keyword' => $keyword,
            'products' => $products,
            'seoData' => $seoData,
        ];
        return view('product.search', $data);
    }

    public function detail($slug, $id)
    {
        $lang = App::getLocale();
        $slugPage = \Illuminate\Support\Facades\Request::segment(2);
        $pageService = new  PageService();
        $page = $pageService->getBySlug($slugPage);
        if (empty($page)) {
            abort(404);
        }

        $productDetail = $this->product->getById($id);

        if (empty($productDetail)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $seoData = $this->getPostSeoData($productDetail->id);
        $banner = [
            'title' => $productDetail->name,
            'description' => 'Công cụ atuo login facebook',
            'image' => $bannerImg,
        ];
        $productOther = $this->product->getOther($productDetail->id, $lang);
        $data = [
            'banner' => $banner,
            'name' => $productDetail->name,
            'page' => $page->name,
            'productDetail' => $productDetail,
            'seoData' => $seoData,
            'products' => $productOther
        ];
        return view('product.detail', $data);
    }

    public function listCategory($slug_cate)
    {
        $page = $this->checkPage();
        $categoryDetail = $this->product->getCategoryDetailBySlug($slug_cate);
        if (empty($categoryDetail)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $banner = [
            'title' => $categoryDetail->name,
            'description' => 'Công cụ atuo login facebook',
            'image' => $bannerImg,
        ];
        $categoryParent = [];
        $this->product->getParentCategory($categoryDetail, $categoryParent);
        $products = $this->product->getByCateId($categoryDetail->id);

        $seoData = $this->getPageSeoData(Page::where('slug', $page->slug)->first()->id);

        $data = [
            'banner' => $banner,
            'name' => $categoryDetail->name,
            'title' => $categoryDetail->name,
            'products' => $products,
            'seoData' => $seoData,
            'categoryParent' => $categoryParent
        ];
        return view('product.list', $data);
    }

    public function listByProducer($slug, $id)
    {
        $lang = App::getLocale();
        $page = $this->checkPage();
        $producerDetail = Post::where('post_type', config('constants.POST_TYPE.PARTNER'))
            ->where('lang', $lang)
            ->where('id', $id)->where('state', config('constants.POST_STATE.PUBLISHED'))->first();
        if (empty($producerDetail)) {
            return abort(404);
        }

        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $banner = [
            'title' => trans('message.producer') . ' ' . $producerDetail->name,
            'description' => 'Công cụ atuo login facebook',
            'image' => $bannerImg,
        ];
        $itemInPage = config('constants.
        ');
        $seoData = $this->getPageSeoData(Page::where('slug', $page->slug)->first()->id);
        $products = $this->product->getByProducer($producerDetail->id, $itemInPage);
        $data = [
            'banner' => $banner,
            'name' => 'Hãng Sản Xuất ' . $producerDetail->name,
            'title' => 'Hãng Sản Xuất ' . $producerDetail->name,
            'products' => $products,
            'seoData' => $seoData,
        ];
        return view('product.list', $data);

    }

    public function order($slug)
    {
        $lang = App::getLocale();
        $prefixLang = '';
        if ($lang == 'en') {
            $prefixLang = '_en';
        }
        $this->checkPage();
        $productDetail = $this->product->getDetail($slug);
        if (empty($productDetail)) {
            abort(404);
        }

        $this->product->addToCart($productDetail, 1);
        return redirect()->route('product_cart' . $prefixLang);
    }

    public function cartDetail()
    {
        $page = $this->checkPage();
        $cart = $this->product->getCart();

        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $this->checkPage();
        $banner = [
            'title' => trans('message.cart'),
            'description' => 'Công cụ atuo login facebook',
            'image' => $bannerImg,
        ];

        $option = new OptionService();
        $paymentTypeContent = $option->getContentOptionByKey('banking');
        $paymentType = config('option.banking');

        $data = [
            'banner' => $banner,
            'name' =>  trans('message.cart'),
            'title' =>  trans('message.cart'),
            'cart' => $cart,
            'page' => $page,
            'paymentType' => $paymentType,
            'paymentTypeContent' => $paymentTypeContent
        ];
        return view('product.cart', $data);
    }

    public function download($slug)
    {
        $productDetail = $this->product->getDetail($slug);
        if (empty($productDetail)) {
            abort(404);
        }

        $downloadDir = config('constants.TOOL_DOWNLOAD_DIR');
        $downloadLink = base_path("{$downloadDir}/{$productDetail->url}");
        return Response::download($downloadLink);
    }


    public function cartDeleteItem($id)
    {
        $page = $this->checkPage();
        $productDetail = $this->product->getById($id);
        if (empty($productDetail)) {
            abort(404);
        }
        $this->product->deleteItemCart($productDetail->id);
        return redirect()->route('product_cart');
    }

    public function cartChangeAmount(Request $request,$id)
    {
        $response = [];
        $amount = $request->amount;
        if (!is_numeric($amount) && $amount < 1) {
            $response = [
                'status' => 400,
                'message' => 'Vui Lòng Nhập Số Nguyên và lớn hơn 1!',
            ];
            return response()->json($response);
        }
        $this->product->updateCartAmount($id, $amount);
        $cart = $this->product->getCart();
        $cartBody = view('product.includes.body_cart_list', ['cart' => $cart])->render();;
        $response = [
            'status' => 200,
            'tableCart' => $cartBody,

        ];
        return response()->json($response);

    }
}
