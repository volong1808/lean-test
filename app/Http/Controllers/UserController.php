<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Http\Requests\LoginUser;
use App\Mail\ResetPassworUserMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Http\Requests\RegisterUser;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ResetPasswordUser;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public function register()
    {
        return view('user.register');
    }

    public function doRegister(RegisterUser $request)
    {
        $dataUser = $request->all();
        $this->userService->add($dataUser);
        return redirect()->route('front_en_login')
            ->with('successRegister', 'Tạo tài khoản thành công vui lòng đăng nhập');
    }

    public function login()
    {
        $previousUrl =  URL::previous();
        if ($previousUrl != URL::current()) {
            Session::put('previousUrlLogin', $previousUrl);
        }
        return view('user.login');
    }

    public function doLogin(LoginUser $request)
    {
        $roleUser = config('constants.role.USER');
        $status = config('constants.USER_STATUS.ACTIVE');
        $data = [
            'username' => $request->username,
            'password' => $request->password,
            'status' => $status,
            'role' => $roleUser
        ];
        $loginStage = Auth::attempt($data, $request->remember);
        if ($loginStage) {
            $previousUrl = Session::get('previousUrlLogin');
            return redirect($previousUrl);
        }
        return redirect()->route('front_en_login')
            ->with('errorLogin', 'Đăng nhập không thành công vui lòng kiểm tra lại.');
    }

    public function reset()
    {
        return view('user.reset');
    }

    public function doReset(ResetPasswordUser $request)
    {
        $newPassword = generateRandomString();
        $user = $this->userService->updatePassword($request->username, $newPassword);
        if ($user == false) {
            redirect()->route('front_en_login')->with('errorLogin', 'Đổi mật khẩu không thành công.');
        }
        $user = $this->userService->getByUserName($request->username);
        $data = [
            'email' => $user->email,
            'name' => $user->name,
            'newPassword' => $newPassword,
        ];
        $emailReset = new ResetPassworUserMail($data);
        Mail::to($request->email)->send($emailReset);
        return redirect()->route('front_en_login')->with('resetSuccess', 'Khôi phục mật khẩu thành công! vui lòng đăng nhập lại.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
