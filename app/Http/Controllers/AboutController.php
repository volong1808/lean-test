<?php

namespace App\Http\Controllers;

use App\Services\PageService;
use App\Services\VideoService;
use Illuminate\Support\Facades\Request;

class AboutController extends Controller
{
    public function index()
    {
        $pageService = new PageService();
        $slug = Request::segment(2);
        $page = $pageService->getBySlug($slug);
        if (empty($page)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }

        $videoService = new VideoService();
        $aboutList = $videoService->getAll();

        $banner = [
            'title' => $page->name,
            'image' => $bannerImg,
        ];
        $data = [
            'banner' => $banner,
            'page' => $page,
            'name' => $page->name,
            'aboutList' => $aboutList
        ];
        return view('about.list', $data);
    }

    public function detail($slug)
    {
        $pageService = new PageService();
        $videoService = new VideoService();
        $slugPage = Request::segment(2);
        $page = $pageService->getBySlug($slugPage);
        $aboutDetail = $videoService->getBySlug($slug);
        if (empty($page) || empty($aboutDetail)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }

        $aboutList = $videoService->getAll();
        $banner = [
            'title' => $page->name,
            'image' => $bannerImg,
        ];
        $data = [
            'banner' => $banner,
            'page' => $page,
            'name' => $aboutDetail->name,
            'aboutList' => $aboutList,
            'aboutDetail' => $aboutDetail
        ];
        return view('about.about', $data);
    }
}
