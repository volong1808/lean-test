<?php

namespace App\Http\Controllers;

use App\Helpers\SeoHelper;
use App\Page;
use App\Services\PageService;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Route;

class CustomerController extends Controller
{
    use SeoHelper;

    public function index(Request $request)
    {
        $slugPage = \Illuminate\Support\Facades\Request::segment(1);
        $pageService = new  PageService();
        $page = $pageService->getBySlug($slugPage);
        if (empty($page)) {
            abort(404);
        }

        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $banner = [
            'title' => 'Blog',
            'description' => 'Bài viết chia sẻ về cuộc sống công nghệ',
            'image' => $bannerImg,
        ];
        $news = Post::where('post_type', config('constants.POST_TYPE.CUSTOMER'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->orderBy('id', 'DESC')
            ->paginate($itemInPage);
        $seoData = $this->getPageSeoData(Page::where('slug', $page->slug)->first()->id);
        $data = [
            'banner' => $banner,
            'name' => $page->name,
            'title' => $page->name,
            'news' => $news,
            'newPost' => $this->getNewPost(),
            'seoData' => $seoData,
        ];
        return view('job.list', $data);
    }

    public function detail($slug)
    {
        $slugPage = \Illuminate\Support\Facades\Request::segment(1);
        $pageService = new  PageService();
        $page = $pageService->getBySlug($slugPage);
        if (empty($page)) {
            abort(404);
        }
        $newsDetail = Post::where('slug', $slug)->first();
        if (empty($newsDetail)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $seoData = $this->getPostSeoData($newsDetail->id);
        $banner = [
            'title' => $newsDetail->name,
            'image' => $bannerImg,
        ];

        $data = [
            'banner' => $banner,
            'newsDetail' => $newsDetail,
            'newPost' => $this->getNewPost(),
            'seoData' => $seoData,
            'page' => $page->name,
            'name' => $newsDetail->name,
            'title' => $page->name,
        ];
        return view('job.detail', $data);
    }

    protected function getNewPost()
    {
        $newPost = Post::where('post_type', config('constants.POST_TYPE.JOB'))
            ->with('image')
            ->skip(0)->take(config('constants.NEWS_HOME_PAGE_ITEM'))
            ->orderBy('id', 'DESC')
            ->get();
        return $newPost;
    }
}
