<?php

namespace App\Http\Controllers;

use App\Helpers\SeoHelper;
use App\Page;
use App\Post;
use App\Services\PageService;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request;

class FieldController extends Controller
{
    use SeoHelper;

    public function index()
    {
        $slugPage = Request::segment(2);

        $pageService = new  PageService();
        $page = $pageService->getBySlug($slugPage);
        if (empty($page)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        $banner = [
            'title' => $page->name,
            'description' => $page->description,
            'image' => $bannerImg,
        ];
        $news = Post::where('post_type', config('constants.POST_TYPE.FIELD'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->orderBy('id', 'DESC')
            ->paginate($itemInPage);
        $seoData = $this->getPageSeoData(Page::where('slug', 'linh-vuc')->first()->id);
        $data = [
            'image' => $bannerImg,
            'banner' => $banner,
            'name' => $page->name,
            'title' => $page->name,
            'fields' => $news,
            'page' => $page,
            'newPost' => $this->getNewPost(),
            'seoData' => $seoData,
        ];
        return view('field.list', $data);
    }

    public function detail($slug)
    {
        $slugPage = Request::segment(2);
        $pageService = new  PageService();
        $page = $pageService->getBySlug($slugPage);
        if (empty($page)) {
            abort(404);
        }
        $newsDetail = Post::where('slug', $slug)->first();
        if (empty($newsDetail)) {
            abort(404);
        }
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($page->image)) {
            $bannerImg = $page->image->origin;
        }
        $seoData = $this->getPostSeoData($newsDetail->id);
        $banner = [
            'title' => $newsDetail->name,
            'description' => $newsDetail->description,
            'image' => $bannerImg,
        ];

        $data = [
            'banner' => $banner,
            'newsDetail' => $newsDetail,
            'newPost' => $this->getNewPost(),
            'seoData' => $seoData,
            'page' => $page->name,
            'name' => $newsDetail->name,
            'title' => $page->name . ' - ' . $newsDetail->name,
        ];
        return view('field.detail', $data);
    }

    protected function getNewPost()
    {
        $newPost = Post::where('post_type', config('constants.POST_TYPE.POST'))
            ->with('image')
            ->skip(0)->take(config('constants.NEWS_HOME_PAGE_ITEM'))
            ->orderBy('id', 'DESC')
            ->get();
        return $newPost;
    }
}
