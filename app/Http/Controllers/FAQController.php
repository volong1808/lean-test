<?php

namespace App\Http\Controllers;

use App\Helpers\SeoHelper;
use App\Page;
use App\Services\CategoryService;

class FAQController extends Controller
{
    use SeoHelper;

    public function index($categorySlug = null)
    {
        $categoryService = new CategoryService();
        $postType = config('constants.POST_TYPE.FAQ');
        $faqs = $categoryService->getPostInCat($postType);
        $banner = [
            'title' => 'Q&A',
            'image' => '/images/banner_default.jpg',
        ];
        $seoData = $this->getPageSeoData(Page::where('slug', 'qa')->first()->id);
        $data = [
            'banner' => $banner,
            'seoData' => $seoData,
            'faqs' => $faqs,
            'categorySlug' => $categorySlug
        ];
        return view('faq', $data);
    }
}
