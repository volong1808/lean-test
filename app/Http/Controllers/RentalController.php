<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\SeoHelper;
use App\Page;
use App\Services\PageService;
use App\Services\PostService;
use App\Post;
use Illuminate\Support\Facades\Session;
use App;


class RentalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    use SeoHelper;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lang = App::getLocale();
        $page = new PageService();


        $pageDetail = $this->checkPage(2);
        $bannerImg = '/images/banner_default.jpg';
        if (!empty($pageDetail->image)) {
            $bannerImg = $pageDetail->image->origin;
        }
        $banner = [
            'title' => $pageDetail->name,
            'description' => $pageDetail->description,
            'image' => $bannerImg,
        ];

        $seoData = $this->getPageSeoData($pageDetail->id);
        $premierBrands = $page->getBySlug(trans('message.premier_brands_slug'));
        $data['seoData'] = $seoData;
        $data['page'] = $pageDetail;
        $data['banner'] = $banner;
        $data['premierBrands'] = $premierBrands;
        return view('rental', $data);
    }
}
