<?php

/**
 * @param $path
 * @return string
 */
function getImagePath($path)
{
    $imagePath = base_path() . DIRECTORY_SEPARATOR . $path;
    return $imagePath;
}
