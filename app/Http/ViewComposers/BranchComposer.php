<?php
namespace App\Http\ViewComposers;

use App;
use App\Category;
use App\Post;
use App\Services\PageService;
use App\Services\PostService;
use Illuminate\View\View;
use App\Services\OptionService;

class BranchComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $postService = new PostService();
        $page = new PageService();
        $branchs = $postService->getByTypeLimit(config('constants.POST_TYPE.PARTNER'), 0, 5);
        $premierBrands = $page->getBySlug(trans('message.premier_brands_slug'));
        $view->with('topBranch', $branchs)
            ->with('premierBrands', $premierBrands);

    }
}
