<?php
namespace App\Http\ViewComposers;

use App;
use App\Category;
use App\Post;
use App\Services\PostService;
use Illuminate\Support\Facades\Request;
use Illuminate\View\View;
use App\Services\OptionService;

class MasterComposer
{
    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $lang = App::getLocale();
        $prefixLang = '';
        if ($lang == 'en') {
            $prefixLang = '_en';
        }
        $slugPage = Request::segment(2);
        $option = new OptionService();
        $companyKeyOption = config('constants.company_config.key');
        $pluginKeyOption = config('constants.plugin_config.key');
        $company = $option->getContentOptionByKey($companyKeyOption . $prefixLang);
        $plugin = $option->getContentOptionByKey($pluginKeyOption);
        $postService = new PostService();
        $partners = $postService->getByTypeAll(config('constants.POST_TYPE.PARTNER'));
        $aboutList = Post::where('post_type', config('constants.POST_TYPE.VIDEO'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->with('video')
            ->orderBy('id', 'DESC')
            ->get();
        $serviceList = Post::where('post_type', config('constants.POST_TYPE.SERVICE'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->get();
        $jobList = Post::where('post_type', config('constants.POST_TYPE.JOB'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->get();
        $categories = Category::where('parent_id', null)
            ->where('post_type', config('constants.POST_TYPE.PRODUCT'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('order', 'ASC')
            ->get();
        $productMenu = [];
        foreach ($categories as $category) {
            $subCategory = Category::where('post_type', config('constants.POST_TYPE.PRODUCT'))
                ->where('state', config('constants.POST_STATE.PUBLISHED'))
                ->where('parent_id', $category->id)
                ->with('image')
                ->where('lang', $lang)
                ->orderBy('order', 'ASC')
                ->get();
            $category->subCategory = $subCategory;
            $productMenu[] = $category;
        }
        $topProduct = Post::select('posts.*')
            ->where('posts.post_type', config('constants.POST_TYPE.PRODUCT'))
            ->with('image')
            ->where('lang', $lang)
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }])
            ->orderBy('id', 'DESC')
            ->skip(0)->take(4)
            ->get();
        $view->with('company', $company)
            ->with('plugin', $plugin)
            ->with('slugPage' , $slugPage)
            ->with('aboutList', $aboutList)
            ->with('serviceList', $serviceList)
            ->with('partners', $partners)
            ->with('newProducts', $topProduct)
            ->with('productMenu', $productMenu)
            ->with('lang', $lang)
            ->with('prefixLang', $prefixLang)
            ->with('jobList', $jobList);

    }
}
