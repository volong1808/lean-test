<?php

namespace App\Mail;

use App\Services\OptionService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $option = new OptionService();
        $companyKeyOption = config('constants.company_config.key');
        $emailTo = $option->getItemByKey($companyKeyOption, 'email');
        return $this->subject('[Đặt Hàng] Email Đặt Hàng Từ Website Artcaremedical.com')
            ->from($emailTo, 'Artcaremedical.com`')
            ->view('checkout.invoice_email', $this->data);
    }
}
