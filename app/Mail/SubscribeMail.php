<?php

namespace App\Mail;

use App\Services\OptionService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Session;

class SubscribeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $lang = Session::has('lang') ? Session::get('lang') : config('constants.LANGUAGE_DEFAULT');
        $option = new OptionService();
        $companyKeyOption = config('constants.COMPANY_CONFIG_KEY.'. $lang);
        $emailTo = $option->getItemByKey($companyKeyOption, 'email');
        return $this->subject('[Đăng Ký Nhận Tin] Email Đăng Ký Nhận Tin Từ Website ' . url('/'))
            ->from($emailTo, url('/'))
            ->view('subscribe.email')
            ->with('content', $this->data);
    }
}
