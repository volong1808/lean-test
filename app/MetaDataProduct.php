<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaDataProduct extends Model
{
    protected $table = 'metadata_product';
    public $timestamps = false;

    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }

    public function partner()
    {
        return $this->hasOne(Post::class, 'id', 'change_log');
    }

    public function field()
    {
        return $this->hasOne(Post::class, 'id', 'field_id');
    }
}
