<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRaw extends Model
{
    protected $table = 'users_raw';

    public $timestamps = false;
}
