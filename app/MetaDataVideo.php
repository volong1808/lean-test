<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MetaDataVideo
 * @package App
 */
class MetaDataVideo extends Model
{
    protected $table = 'metadata_video';

    public $timestamps = false;

    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }
}
