<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = true;

    public function plan()
    {
        return $this->hasOne(MetaDataPlan::class, 'id', 'plan_id');
    }
}
