<?php

namespace App\Helpers;

use App\Category;
use App\CategoryPost;
use App\Image;


/**
 * Trait CategoryHelper
 * @package App\Helpers
 */
trait CategoryHelper
{
    use PostHelper;
    /**
     * @param string $postType
     * @param Category $category
     *
     * @return array
     */
    public function getCategoryInputData($postType, $category = [], $parentCategory = [])
    {
        $data = [];
        $imageDefault = '';
        if (empty($category) && empty($parentCategory)) {
            $imageDefault = '';
        } elseif (!empty($category) && empty($parentCategory)) {
            $imageDefault = Image::find($category->image_id);
        } else {
            $imageDefault = Image::find($parentCategory->image_id);
        }
        $data['name'] = empty($category) ? '' : $category->name;
        $data['description'] = empty($category) ? '' : $category->description;
        $data['content'] = empty($category) ? '' : $category->content;
        $data['slug'] = empty($category) ? '' : $category->slug;
        $data['state'] = empty($category) ? config('constants.POST_STATE.PUBLISHED') : $category->state;
        $data['image'] = $imageDefault;
        $data['lang'] = empty($category) ? config('constants.LANGUAGE_DEFAULT') : $category->lang;
        $data['parent_id'] =  empty($category) ? '' : $category->parent_id;
        $data['parrent_post_id'] =  empty($category) ? '' : $category->parrent_post_id;
        $data['order'] =  empty($category) ? '' : $category->order;
        return $data;
    }

    /**
     * @param $postType
     * @param Category $category
     * @return array
     */
    public function getCategoryOption($postType, $category = [], $lang = null)
    {
        $option = [0 => 'Danh Mục Cha'];
        $parentCategories = Category::whereNull('parent_id')->where('post_type', $postType);
        if (!empty($lang)) {
            $parentCategories = $parentCategories->where('lang', $lang);
        }
        $parentCategories = $parentCategories->get();
        foreach ($parentCategories as $parentCategory) {
            if (!empty($category) AND $category->id == $parentCategory->id) {
                continue;
            }
            $option[$parentCategory->id] = $parentCategory->name;
        }
        return $option;
    }

    /**
     * Function help to save the category
     *
     * @param $postType
     * @param $request
     * @param array $category
     * @return integer
     */
    public function saveCategory($postType, $request, $category = [])
    {
        $current = date('Y-m-d H:i:s');
        if (empty($category)) {
            $category = new Category();
            $category->created_at = $current;
        }
        if (!empty($request->parrent_post_id)) {
            $pageParent = Category::find($request->parrent_post_id);
            $image = Image::find($pageParent->image_id);
            $category->image_id =  !empty($image) ? $image->id : null;
        }

        if ($request->hasFile('image')) {
            $imageId = $this->savePostImage($postType, $request);
            if (!empty($category->image_id)) {
                $oldImageId = $category->image_id;
                $image = Image::find($oldImageId);
                if (!empty($image)) {
                    deleteFileDatabase($image->url);
                    deleteFileDatabase($image->thumb);
                    deleteFileDatabase($image->origin);
                    $image->delete();
                }
            }
            $category->image_id = $imageId;
        }

        $category->post_type = $postType;
        $category->name = isset($request->name) ? $request->name : '';
        $category->description = isset($request->description) ? $request->description : '';
        $category->content = isset($request->content) ? $request->content : '';
        $category->slug = isset($request->slug) ? $request->slug : '';
        $category->state = isset($request->state) ? $request->state : 0;
        $category->parent_id = !empty($request->parent_id) ? $request->parent_id : null;
        $category->lang = isset($request->lang) ? $request->lang : config('constants.LANGUAGE_DEFAULT');
        $category->parrent_post_id = isset($request->parrent_post_id) ? $request->parrent_post_id : null;
        $category->order = isset($request->order) ? $request->order : 99;
        $category->save();
        return $category->id;
    }

    /**
     * @param $category
     */
    public function deleteCategory($category)
    {
        if ($category->parent_id == null) {
            $childCategories = Category::where('parent_id', $category->id)->get();
            if (!empty($childCategories)) {
                foreach ($childCategories as $childCategory) {
                    $this->deleteCategoryPost($childCategory->id);
                    $childCategory->delete();
                }
            }

        }
        $this->deleteCategoryPost($category->id);
        $category->delete();
        return;
    }

    /**
     * @param $categoryId
     */
    public function deleteCategoryPost($categoryId)
    {
        CategoryPost::where('category_id', $categoryId)->delete();
        return;
    }
}
