<?php
if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('getYoutubeIdFromUrl')) {
    //Get code youtube
    function getYoutubeIdFromUrl($url)
    {
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $qs);
            if (isset($qs['v'])) {
                return $qs['v'];
            } else {
                if ($qs['vi']) {
                    return $qs['vi'];
                }
            }
        }
        if (isset($parts['path'])) {
            $path = explode('/', trim($parts['path'], '/'));
            return $path[count($path) - 1];
        }
        return null;
    }
}

if (!function_exists('getYoutubeThumbFromUrl')) {
    //Get code youtube
    function getYoutubeThumbFromUrl($url, $imageName = 0)
    {
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
        $thumbUrl = null;
        if (!empty($matches[1])) {
            $thumbUrl = "http://img.youtube.com/vi/{$matches[1]}/{$imageName}.jpg";
        }
        return $thumbUrl;
    }
}

if (!function_exists('strToHex')) {
    function strToHex($string)
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0' . $hexCode, -2);
        }
        return strToUpper($hex);
    }
}
if (!function_exists('hexToStr')) {
    function hexToStr($hex)
    {
        $string = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
        }
        return $string;
    }
}

if (!function_exists('getTotalPriceCart')) {
    function getTotalPriceCart($cart)
    {
        $total = 0;
        foreach ($cart as $item) {
            $total += ($item['product']->price * $item['amount']);
        }
        return $total;
    }
}

if (!function_exists('partition')) {
    function partition($list, $p)
    {
        $listlen = count($list);
        $partlen = floor($listlen / $p);
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice($list, $mark, $incr);
            $mark += $incr;
        }
        return $partition;
    }
}