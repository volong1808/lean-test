<?php
namespace App\Helpers;

use App\Image;
use App\Page;
use App\SeoData;
use Illuminate\Support\Facades\DB;
include 'FormFileHelper.php';

trait PageHelper
{
    /**
     * @param $pageType
     * @param array $page
     * @return array
     */
    public function getPageInputData($pageType, $page = [], $parentPage = []) {
        $initValue = [];
        $imageDefault = '';
        if (empty($page) && empty($parentPage)) {
            $imageDefault = '';
        } elseif (!empty($page) && empty($parentPage)) {
            $imageDefault = Image::find($page->image_id);
        } else {
            $imageDefault = Image::find($parentPage->image_id);
        }
        $initValue['name'] = empty($page) ? '' : $page->name;
        $initValue['description'] = empty($page) ? '' : $page->description;
        $initValue['content'] = empty($page) ? '' : $page->content;
        $initValue['slug'] = empty($page) ? '' : $page->slug;
        $initValue['state'] = empty($page) ? config('constants.page_STATE.PUBLISHED') : $page->state;
        $initValue['image'] = $imageDefault;
        $initValue['lang'] = empty($page) ? config('constants.LANGUAGE_DEFAULT') : $page->lang;
        $initValue['parent_id'] = empty($page) ? '' : $page->parrent_post_id;

        if (isset($this->seoable) && $this->seoable) {
            if (!empty($page)) {
                $seoData = DB::table('seo_data')->where('item_type', 'page')
                    ->where('item_id', $page->id)->first();
                $initValue['seo_name'] = isset($seoData) ? $seoData->name : '';
                $initValue['seo_description'] = isset($seoData) ? $seoData->description : '';
            } else {
                $initValue['seo_name'] = '';
                $initValue['seo_description'] = '';
            }
        }

        return $initValue;
    }

    /**
     * @param $pageType
     * @param $request
     * @param array $page
     * @return mixed
     */
    public function savePage($pageType, $request, $page = [])
    {
        $current = date('Y-m-d H:i:s');
        if (empty($page)) {
            $page = new Page();
            $page->created_at = $current;
        }

        if (!empty($request->parrent_id)) {
            $pageParent = Page::find($request->parrent_id);
            $image = Image::find($pageParent->image_id);
            $page->image_id =  !empty($image) ? $image->id : null;
        }

        if ($request->hasFile('image')) {
            if (!empty($page) AND $page->image_id) {
                $image = Image::find($page->image_id);
                if (!empty($image)) {
                    deleteFileDatabase($image->url);
                    deleteFileDatabase($image->thumb);
                    deleteFileDatabase($image->origin);
                    $image->delete();
                }

            }
            $page->image_id = $this->savePageImage($pageType, $request);
        }
        $page->page_type = $pageType;
        $page->name = isset($request->name) ? $request->name : '';
        $page->description = isset($request->description) ? $request->description : '';
        $page->content = isset($request->content) ? $request->content : '';
        $page->slug = isset($request->slug) ? $request->slug : '';
        $page->state = isset($request->state) ? $request->state : 0;
        $page->lang = isset($request->lang) ? $request->lang : config('constants.LANGUAGE_DEFAULT');
        $page->parrent_post_id = isset($request->parrent_id) ? $request->parrent_id : null;

        $page->save();

        if (isset($this->seoable) && $this->seoable) {
            $this->saveSeoData($page, $request);
        }

        return $page->id;
    }

    public function saveSeoData($page, $request)
    {
        $seoData = SeoData::where('item_type', 'page')->where('item_id', $page->id)->first();
        if (!empty($seoData)) {
            $seoData->name = $request->seo_name;
            $seoData->description = $request->seo_description;
            $seoData->image_id = $page->image_id;
            $seoData->save();
        } else {
            $seoData = new SeoData();
            $seoData->name = $request->seo_name;
            $seoData->description = $request->seo_description;
            $seoData->item_type = 'page';
            $seoData->item_id = $page->id;
            $seoData->image_id = $page->image_id;
            $seoData->save();
        }
    }

    /**
     * @param $pageType
     * @param $request
     * @return bool
     */
    public function savePageImage($pageType, $request)
    {
        $imagedetails = getimagesize($_FILES['image']['tmp_name']);
        $imageOrigin = getUploadImageUrl(
            $request->image,
            $imagedetails[0],
            $imagedetails[1],
            'origin'
        );
        $imageInfo = getUploadImageUrl(
            $request->image,
            config("constants.{$pageType}_image.WIDTH"),
            config("constants.{$pageType}_image.HEIGHT")
        );
        $thumpInfo = getUploadImageUrl(
            $request->image,
            config("constants.{$pageType}_image.THUMB_WIDTH"),
            config("constants.{$pageType}_image.THUMB_HEIGHT"),
            'thumb'
        );
        $newImage = new Image();
        $newImage->origin = $imageOrigin['url'];
        $newImage->url = $imageInfo['url'];
        $newImage->thumb = $thumpInfo['url'];
        $newImage->file_name = $imageInfo['filename'];
        $newImage->file_path = $imageInfo['filePath'] . $imageInfo['filename'];
        $newImage->width = config("constants.{$pageType}_image.WIDTH");
        $newImage->height = config("constants.{$pageType}_image.HEIGHT");
        $newImage->name = $request->name;
        $newImage->size = filesize(base_path() . DIRECTORY_SEPARATOR . $imageInfo['url']);
        $newImage->alt = $request->name . ' image';

        $newImage->save();
        return $newImage->id;
    }
}
