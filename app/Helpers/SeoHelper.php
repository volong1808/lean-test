<?php
namespace App\Helpers;

use App\Image;
use App\Page;
use App\SeoData;
use Illuminate\Support\Facades\DB;

trait SeoHelper
{
    public function getPageSeoData($pageId)
    {
        return $seoData = SeoData::where('item_type', 'page')->where('item_id', $pageId)->first();
    }

    public function getPostSeoData($postId)
    {
        return $seoData = SeoData::where('item_type', 'post')->where('item_id', $postId)->first();
    }
}
