<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 * @package App
 */
class Post extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function categoryChild()
    {
        return $this->belongsToMany(Category::class)->where('categories.parent_id', '<>', null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function slide()
    {
        return $this->hasOne(MetaDataSlide::class, 'post_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function video()
    {
        return $this->hasOne(MetaDataVideo::class, 'post_id', 'id');
    }

    public function faq()
    {
        return $this->hasOne(MetaDataFaq::class, 'post_id', 'id');
    }

    public function galleries()
    {
        return $this->hasMany(Gallery::class);
    }

    public function postSeo()
    {
        return $this->hasOne(SeoData::class, 'item_id', 'id')
            ->where('item_type', 'post');
    }

    public function product()
    {
        return $this->hasOne(MetaDataProduct::class, 'post_id', 'id');
    }

    public function langPost()
    {
        return $this->hasOne(Post::class, 'parrent_post_id', 'id');
    }
}
