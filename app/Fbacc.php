<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fbacc extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'fbacc';
    protected $primaryKey = 'taikhoan';
    public $timestamps = false;
}
