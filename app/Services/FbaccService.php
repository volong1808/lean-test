<?php

namespace App\Services;

use App\Fbacc;

class FbaccService {
    public $fbacc;

    public function __construct()
    {
        $this->fbacc = new Fbacc();
    }

    public function add($username, $iDay, $time)
    {
        $this->fbacc->taikhoan = $username;
        $this->fbacc->date = $iDay;
        $this->fbacc->time = $time;
        return $this->fbacc->save();
    }

    public function getByUsername($username)
    {
        return $this->fbacc->where('taikhoan', $username)->first();
    }

    public function update($username, $iDay, $time) {
        $account = $this->getByUsername($username);
        if (empty($account)) {
            return false;
        }
        $account->date = $iDay;
        $account->time = $time;
        return $account->save();

    }

    public function calcTime($account, $iDay)
    {
        $GMTime = time();
        $sTime = $account->time;
        if ($GMTime > $sTime){
            $TimeSet = $GMTime + $iDay * 86400;
        }else{
            $TimeSet = $sTime + $iDay * 86400 ;
        }
        $timezone  = 7;
        $cTime = gmdate('d-m-Y H:i', $TimeSet + 3600*($timezone+date("0")));
        $data = [
            'date' => $cTime,
            'time' => $TimeSet
        ];
        return $data;
    }

    public function createAccount($username, $iDay)
    {
        $account = $this->getByUsername($username);
        if (empty($account)) {
            $this->add($username, '', 0);
        }
        $account = $this->getByUsername($username);
        $dataTime = $this->calcTime($account, $iDay);
        return $this->update($username, $dataTime['date'], $dataTime['time']);

    }
}