<?php

namespace App\Services;
use App\Post;

class VideoService {
    public $video;
    public $type;

    public function __construct($type = null)
    {
        $this->video = new Post();
        if (!empty($type)) {
            $this->video = $type;
        } else {
            $this->type = config('constants.POST_TYPE.VIDEO');
        }
    }


    public function getOther($slugIgnore = null, $page = 15)
    {
        return $this->video->where('slug', '<>', $slugIgnore)
            ->with('video')
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('post_type', $this->type)
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }

    public function getAll($page = 15)
    {
        return $this->video->with('video')
            ->with('image')
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('post_type', $this->type)
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }


    public function getNewVideo()
    {
        return $this->video->with('video')
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('post_type', $this->type)
            ->orderBy('id', 'DESC')
            ->first();
    }

    public function getBySlug($slug)
    {
        return $this->video->with('video')
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('post_type', $this->type)
            ->where('slug', $slug)
            ->first();
    }
}