<?php

namespace App\Services;

use App\Category;

class CategoryService {
    public $category;

    public function __construct()
    {
        $this->category = new Category();
    }

    public function getPostInCat($type, $page = 15)
    {
        return $this->category->where('post_type', $type)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('posts')
            ->with('image')
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }

    public function getParent($type, $lang, $page = 15)
    {
        return $this->category->where('post_type', $type)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('parent_id', null)
            ->where('lang', $lang)
            ->with('posts')
            ->with('image')
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }

    public function getBySlug($slug)
    {
        return $this->category->with('children')->where('slug', $slug)->where('state', config('constants.POST_STATE.PUBLISHED'))->first();
    }

    public function getSub($parentId, $page = 15)
    {
        return $this->category->where('parent_id', $parentId)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('parent_id', null)
            ->with('posts')
            ->with('image')
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }
}