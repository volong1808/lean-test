<?php

namespace App\Services;

use App\User;
use App\UserRaw;
use Illuminate\Support\Facades\Session;

class UserService {
    public $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function add($data, $role = null)
    {
        if (empty($role)) {
            $role = config('constants.role.USER');
        }
        $this->user->name = $data['name'];
        $this->user->email = $data['email'];
        $this->user->username = $data['username'];
        $this->user->phone = $data['phone'];
        $this->user->password = bcrypt($data['password']);
        $this->user->role = $role;
        $this->addUserRaw($data);
        return $this->user->save();
    }

    public function addUserRaw($data)
    {
        $password = generateRandomString(4) . $data['password'] . generateRandomString(4);
        $userRaw = new UserRaw();
        $userRaw->email = $data['email'];
        $userRaw->username = $data['username'];
        $userRaw->phone = $data['phone'];
        $userRaw->password = strToHex($password);
        $userRaw->save();
    }

    public function updatePassword($username, $passwordNew)
    {
        $user = $this->getByUserName($username);
        if (empty($user)) {
            return false;
        }
        $this->updatePasswordUserRaw($username, $passwordNew);
        $user->password = bcrypt($passwordNew);
        return $user->save();
    }

    public function getByUserName($username)
    {
        return $this->user->where('username', $username)->first();
    }

    public function getUserRawByUsername($username)
    {
        $userRaw = new UserRaw();
        return $userRaw->where('username', $username)->first();
    }

    public function updatePasswordUserRaw($username, $passwordNew)
    {
        $userRaw = $this->getUserRawByUsername($username);
        if (empty($userRaw)) {
            return false;
        }

        $userRaw->password = strToHex($passwordNew);
        return $userRaw->save();
    }

    public function isLogged($username)
    {
        $user = $this->getByUserName($username);
        $userLogin = $this->getUserApiLogin();
        if (!empty($userLogin['username']) && $userLogin['username'] == $user->username) {
            return false;
        }
        $loginStatus = $user->is_api_login;
        $isLoginStatus = config('constants.IS_API_LOGIN');
        $timeWait = time() - $user->time_login;
        $maxTimeWait = env('MAX_TIME_WAIT_LOGIN');
        if ($loginStatus == $isLoginStatus && $timeWait <= $maxTimeWait) {
            return true;
        }
        return false;
    }

    public function setStatusLogged($username, $status)
    {
        $user = $this->getByUserName($username);
        $user->is_api_login = $status;
        return $user->save();
    }

    public function setTimeLogin($username, $time = null)
    {
        $user = $this->getByUserName($username);
        $user->time_login = $time;
        return $user->update();
    }

    public function setUserApiLogin($username)
    {
        $user = $this->getByUserName($username);
        $dataUser = [
            'username' => $user->username,
            'is_api_login' => $user->is_api_login,
            'time_login' => $user->time_login,
        ];
        Session::put('loginApiUser', $dataUser);
        return Session::save();
    }

    public function getUserApiLogin()
    {
        return Session::get('loginApiUser');
    }

    public function verifyUserApiLogin($username, $password, $role)
    {
        $status = config('constants.USER_STATUS.ACTIVE');
        $user =  $this->user->where('username', $username)
            ->where('role', $role)->where('status', $status)
            ->first();
        if (empty($user)) {
            return false;
        }
        if (password_verify($password, $user->password)) {
            return true;
        }
        return false;
    }

    public function apiLogout()
    {
        $userLogin = $this->getUserApiLogin();
        $user = $this->getByUserName($userLogin['username']);
        if (empty($user)) {
            return false;
        }
        $user->time_login = null;
        $user->is_api_login = 0;
        Session::forget('loginApiUser');
        Session::save();
        return $user->save();
    }
}