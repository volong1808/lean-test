<?php

namespace App\Services;

use App\MetaDataPlan;

class PlanService
{
    public $plan;

    public function __construct()
    {
        $this->plan = new MetaDataPlan();
    }

    public function getPlanById($id)
    {
        return $this->plan->where('id', $id)->with('post')->first();
    }

    public function getPlanByProductId($productId)
    {
        return $this->plan->where('product_id', $productId)->with('post')->get();
    }
}