<?php

namespace App\Services;

use App\Page;
use App;

class PageService {
    public $page;

    public function __construct()
    {
        $this->page = new Page();
    }

    public function getBySlug($slug)
    {
        $lang = App::getLocale();
        return $this->page->where('slug', $slug)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->first();
    }
}