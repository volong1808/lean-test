<?php

namespace App\Services;

use App\Post;
use App;

class PostService {
    public $post;

    public function __construct()
    {
        $this->post = new Post();
    }

    public function getByType($type, $page = 15)
    {
        $lang = App::getLocale();
        return $this->post->where('post_type', $type)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }

    public function getByTypeLimit($type, $limitStart = 0, $limitEnd = 10)
    {
        $lang = App::getLocale();
        return $this->post->where('post_type', $type)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->skip($limitStart)->take($limitEnd)->get();
    }

    public function getByTypeAll($type)
    {
        $lang = App::getLocale();
        return $this->post->where('post_type', $type)
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('id', 'DESC')
            ->get();
    }
}