<?php

namespace App\Services;

use App\Category;
use App\MetaDataProduct;
use App\Post;
use Illuminate\Support\Facades\Session;

class ProductService
{
    public $product;

    public function __construct()
    {
        $this->product = new Post();
    }

    public function getDetail($slug)
    {
        return $this->product->select('posts.*', 'change_log', 'model', 'producer', 'price', 'discount_price')->where('state', config('constants.POST_STATE.PUBLISHED'))
                ->where('slug', $slug)
                ->join('metadata_product', 'metadata_product.post_id', '=', 'posts.id')
                ->with('image')
                ->with(['product' => function($query) {
                    $query->with(['partner' => function ($query) {
                        $query->with('image');
                    }]);
                }])
                ->with(['galleries' => function ($query) {
                    $query->with('image');
                }])->first();
    }

    public function search($keyword, $lang, $page)
    {
        $prpducts = Post::where('posts.post_type', config('constants.POST_TYPE.PRODUCT'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('name', 'like', '%' . $keyword . '%')
            ->with('image')
            ->where('lang', $lang)
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }]);
        $prpducts = $prpducts->where('lang', $lang);
        $prpducts = $prpducts->paginate($page);
        return $prpducts;
    }

    public function getById($id)
    {
        return $this->product->select('posts.*', 'change_log', 'model', 'producer', 'price', 'discount_price')->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('posts.id', $id)
            ->join('metadata_product', 'metadata_product.post_id', '=', 'posts.id')
            ->with('image')
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }])
            ->with(['galleries' => function ($query) {
                $query->with('image');
            }])->first();
    }

    public function getByProducer($producerId, $page = 15)
    {
        return $this->product->select('posts.*', 'change_log', 'model', 'producer', 'price', 'discount_price')
            ->where('posts.post_type', config('constants.POST_TYPE.PRODUCT'))
            ->where('metadata_product.change_log', $producerId)
            ->with('image')
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }])
            ->join('metadata_product', 'metadata_product.post_id', '=', 'posts.id')
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate($page);
    }

    public function getOther($inogerId, $lang)
    {
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        return $this->product->where('posts.post_type', config('constants.POST_TYPE.PRODUCT'))
            ->where('state', config('constants.POST_STATE.PUBLISHED'))
            ->where('id', '<>', $inogerId)
            ->with('image')
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->where('lang', $lang)
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }])
            ->paginate($itemInPage);
    }

    public function getAll()
    {
        return $this->product->with(['post' => function ($query) {
            $query->where('state', config('constants.POST_STATE.PUBLISHED'))
                ->with('image');
        }]) ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function getCategoryDetailBySlug($slug)
    {
        $category = new Category();
        return $category->where('slug', $slug)->where('state', config('constants.POST_STATE.PUBLISHED'))->first();
    }

    public function getParentCategory ($categoryCurrent, &$parentCategory)
    {
        if (!empty($categoryCurrent->parent_id)) {
            $category = new Category();
            $categoryDetail = $category->where('id', $categoryCurrent->parent_id)
                ->where('state', config('constants.POST_STATE.PUBLISHED'))->first();
            if (empty($categoryDetail)) {
                return $parentCategory;
            }

            if (!empty($categoryDetail->parent_id)) {
                $parentCategory[] = $categoryDetail;
                $this->getParentCategory($categoryDetail, $parentCategory);
            }

            $parentCategory[] = $categoryDetail;
        }
    }

    public function getByCateId($cateId)
    {
        $itemInPage = config('constants.NEWS_PAGE_ITEM');
        return $this->product->select('posts.*', 'categories.slug as cate_slug')
            ->where('posts.post_type', config('constants.POST_TYPE.PRODUCT'))
            ->where('category_post.category_id', $cateId)
                            ->join('category_post', 'category_post.post_id', '=', 'posts.id')
                            ->join('categories', 'category_post.category_id', '=', 'categories.id')
            ->with('image')
            ->with(['product' => function($query) {
                $query->with(['partner' => function ($query) {
                    $query->with('image');
                }]);
            }])
            ->orderBy('order', 'ASC')
            ->orderBy('id', 'DESC')
            ->paginate($itemInPage);
    }

    public function addToCart($product, $amount = 1)
    {
        $cart = Session::get('cart');
        $cart[$product->id]['product'] = $product;
        $cart[$product->id]['amount'] = $amount;
        return Session::put('cart', $cart);
    }

    public function deleteItemCart($id)
    {
        if (Session::has('cart.' . $id)) {
            return Session::forget('cart.' . $id);
        }
        return false;
    }

    public function updateCartAmount($id, $amount)
    {
        if (Session::has('cart.' . $id)) {
            if ($amount == 0) {
                return $this->deleteItemCart($id);
            } else {
                $cart = $this->getCart();
                $cart[$id]['amount'] = $amount;
                return Session::put('cart', $cart);
            }

        }
        return false;
    }

    public function getCart()
    {
        return Session::get('cart');
    }

    public function removeCart()
    {
        return Session::forget('cart');
    }
}