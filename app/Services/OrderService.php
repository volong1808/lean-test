<?php

namespace App\Services;

use App\Order;
use App\OrderItem;

class OrderService
{
    public $order;

    public function __construct()
    {
        $this->order = new Order();
    }

    public function getLastOder()
    {
        return $this->order->orderBy('id', 'DESC')->first();
    }

    public function getCode()
    {
        $lastId = 0;
        $lastCoder = $this->getLastOder();
        if (!empty($lastCoder)) {
            $lastId = $lastCoder->id;
        }

        return date('dmyHis') . $lastId + 1;
    }

    public function add($data)
    {
        $this->order->name = $data['name'];
        $this->order->phone_number = $data['phone_number'];
        $this->order->email = $data['email'];
        $this->order->address = $data['address'];
        $this->order->message = $data['message'];
        $this->order->status = 1;
        $this->order->save();
        return $this->order;
    }

    public function addCartItem($data, $orderId)
    {
        $orderItem = new OrderItem();
        $orderItem->product_id = $data['product_id'];
        $orderItem->order_id = $orderId;
        $orderItem->price = 0;
        $orderItem->amount = $data['amount'];
        $orderItem->save();
    }

    public function getByCode($code)
    {
        return $this->order->where('code', $code)->first();
    }

    public function getItemCart($oderId)
    {
        $cartItem = new OrderItem();

    }

    public function updateStatusPaidByCode($code, $data)
    {
        $order = $this->getByCode($code);
        $order->status = $data['status'];
        if (!empty($data['transaction_no'])) {
            $order->transaction_no = $data['transaction_no'];
        }
        return $order->save();
    }
}