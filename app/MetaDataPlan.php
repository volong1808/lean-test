<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaDataPlan extends Model
{
    protected $table = 'metadata_plan';
    public $timestamps = false;

    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'post_id');
    }
}
