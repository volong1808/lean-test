<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MetaDataFaq
 * @package App
 */
class MetaDataFaq extends Model
{
    protected $table = 'metadata_faq';

    public $timestamps = false;

    public function post()
    {
        return $this->belongsTo(Post::class, 'id', 'post_id');
    }
}
