<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoData extends Model
{
    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }
}
