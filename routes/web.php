<?php
use Illuminate\Support\Facades\Session;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => ['auth', 'admin']], function () {
            Route::get('/', 'AdminController@index')->name('admin_index');

            Route::get('/my-profile', 'UserController@myProfile')->name('my_profile');
            Route::post('/update-profile', 'UserController@updateProfile')->name('update_profile');

            Route::get('product/list', 'ProductController@list')->name('product_list');
            Route::get('product/add', 'ProductController@add')->name('product_add');
            Route::post('product/add/submit', 'ProductController@doAdd')->name('product_add_submit');
            Route::post('/product/delete', 'ProductController@delete')->name('product_delete');
            Route::post('/product/bulk-action', 'ProductController@bulkAction')->name('product_bulk_action');
            Route::get('product/edit/{id}', 'ProductController@edit')->name('product_edit');
            Route::post('product/do_edit/{id}', 'ProductController@doEdit')->name('product_do_edit');
            Route::post('product/order/{id}', 'ProductController@order')->name('product_order_ajax');

            Route::get('slide/list', 'SlideController@list')->name('slide_list');
            Route::post('/slide/delete', 'SlideController@delete')->name('slide_delete');
            Route::post('/slide/bulk-action', 'SlideController@bulkAction')->name('slide_bulk_action');
            Route::get('/slide/add', 'SlideController@add')->name('slide_add');
            Route::post('/slide/do_add', 'SlideController@doAdd')->name('slide_do_add');
            Route::get('slide/edit/{id}', 'SlideController@edit')->name('slide_edit');
            Route::post('slide/do_edit/{id}', 'SlideController@doEdit')->name('slide_do_edit');

            Route::get('customer/list', 'CustomerController@list')->name('customer_list');
            Route::post('/customer/delete', 'CustomerController@delete')->name('customer_delete');
            Route::post('/customer/bulk-action', 'CustomerController@bulkAction')->name('customer_bulk_action');
            Route::get('/customer/add', 'CustomerController@add')->name('customer_add');
            Route::post('/customer/do_add', 'CustomerController@doAdd')->name('customer_do_add');
            Route::get('customer/edit/{id}', 'CustomerController@edit')->name('customer_edit');
            Route::post('customer/do_edit/{id}', 'CustomerController@doEdit')->name('customer_do_edit');

            Route::get('partner/list', 'PartnerController@list')->name('partner_list');
            Route::post('/partner/delete', 'PartnerController@delete')->name('partner_delete');
            Route::post('/partner/bulk-action', 'PartnerController@bulkAction')->name('partner_bulk_action');
            Route::get('/partner/add', 'PartnerController@add')->name('partner_add');
            Route::post('/partner/do_add', 'PartnerController@doAdd')->name('partner_do_add');
            Route::get('partner/edit/{id}', 'PartnerController@edit')->name('partner_edit');
            Route::post('partner/do_edit/{id}', 'PartnerController@doEdit')->name('partner_do_edit');

            Route::get('post/list', 'PostController@list')->name('post_list');
            Route::post('/post/delete', 'PostController@delete')->name('post_delete');
            Route::post('/post/bulk-action', 'PostController@bulkAction')->name('post_bulk_action');
            Route::get('post/add', 'PostController@add')->name('post_add');
            Route::post('post/add', 'PostController@handleAdd')->name('post_add_submit');
            Route::get('post/edit/{id}', 'PostController@edit')->name('post_edit');
            Route::post('post/edit/{id}', 'PostController@handleEdit')->name('post_edit_submit');

            Route::get('service/list', 'ServiceController@list')->name('service_list');
            Route::post('/service/delete', 'ServiceController@delete')->name('service_delete');
            Route::post('/service/bulk-action', 'ServiceController@bulkAction')->name('service_action');
            Route::get('service/add', 'ServiceController@add')->name('service_add');
            Route::post('service/add', 'ServiceController@handleAdd')->name('service_add_submit');
            Route::get('service/edit/{id}', 'ServiceController@edit')->name('service_edit');
            Route::post('service/edit/{id}', 'ServiceController@handleEdit')->name('service_edit_submit');

            Route::get('plan/list', 'PlanController@index')->name('plan_list');
            Route::post('/plan/delete', 'PlanController@delete')->name('plan_delete');
            Route::post('/plan/bulk-action', 'PlanController@bulkAction')->name('plan_bulk_action');
            Route::get('plan/add', 'PlanController@add')->name('plan_add');
            Route::post('plan/do_add', 'PlanController@doAdd')->name('plan_do_add');
            Route::get('plan/edit/{id}', 'PlanController@edit')->name('plan_edit');
            Route::post('plan/do_edit/{id}', 'PlanController@doEdit')->name('plan_do_edit');

            Route::get('product_category/list', 'ProductCategoryController@list')->name('product_category_list');
            Route::post('/product_category/delete', 'ProductCategoryController@delete')->name('product_category_delete');
            Route::post('/product_category/bulk-action', 'ProductCategoryController@bulkAction')->name('product_category_bulk_action');
            Route::get('product_category/add', 'ProductCategoryController@add')->name('product_category_add');
            Route::post('product_category/add', 'ProductCategoryController@handleAdd')->name('product_category_add_submit');
            Route::get('product_category/edit/{id}', 'ProductCategoryController@edit')->name('product_category_edit');
            Route::post('product_category/edit/{id}', 'ProductCategoryController@handleEdit')->name('product_category_edit_submit');
            Route::post('product_category/order/{id}', 'ProductCategoryController@order')->name('product_order_ajax');

            Route::get('post_category/list', 'PostCategoryController@index')->name('post_category_list');
            Route::post('/post_category/delete', 'PostCategoryController@delete')->name('post_category_delete');
            Route::post('/post_category/bulk-action', 'PostCategoryController@bulkAction')->name('post_category_bulk_action');
            Route::get('post_category/add', 'PostCategoryController@add')->name('post_category_add');
            Route::post('post_category/add', 'PostCategoryController@handleAdd')->name('post_category_add_submit');
            Route::get('post_category/edit/{id}', 'PostCategoryController@edit')->name('post_category_edit');
            Route::post('post_category/edit/{id}', 'PostCategoryController@handleEdit')->name('post_category_edit_submit');

            Route::get('attribute/list', 'AttributeController@index')->name('attribute_list');
            Route::post('attribute/delete', 'AttributeController@delete')->name('attribute_delete');
            Route::post('attribute/bulk-action', 'AttributeController@bulkAction')->name('attribute_bulk_action');
            Route::get('attribute/add', 'AttributeController@add')->name('attribute_add');
            Route::post('attribute/add', 'AttributeController@handleAdd')->name('attribute_add_submit');
            Route::get('attribute/edit/{id}', 'AttributeController@edit')->name('attribute_edit');
            Route::post('attribute/edit/{id}', 'AttributeController@handleEdit')->name('attribute_edit_submit');

            Route::get('page/list', 'PageController@list')->name('page_list');
            Route::post('/page/delete', 'PageController@delete')->name('page_delete');
            Route::post('/page/bulk-action', 'PageController@bulkAction')->name('page_bulk_action');
            Route::get('page/edit/{id}', 'PageController@edit')->name('page_edit');
            Route::post('page/do_edit/{id}', 'PageController@doEdit')->name('page_do_edit');
            Route::get('page/add/', 'PageController@add')->name('page_add');
            Route::post('page/do_add/', 'PageController@doAdd')->name('page_do_add');

            Route::post('gallery/add', 'GalleryController@add')->name('add_gallery_ajax');
            Route::post('gallery/delete', 'GalleryController@delete')->name('delete_gallery_ajax');
            Route::post('gallery/clear', 'GalleryController@clear')->name('clear_gallery_ajax');

            Route::get('config/{key}', 'OptionController@config')->name('config_website');
            Route::post('config/{key}', 'OptionController@configSave')->name('config_website_post');

            Route::get('user/list', 'UserController@index')->name('user_list');
            Route::post('user/bulk-action', 'UserController@bulkAction')->name('user_bulk_action');
            Route::get('user/edit/{id}', 'UserController@edit')->name('user_edit');

            Route::get('contacts', 'ContactController@index')->name('admin_contacts');
            Route::post('contact/delete', 'ContactController@delete')->name('contact_delete');
            Route::get('contact/show/{id}', 'ContactController@show')->name('contact_show');
            Route::post('contact/update', 'ContactController@update')->name('contact_update');
            Route::post('contact/bulk-action', 'ContactController@bulkAction')->name('contact_bulk_action');

            Route::get('order', 'OrderController@index')->name('admin_order');
            Route::post('order/delete', 'OrderController@delete')->name('order_delete');
            Route::post('order/change_stage', 'OrderController@changeStage')->name('order_chang_stage');
            Route::post('order/bulk-action', 'OrderController@bulkAction')->name('order_bulk_action');

            Route::get('about/list', 'VideoController@list')->name('video_list');
            Route::post('about/delete', 'VideoController@delete')->name('video_delete');
            Route::post('about/bulk-action', 'VideoController@bulkAction')->name('video_bulk_action');
            Route::get('about/add', 'VideoController@add')->name('video_add');
            Route::post('about/do_add', 'VideoController@doAdd')->name('video_do_add');
            Route::get('about/edit/{id}', 'VideoController@edit')->name('video_edit');
            Route::post('about/do_edit/{id}', 'VideoController@doEdit')->name('video_do_edit');

            Route::get('faq/list', 'FaqController@index')->name('faq_list');
            Route::post('faq/delete', 'FaqController@delete')->name('faq_delete');
            Route::post('faq/bulk-action', 'FaqController@bulkAction')->name('faq_bulk_action');
            Route::get('faq/add', 'FaqController@add')->name('faq_add');
            Route::post('faq/do_add', 'FaqController@doAdd')->name('faq_do_add');
            Route::get('faq/edit/{id}', 'FaqController@edit')->name('faq_edit');
            Route::post('faq/do_edit/{id}', 'FaqController@doEdit')->name('faq_do_edit');

            Route::get('faq_category/list', 'FaqCategoryController@index')->name('faq_category_list');
            Route::post('faq_category/delete', 'FaqCategoryController@delete')->name('faq_category_delete');
            Route::post('faq_category/bulk-action', 'FaqCategoryController@bulkAction')->name('faq_category_bulk_action');
            Route::get('faq_category/add', 'FaqCategoryController@add')->name('faq_category_add');
            Route::post('faq_category/add', 'FaqCategoryController@handleAdd')->name('faq_category_add_submit');
            Route::get('faq_category/edit/{id}', 'FaqCategoryController@edit')->name('faq_category_edit');
            Route::post('faq_category/edit/{id}', 'FaqCategoryController@handleEdit')->name('faq_category_edit_submit');

            Route::get('feedback/list', 'FeedbackController@list')->name('feedback_list');
            Route::post('/feedback/delete', 'FeedbackController@delete')->name('feedback_delete');
            Route::post('/feedback/bulk-action', 'FeedbackController@bulkAction')->name('feedback_bulk_action');
            Route::get('feedback/add', 'FeedbackController@add')->name('feedback_add');
            Route::post('feedback/add', 'FeedbackController@handleAdd')->name('feedback_add_submit');
            Route::get('feedback/edit/{id}', 'FeedbackController@edit')->name('feedback_edit');
            Route::post('feedback/edit/{id}', 'FeedbackController@handleEdit')->name('feedback_edit_submit');

            Route::get('job/list', 'JobController@list')->name('job_list');
            Route::post('/job/delete', 'JobController@delete')->name('job_delete');
            Route::post('/job/bulk-action', 'JobController@bulkAction')->name('job_bulk_action');
            Route::get('job/add', 'JobController@add')->name('job_add');
            Route::post('job/add', 'JobController@handleAdd')->name('job_add_submit');
            Route::get('job/edit/{id}', 'JobController@edit')->name('job_edit');
            Route::post('job/edit/{id}', 'JobController@handleEdit')->name('job_edit_submit');

            Route::get('field/list', 'FieldController@list')->name('field_list');
            Route::post('/field/delete', 'FieldController@delete')->name('field_delete');
            Route::post('/field/bulk-action', 'FieldController@bulkAction')->name('field_bulk_action');
            Route::get('field/add', 'FieldController@add')->name('field_add');
            Route::post('field/add', 'FieldController@handleAdd')->name('field_add_submit');
            Route::get('field/edit/{id}', 'FieldController@edit')->name('field_edit');
            Route::post('field/edit/{id}', 'FieldController@handleEdit')->name('field_edit_submit');

            Route::post('editor/upload', 'EditorController@upload')->name('editor_upload');

        });
    });
});
Route::get('change-lang/{lang}', 'HomeController@changeLang')->name('change_lang');
Route::get('', 'HomeController@changeLang')->middleware('localeDefault')->name('index');
Route::group(['middleware' => ['stage_page', 'locale']], function () {
    Route::group(['prefix' => 'vi'], function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('cho-thue', 'RentalController@index')->name('cho-thue');
        Route::group(['prefix' => trans('message.blog_slug')], function () {
            Route::get('', 'NewsController@index')->name('blog_list');
            Route::get('{slug}', 'NewsController@detail')->name('blog_detail');
        });
        Route::group(['prefix' => trans('message.job_slug')], function () {
            Route::get('', 'JobController@index')->name('fr_job_list');
            Route::get('{slug}', 'JobController@detail')->name('fr_job_detail');
        });

        Route::group(['prefix' => trans('message.branch_slug')], function () {
            Route::get('', 'PartnerController@index')->name('branch_list');
            Route::get('{slug}', 'PartnerController@detail')->name('branch_detail');
        });

        Route::group(['prefix' => trans('message.service_slug')], function () {
            Route::get('', 'ServiceController@index')->name('fr_service_list');
            Route::get('{slug}', 'ServiceController@detail')->name('fr_service_detail');
        });

        Route::group(['prefix' => trans('message.field_slug')], function () {
            Route::get('', 'FieldController@index')->name('fr_field_list');
            Route::get('{slug}', 'FieldController@detail')->name('fr_field_detail');
        });

        /*--------------- Product -------------*/
        Route::group(['prefix' =>  trans('message.product_slug')], function () {
            Route::get('', 'ProductController@category')->name('fr_product_list');
            Route::get(trans('message.all_slug'), 'ProductController@index')->name('fr_product_all');
            Route::get(trans('message.search_slug'), 'ProductController@search')->name('fr_product_search');
            Route::get(trans('message.category_slug') . '/all', 'ProductController@category')->name('product_category_all');
            Route::get(trans('message.category_slug') . '/{slug_cate}', 'ProductController@categoryItem')->name('product_category');
            Route::get(trans('message.branch_slug') . '/{slug_producer}/{id}', 'ProductController@listByProducer')->name('product_list_by_producer');
            Route::get('{slug}/{id}', 'ProductController@detail')->name('fr_product_detail');

            Route::get('/nha-san-xuat/{slug_producer}/{id}', 'ProductController@listByProducer')->name('product_list_by_producer');
//            Route::get('{slug}', 'ProductController@detail')->name('product_detail');
//            Route::get('download/{slug}', 'ProductController@download')->name('product_download');
        });

        Route::get('lien-he', 'ContactController@index')->name('contact');
        Route::post('lien-he', 'ContactController@sentContact')->name('contact_post_vi');

        Route::post('dat-hang', 'ContactController@sendOrder')->name('contact_order_vi');
        Route::post('nhan-tin', 'ContactController@sendSubscribe')->name('contact_subscribe_vi');

        Route::get('{slug}', 'PageController@detail')->name('page');

    });

    Route::group(['prefix' => 'en'], function () {
        Route::get('/', 'HomeController@index')->name('home_en');
        Route::get('rental', 'RentalController@index')->name('cho-thue_en');
        Route::group(['prefix' => 'news'], function () {
            Route::get('', 'NewsController@index')->name('blog_list_en');
            Route::get('{slug}', 'NewsController@detail')->name('blog_detail_en');
        });
        Route::group(['prefix' => 'job'], function () {
            Route::get('', 'JobController@index')->name('fr_job_list_en');
            Route::get('{slug}', 'JobController@detail')->name('fr_job_detail_en');
        });

        Route::group(['prefix' => 'branch'], function () {
            Route::get('', 'PartnerController@index')->name('branch_list_en');
            Route::get('{slug}', 'PartnerController@detail')->name('branch_detail_en');
        });

        Route::group(['prefix' => 'service'], function () {
            Route::get('', 'ServiceController@index')->name('fr_service_list_en');
            Route::get('{slug}', 'ServiceController@detail')->name('fr_service_detail_en');
        });

        Route::group(['prefix' => 'industrie'], function () {
            Route::get('', 'FieldController@index')->name('fr_field_list_en');
            Route::get('{slug}', 'FieldController@detail')->name('fr_field_detail_en');
        });

        /*--------------- Product -------------*/

        Route::group(['prefix' =>  'product'], function () {
            Route::get('', 'ProductController@category')->name('fr_product_list_en');
            Route::get('all', 'ProductController@index')->name('fr_product_all_en');
            Route::get('search', 'ProductController@search')->name('fr_product_search_en');
            Route::get('category' . '/all', 'ProductController@category')->name('product_category_all_en');
            Route::get('category' . '/{slug_cate}', 'ProductController@categoryItem')->name('product_category_en');
            Route::get('branch' . '/{slug_producer}/{id}', 'ProductController@listByProducer')->name('product_list_by_producer_en');
            Route::get('{slug}/{id}', 'ProductController@detail')->name('fr_product_detail_en');

            Route::get('/branch/{slug_producer}/{id}', 'ProductController@listByProducer')->name('product_list_by_producer_en');
//            Route::get('{slug}', 'ProductController@detail')->name('product_detail');
//            Route::get('download/{slug}', 'ProductController@download')->name('product_download');
        });

        Route::get('contact', 'ContactController@index')->name('contact_en');
        Route::post('contact', 'ContactController@sentContact')->name('contact_post_en');

        Route::post('order', 'ContactController@sendOrder')->name('contact_order_en');
        Route::post('subscribe', 'ContactController@sendSubscribe')->name('contact_subscribe_en');

        Route::get('{slug}', 'PageController@detail')->name('page_en');
    });
});


