$(function () {
    // Ajax Subscribe
    $(document).on('submit', '#form-subscribe', function (e) {
        e.preventDefault();
        var form = $(this);
        var successForm = form.closest('.newsletter__form').find('#success-form-subscribe');
        successForm.html('');
        successForm.hide();

        var inputs = [
            { name: 'name', element: form.find('#error-name') },
            { name: 'email', element: form.find('#error-email') }
        ];
        clearErrorInputs(inputs);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: {
                name: form.find('input[name="name"]').val(),
                email: form.find('input[name="email"]').val()
            },
            success: function (response) {
                if(response.success) {
                    successForm.html(response.success);
                    successForm.show();
                    form.trigger('reset');
                }
            },
            error: function (err) {
                var response = JSON.parse(err.responseText);
                showErrorInputs(response, inputs);
            }
        });
    });

    // Loading Ajax
    $(document).ajaxStart(function() {
        $("#loading").show();
    });
    $(document).ajaxStop(function() {
        $("#loading").hide();
    });

    $(document).on('click', 'input[name="amount"]',function() {
        $('input[name="amount"]').removeAttr('checked');
        $('input[name="amount_other"]').val('');
        $(this).attr('checked', 'checked');
    });

    $(document).on('click', 'input[name="amount_other"]',function() {
        $('input[name="amount"]').removeAttr('checked');
        $(this).prev("input")[0].click();
    });
});

function clearErrorInputs(inputs) {
    inputs.forEach(input => {
        input.element.html('');
    });
}

function showErrorInputs(response, inputs){
    inputs.forEach(input => {
        if(response[input.name]) {
            var errorTxt = '';
            response[input.name].forEach(function (error) { errorTxt += '<div class="gfield_description validation_message">' + error + '</div>'; });
            input.element.html(errorTxt);
        }
    });
}